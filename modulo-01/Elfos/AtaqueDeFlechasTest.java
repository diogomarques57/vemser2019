import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import java.util.*;

public class AtaqueDeFlechasTest
{
    @Test
    public void Testar()
    {
        ArrayList<Elfo> elfos = new ArrayList<>();
        AtaqueDeFlechas a = new AtaqueDeFlechas();
        
        ElfoVerde verde1= new ElfoVerde("Verde");
        ElfoVerde verde2= new ElfoVerde("Verde");
        ElfoVerde verde3= new ElfoVerde("Verde");
        ElfoVerde verde4= new ElfoVerde("Verde");
        
        verde1.getFlecha().setQuantidade(40);
        verde2.getFlecha().setQuantidade(20);
        verde3.getFlecha().setQuantidade(0);
        verde4.getFlecha().setQuantidade(5);
        
        elfos.add(verde1);
        elfos.add(verde2);
        elfos.add(verde3);
        elfos.add(verde4);
        
        ElfoNoturno noturno1 = new ElfoNoturno("Noturno");
        ElfoNoturno noturno2 = new ElfoNoturno("Noturno");
        ElfoNoturno noturno3 = new ElfoNoturno("Noturno");
        ElfoNoturno noturno4 = new ElfoNoturno("Noturno");
        ElfoNoturno noturno5 = new ElfoNoturno("Noturno");
        ElfoNoturno noturno6 = new ElfoNoturno("Noturno");
        ElfoNoturno noturno7 = new ElfoNoturno("Noturno");
        
        noturno1.getFlecha().setQuantidade(44);
        noturno2.getFlecha().setQuantidade(200);
        noturno3.getFlecha().setQuantidade(8);
        noturno4.getFlecha().setQuantidade(66);
        noturno5.getFlecha().setQuantidade(1);
        noturno6.getFlecha().setQuantidade(8);
        noturno7.getFlecha().setQuantidade(99);
        
        elfos.add(noturno1);
        elfos.add(noturno2);
        elfos.add(noturno4);
        elfos.add(noturno4);
        elfos.add(noturno6);
        elfos.add(noturno7);
        
        elfos = a.getOrdemDeAtaque(elfos);
        for(int i = 0; i < elfos.size(); i++)
        {
            System.out.println(elfos.get(i).getNome() + " " + elfos.get(i).getFlecha().getQuantidade());
        }
        
        assertEquals(null,a.getOrdemDeAtaque(elfos));
        
        
    }
}
