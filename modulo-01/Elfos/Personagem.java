public abstract class Personagem{
    protected String nome;
    protected Status status;
    protected Inventario inventario;
    protected double vida;
    protected int experiencia;
    protected int qtdXpPorAtaque;
    protected double qtdDanoRecebidoPorAtaque;
    
    {
        inventario = new Inventario(0);
        experiencia = 0;
        status = Status.RECEM_CRIADO;
        qtdXpPorAtaque = 1;
        qtdDanoRecebidoPorAtaque = 0.0;
    }
    
    protected Personagem(String nome){
        this.setNome(nome);
    }
    
    protected String getNome(){
        return nome;
    }
    
    protected void setNome(String nome){
        this.nome = nome;
    }
    
    protected double getQtdVida(){
        return vida;
    }
    
    protected void setQtdVida(double qtdVida){
        this.vida = qtdVida;
    }
    
    protected void curarVida(double cura){
        this.vida += cura;
    }
    
    protected Status getStatus(){
        return this.status;
    }
    
    protected int getExperiencia(){
        return this.experiencia;
    }
    
    protected void aumentarXp(){
      experiencia +=this.qtdXpPorAtaque;
    }    
    
    protected int getQtdItens(){
        return inventario.getTamanhoMochila();
    }
    
    protected Inventario getInventario(){
        return this.inventario;
    }
    
    protected void ganharItem(Item item){
        inventario.adicionar(item);
    }
    
    protected void perderItem(Item item){
        inventario.remover(item);
    }
    
    protected boolean podeSofrerDano(){
        return this.vida > 0;
    }
    
    protected void sofrerDano(double dano){
        if(podeSofrerDano() && (this.status != Status.MORTO)){
            
            this.vida -= this.vida >= dano ? dano : this.vida;
            if(this.vida <=0){
                status= Status.MORTO;
            }
            else{
                status = Status.SOFREU_DANO;
            }
        }
    }
    
    protected abstract String imprimirResultado();
}
