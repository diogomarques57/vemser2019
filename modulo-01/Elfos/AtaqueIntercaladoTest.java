
import java.util.*;
import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;


public class AtaqueIntercaladoTest
{
    @Test
    public void Testar()
    {
        ArrayList<Elfo> elfos = new ArrayList<>();
        AtaqueIntercalado ataque = new AtaqueIntercalado();
        
        ElfoVerde verde1= new ElfoVerde("Verde");
        ElfoVerde verde2= new ElfoVerde("Verde");
        ElfoVerde verde3= new ElfoVerde("Verde");
        ElfoVerde verde4= new ElfoVerde("Verde");
        
        
        elfos.add(verde1);
        elfos.add(verde2);
        elfos.add(verde3);
        elfos.add(verde4);
        
        ElfoNoturno noturno1 = new ElfoNoturno("Noturno");
        ElfoNoturno noturno2 = new ElfoNoturno("Noturno");
        ElfoNoturno noturno3 = new ElfoNoturno("Noturno");
        ElfoNoturno noturno4 = new ElfoNoturno("Noturno");
        ElfoNoturno noturno5 = new ElfoNoturno("Noturno");
        ElfoNoturno noturno6 = new ElfoNoturno("Noturno");
        ElfoNoturno noturno7 = new ElfoNoturno("Noturno");
        
        
        elfos.add(noturno1);
        elfos.add(noturno2);
        elfos.add(noturno3);
        elfos.add(noturno4);
        elfos.add(noturno4);
        elfos.add(noturno6);
        elfos.add(noturno7);
        
        assertEquals(null,ataque.getOrdemDeAtaque(elfos));
        
    }
}
