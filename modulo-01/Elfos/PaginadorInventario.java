import java.util.*;

public class PaginadorInventario{
    private Inventario inventario;
    private int marcador = 0;
    
    public PaginadorInventario(Inventario inventario){
        this.inventario = inventario;
    }
    
    public void pular(int marcador){
        this.marcador = marcador;
    }
    
    public ArrayList<Item> limitar(int limitador){
        ArrayList<Item> lista = new ArrayList<>();
        int max = (limitador+marcador>=inventario.getTamanhoMochila())?inventario.getTamanhoMochila():limitador+marcador;
        for(int i = marcador ; i <max;i++){
            if(inventario.obter(i)!=null){
                lista.add(inventario.obter(i));
            }
        }
        return lista;
    }
}
