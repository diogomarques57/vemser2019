import java.util.*;

public class EstatisticasInventario{
    private Inventario inventario;
    
    public EstatisticasInventario(Inventario inventario){
        this.inventario = inventario;
    }

    public double calcularMedia(){
        ArrayList<Item> itens = this.inventario.getItens();
        if(itens.size() <= 0){
            return Double.NaN;
        }
        double media = 0.0;
        for(int i = 0; i< itens.size();i++){
            media += (double) itens.get(i).getQuantidade();
        }
        return (itens.size()>0) ? media/itens.size() : 0.0;
    }
    
    public double calcularMediana(){
        ArrayList<Item> itens = this.inventario.getItens();
        if(itens.size() <= 0){
            return Double.NaN;
        }
        Inventario novoInventario = new Inventario(itens.size());
        novoInventario.adicionarVarios(itens);
        novoInventario.ordenarItens();
        itens = novoInventario.getItens();
        
        if(itens.size()%2 == 0){
            int menor = itens.get((itens.size()/2)-1).getQuantidade();
            int maior = itens.get((itens.size()/2)).getQuantidade();
            return (maior+menor)/2;
        }
        else{
            int index = itens.size()/2;
            double mediana = (double) itens.get(index).getQuantidade();
            return mediana;
        }
    }
    
    public int qtdItensAcimaDaMedia(){
        ArrayList<Item> itens = this.inventario.getItens();
        int count = 0;
        double media = this.calcularMedia();
        for(int i = 0; i< itens.size();i++){
            if(itens.get(i).getQuantidade()>media){
                count ++;
            }
        }
        return count;
    }
    
}
