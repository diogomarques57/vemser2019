

import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class ElfoDaLuzTest{
    @Test
    public void ElfoDaLuzAtacaAnaoEperdeVida(){
        ElfoDaLuz feanor = new ElfoDaLuz("Fëanor");
        Dwarf farlum = new Dwarf("Farlum");
	feanor.atacarComEspada(farlum);
	
	assertEquals(100.0, farlum.getQtdVida(),1e-9);
	assertEquals(79.0, feanor.getQtdVida(),1e-9);
	 
    }
    
    @Test
    public void ElfoDaLuzAtacaDuasVezes(){
        ElfoDaLuz feanor = new ElfoDaLuz("Fëanor");
        Dwarf farlum = new Dwarf("Farlum");
	feanor.atacarComEspada(farlum);
	feanor.atacarComEspada(farlum);
	assertEquals(90.0, farlum.getQtdVida(),1e-9);
	assertEquals(89.0, feanor.getQtdVida(),1e-9);
	 
    }
    
    @Test
    public void ElfoDaLuzAtacaDesseteVezesEMorre(){
        ElfoDaLuz feanor = new ElfoDaLuz("Fëanor");
        Dwarf farlum = new Dwarf("Farlum");
	feanor.atacarComEspada(farlum);
	feanor.atacarComEspada(farlum);
	feanor.atacarComEspada(farlum);
	feanor.atacarComEspada(farlum);
	feanor.atacarComEspada(farlum);
	feanor.atacarComEspada(farlum);
	feanor.atacarComEspada(farlum);
	feanor.atacarComEspada(farlum);
	feanor.atacarComEspada(farlum);
	feanor.atacarComEspada(farlum);
	feanor.atacarComEspada(farlum);
	feanor.atacarComEspada(farlum);
	feanor.atacarComEspada(farlum);
	feanor.atacarComEspada(farlum);
	feanor.atacarComEspada(farlum);
	feanor.atacarComEspada(farlum);
	feanor.atacarComEspada(farlum);
	assertEquals(00.0, farlum.getQtdVida(),1e-9);
	assertEquals(0.0, feanor.getQtdVida(),1e-9);
	assertEquals(Status.MORTO,feanor.getStatus());
	 
    }
}
