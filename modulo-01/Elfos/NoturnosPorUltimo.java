import java.util.*;
import java.lang.*;
public class NoturnosPorUltimo implements Estrategia
{ 
    public ArrayList<Elfo> getOrdemDeAtaque(ArrayList<Elfo> atacantes){
        ArrayList<Elfo> retorno = new ArrayList<>();

        for(Elfo elfo : atacantes){
            if(elfo.getStatus()!= Status.MORTO){
                switch(elfo.getClass().getName()){
                    case "ElfoVerde":
                    retorno.add(0,elfo);
                    break;
                    case "ElfoNoturno":
                    retorno.add(elfo);
                    break;
                }
            }
        }
        return retorno;
    }

   
    public ArrayList<Elfo> collections(ArrayList<Elfo>elfos){
        ArrayList<Elfo> retorno = new ArrayList<>();
        Collections.sort(elfos,new Comparator<Elfo>() {
                public int compare(Elfo elfoAtual,Elfo proximoElfo){
                    boolean mesmoTipo = elfoAtual.getClass() == proximoElfo.getClass();
                    if(mesmoTipo)
                    {
                        return 0;
                    }
                    return elfoAtual instanceof ElfoVerde && proximoElfo instanceof ElfoNoturno ? -1:1;
                    
                }
                
            });
            return elfos;
    }
    
    public ArrayList<Elfo> getOrdemAtaque(ArrayList<Elfo> atacantes)
    {
        return collections(atacantes);
    }
}
