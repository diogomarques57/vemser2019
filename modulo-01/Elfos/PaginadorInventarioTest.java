
import java.util.*;
import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class PaginadorInventarioTest
{
    @Test
    public void retornarItensDuasVezes(){
        ArrayList<Item> lista = new ArrayList<>();
        Inventario inventario = new Inventario(99);
        Item zero,um,dois,tres;
        zero = new Item(1, "Espada");
        um=new Item(2, "Escudo de metal");
        dois=new Item(3, "Poção de HP");
        tres=new Item(4, "Bracelete");
	inventario.adicionar(zero);
	inventario.adicionar(um);
	inventario.adicionar(dois);
	inventario.adicionar(tres);
	PaginadorInventario paginador = new PaginadorInventario(inventario);
	paginador.pular(0);
	lista.add(zero);
	lista.add(um);
	assertEquals(lista,paginador.limitar(2));
	lista.clear();
	lista.add(dois);
	lista.add(tres);
	paginador.pular(2);
	assertEquals(lista,paginador.limitar(2));
    }
    
    @Test
    public void buscarItensEmArrayVazio(){
        ArrayList<Item> lista = new ArrayList<>();
        Inventario inventario = new Inventario(99);
        PaginadorInventario paginador = new PaginadorInventario(inventario);
        assertEquals(lista,paginador.limitar(2));
    }
    
    @Test
    public void buscarMaisItensDoInventario(){
        ArrayList<Item> lista = new ArrayList<>();
        Inventario inventario = new Inventario(99);
        Item zero,um,dois,tres;
        zero = new Item(1, "Espada");
        um=new Item(2, "Escudo de metal");
        dois=new Item(3, "Poção de HP");
        tres=new Item(4, "Bracelete");
	inventario.adicionar(zero);
	inventario.adicionar(um);
	inventario.adicionar(dois);
	inventario.adicionar(tres);
	PaginadorInventario paginador = new PaginadorInventario(inventario);
	paginador.pular(0);
	lista.add(zero);
	lista.add(um);
	assertEquals(lista,paginador.limitar(2));
	lista.add(dois);
	lista.add(tres);
	assertEquals(lista,paginador.limitar(5));
    }
}
