import java.util.*;

public class ElfoVerde extends Elfo implements Atacar{
    
    private final ArrayList<String> DESCRICOES_VALIDAS = new ArrayList<>(
        Arrays.asList(
            "Espada de aço valariano",
            "Flecha de vidro",
            "Arco de vidro"
            )
    );
    
    public ElfoVerde(String nome){
     super(nome);
     this.qtdXpPorAtaque = 2;
    }
    
    public void ganharItem(Item item){
        String descricao = item.getDescricao();
        boolean validar = DESCRICOES_VALIDAS.contains(descricao);
        if(validar){
            super.ganharItem(item);
        }
    }
    
    public void perderItem(Item item){
        String descricao = item.getDescricao();
        boolean validar = DESCRICOES_VALIDAS.contains(descricao);
        if(validar){
            super.perderItem(item);
        }
    }
    
   
}
