public class Dwarf extends Personagem{

    private boolean escudoEquipado = false;
    
    {
        vida = 110.0F;
    }
    
    public Dwarf(String nome){
        super(nome);
        this.ganharItem(new Item(1,"Escudo"));
    }
    
    
    
    public void equiparEscudo(){
        if(inventario.buscarItem("Escudo") != null){
            this.escudoEquipado = true;
        }
    }
    
    public void desequiparEscudo(){
        this.escudoEquipado = false;
    }
    
    public boolean verificarEscudo(){
        if(inventario.buscarItem("Escudo") != null){
            return this.escudoEquipado;
        }
        else{
            this.escudoEquipado = false;
            return false;
        }
    }
    
    public void sofrerDano(double dano){
        if(this.verificarEscudo()){
                dano/=2;
                this.desequiparEscudo();
        }
        super.sofrerDano(dano);
    }
    
    public String imprimirResultado(){
        return "Dwarf";
    }
        
}
