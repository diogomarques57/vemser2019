
public class DwarfBarbaLonga extends Dwarf
{

    public DwarfBarbaLonga(String nome){
        super(nome);
    }
    
    public void sofrerDano(double dano){
        DadoD6 dado = new DadoD6();
        if(dado.sortear()<5){
            super.sofrerDano(dano);
        }
    }
}
