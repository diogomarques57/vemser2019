
import java.util.*;
import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class ExercitoDeElfosTest{
   @Test
   public void adicionarElfosCorretos(){
       ElfoVerde legolas = new ElfoVerde("legolas");
       ElfoNoturno celeborn = new ElfoNoturno("Celeborn");
       ExercitoDeElfos exercito = new ExercitoDeElfos();
       ArrayList<Elfo> arrayTeste = new ArrayList<>();
       exercito.alistar(legolas);
       exercito.alistar(celeborn);
       arrayTeste.add(legolas);
       arrayTeste.add(celeborn);
       assertEquals(arrayTeste,exercito.getExercito());
   }
   
   @Test
   public void adicionarElfosErrado(){
       ElfoVerde legolas = new ElfoVerde("legolas");
       ElfoDaLuz celeborn = new ElfoDaLuz("Celeborn");
       ExercitoDeElfos exercito = new ExercitoDeElfos();
       ArrayList<Elfo> arrayTeste = new ArrayList<>();
       exercito.alistar(legolas);
       exercito.alistar(celeborn);
       arrayTeste.add(legolas);
       assertEquals(arrayTeste,exercito.getExercito());
   }
   
   @Test
   public void retornarApenasElfosQueSofreramDano(){
       ElfoVerde legolas = new ElfoVerde("legolas");
       ElfoNoturno celeborn = new ElfoNoturno("Celeborn");
       Dwarf gimli = new Dwarf("Gimli");
       celeborn.atacar(gimli);
       ExercitoDeElfos exercito = new ExercitoDeElfos();
       ArrayList<Elfo> arrayTeste = new ArrayList<>();
       exercito.alistar(legolas);
       exercito.alistar(celeborn);
       arrayTeste.add(legolas);
       assertEquals(arrayTeste,exercito.getListaPorStatus(Status.RECEM_CRIADO));
   }
}
