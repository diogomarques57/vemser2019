import java.util.*;
public class AtaqueIntercalado implements Estrategia{
    private ArrayList<String> ordem = new ArrayList(Arrays.asList(
        "ElfoVerde","ElfoNoturno"
    ));
    private int index = 0;
    
    
    public ArrayList<Elfo> getOrdemDeAtaque(ArrayList<Elfo> atacantes){
        ArrayList<Elfo> retorno = new ArrayList<>();
        index = 0;
        int qtdElfos = (atacantes.size()%2)!=0?atacantes.size()-1:atacantes.size();
        for(int i = 0; i<qtdElfos;i++){
            for(Elfo elfo : atacantes){
                if(elfo.getStatus()!= Status.MORTO && elfo.getClass().getName().equals(ordem.get(index))){
                    if (index == 0){
                        index = 1;
                    }
                    else{
                        index = 0;
                    }
                    retorno.add(elfo);
                    atacantes.remove(elfo);
                    break;
                }
            }
        }
        if(retorno.size()%2!=0){
            retorno.remove(retorno.size()-1);
        }
        return retorno;
    }
}