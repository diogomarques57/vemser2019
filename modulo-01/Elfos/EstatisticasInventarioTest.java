

import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class EstatisticasInventarioTest{
    @Test
    public void calcularMediaInvetarioTresItens(){
        Inventario mochila= new Inventario(5);
        Item espada = new Item(2, "Espada");
        Item flecha = new Item(20, "Flecha");
        Item capacete = new Item(12, "Capacete");
        mochila.adicionar(espada);
        mochila.adicionar(flecha);
        mochila.adicionar(capacete);
        EstatisticasInventario estatisticas = new EstatisticasInventario(mochila);
        
        assertEquals(11.33333333333333333,estatisticas.calcularMedia(),1e-9);
        
    }
    
    @Test
    public void calcularMediaInvetarioDoisItens(){
        Inventario mochila= new Inventario(5);
        Item espada = new Item(2, "Espada");
        Item flecha = new Item(20, "Flecha");
        mochila.adicionar(espada);
        mochila.adicionar(flecha);
        EstatisticasInventario estatisticas = new EstatisticasInventario(mochila);
        
        assertEquals(11,estatisticas.calcularMedia(),1e-9);
        
    }
    
    @Test
    public void calcularMediaInvetarioUmItens(){
        Inventario mochila= new Inventario(5);;
        Item capacete = new Item(12, "Capacete");
        mochila.adicionar(capacete);
        EstatisticasInventario estatisticas = new EstatisticasInventario(mochila);
        
        assertEquals(12,estatisticas.calcularMedia(),1e-9);
        
    }
    
     @Test
    public void calcularMediaInvetarioVazio(){
        Inventario mochila= new Inventario(0);
        EstatisticasInventario estatisticas = new EstatisticasInventario(mochila);
        
        assertEquals(Double.NaN,estatisticas.calcularMedia(),1e-9);
        
    }
    
    @Test
    public void calcularMedianaInvetarioTresItens(){
        Inventario mochila= new Inventario(5);
        Item espada = new Item(2, "Espada");
        Item flecha = new Item(20, "Flecha");
        Item capacete = new Item(12, "Capacete");
        mochila.adicionar(espada);
        mochila.adicionar(flecha);
        mochila.adicionar(capacete);
        EstatisticasInventario estatisticas = new EstatisticasInventario(mochila);
        
        assertEquals(12.0,estatisticas.calcularMediana(),1e-9);
        
    }
    
    @Test
    public void calcularMedianaInvetarioDoisItens(){
        Inventario mochila= new Inventario(5);
        Item espada = new Item(2, "Espada");
        Item flecha = new Item(20, "Flecha");
        mochila.adicionar(espada);
        mochila.adicionar(flecha);
        EstatisticasInventario estatisticas = new EstatisticasInventario(mochila);
        
        assertEquals(11.0,estatisticas.calcularMediana(),1e-9);
        
    }
    
    @Test
    public void calcularMedianaInvetarioUmItens(){
        Inventario mochila= new Inventario(5);;
        Item capacete = new Item(12, "Capacete");
        mochila.adicionar(capacete);
        EstatisticasInventario estatisticas = new EstatisticasInventario(mochila);
        
        assertEquals(12.0,estatisticas.calcularMediana(),1e-9);
        
    }
    
     @Test
    public void calcularMedianaInvetarioVazio(){
        Inventario mochila= new Inventario(0);
        EstatisticasInventario estatisticas = new EstatisticasInventario(mochila);
        
        assertEquals(Double.NaN,estatisticas.calcularMediana(),1e-9);
        
    }
}
