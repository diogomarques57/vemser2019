

import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class ContagemElfosTest
{
    @After
    public void tearDown()
    {
        System.gc();
    }
    
    @Test
    public void Contar()
    {
        ContagemElfos contagem= new ContagemElfos();
        ExercitoDeElfos exercito = new ExercitoDeElfos();
        for(int i = 0;i <50;i++)
        exercito.alistar(new ElfoVerde("Verde" + i));
        for(int i = 0;i <50;i++)
        exercito.alistar(new ElfoVerde("Noturno" + i));
        assertEquals(100,contagem.getNumElfos());
    }
    
    @Test
    public void criarUmElfoIncrementaContadorUmaVez()
    {
        Elfo elfo = new Elfo("A");
        assertEquals(1,elfo.getQtdElfos());
        
    }
    
    @Test
    public void criarDoisElfosContadorDuasVezes()
    {
        Elfo elfo = new Elfo("A");
        Elfo elfo2 = new Elfo("B");
        assertEquals(2,elfo.getQtdElfos());
        
    }
}
