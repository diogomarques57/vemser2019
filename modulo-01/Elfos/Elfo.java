














import java.util.*;
import java.lang.*;
public class Elfo extends Personagem implements Atacar{
    private static int qtdElfos=0;
    {
        vida = 100.0;
    }
    
    public Elfo(String nome){
        super(nome);
        this.inventario.adicionar(new Item(1, "Arco"));
        this.inventario.adicionar(new Item(2, "Flecha"));
        qtdElfos++;
    }
    
    public int compareTo(String d) {
      return (this.getClass().getName()).compareTo(d.getClass().getName());
   }
    
   
    protected void finalize() throws Throwable{
        Elfo.qtdElfos --;
    }
    
    public static int qtdElfos(){
        return qtdElfos;
    }
    
    public Item getFlecha(){
        return this.inventario.buscarItem("Flecha");
    }
    
    public int getQtdFlecha(){
         return this.getFlecha().getQuantidade();
    }
  
    public boolean podeAtirarFecha(){
        return this.getQtdFlecha() > 0;
    }
    
    public void atacar(Dwarf dwarf){
        int qtdAtual = this.getQtdFlecha();
        if(podeAtirarFecha()){
            this.getFlecha().setQuantidade(qtdAtual-1);
            aumentarXp();
            double dwarfVida = dwarf.getQtdVida();
            dwarf.sofrerDano(10.0);
            this.sofrerDano(this.qtdDanoRecebidoPorAtaque);
        }
    }
    
    public String imprimirResultado(){
        return "Elfo";
    }
}
