import java.util.*;

public class ComparadorDePerformace{
    public void comparar(){
        ArrayList<Elfo> arrayElfos = new ArrayList<>();
        HashMap<String, Elfo> hashMapElfos = new HashMap<>();
        int qtdElfos = 150000;
        for(int i=0;i<qtdElfos;i++){
            String nome = "Elfo " +i;
            Elfo elfo = new Elfo(nome);
            arrayElfos.add(elfo);
            hashMapElfos.put("nome",elfo);
        }
        
        String nomeBusca = "Elfo 100000";
        
        long mSeqInicio = System.currentTimeMillis();
        Elfo elfoSeq = buscaArrayList(arrayElfos, nomeBusca);
        long mSeqFinal = System.currentTimeMillis();
        
        long mMapInicio = System.currentTimeMillis();
        Elfo elfoMap = buscaHashMap(hashMapElfos, nomeBusca);
        long mMapFinal = System.currentTimeMillis();
        
        String tempoSeq = String.format("%.10f",(mSeqFinal - mSeqInicio)/1000.0);
        String tempoMap = String.format("%.10f",(mMapFinal - mMapInicio)/1000.0);
        
        System.out.println(tempoSeq);
        System.out.println(tempoMap);
        
        System.out.println(elfoSeq);
        System.out.println(elfoMap);
    }
    
    
    private Elfo buscaArrayList(ArrayList<Elfo> lista, String nome){
        for(Elfo elfo: lista){
            if(elfo.getNome().equals(nome)){
                return elfo;
            }
        }
        return null;
    }
    
    private Elfo buscaHashMap(HashMap<String, Elfo> lista, String nome){
        return lista.get(nome);
    }
}
