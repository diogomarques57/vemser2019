import java.util.*;
public class AtaqueDeFlechas implements Estrategia{
    private ArrayList<Elfo> ordem = new ArrayList(Arrays.asList(
                "ElfoVerde","ElfoNoturno"
            ));
    private int index = 0;

    public ArrayList<Elfo> getOrdemDeAtaque(ArrayList<Elfo> atacantes){
        ArrayList<Elfo> retorno = new ArrayList<>();
        index = 0;
        int qtdElfos = atacantes.size();
        int limite = 0;
        int nN = 0;

        for(Elfo elfo : atacantes){
            if(elfo.getStatus()!= Status.MORTO && elfo.getFlecha().getQuantidade() > 0)
            {                   
                retorno.add(elfo);
                index++;
                if(elfo.getClass() == ElfoNoturno.class)
                {
                    nN++;
                }

            }
        }

        limite = (int)(index * 0.3); 
        
        while(nN > limite){
            for(Elfo elfo:retorno)
            {
                if(elfo.getClass() == ElfoNoturno.class)
                {
                    retorno.remove(elfo);
                    nN--;
                    break;
                }
            }
        }
        
        for(int i = 0; i < retorno.size();i++){
                Elfo elfo = retorno.get(i);
                for(int k = 0; k< retorno.size();k++){
                    if(elfo.getFlecha().getQuantidade()> retorno.get(k).getFlecha().getQuantidade()){
                        retorno.remove(i);
                        retorno.add(k,elfo);
                        break;
                    }   
                }
            }
        
        return retorno;
    }
}