
import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class AgendaContatosTest
{
    @Test
    public void TestarAgenda()
    {
        AgendaContatos agenda = new AgendaContatos();
        agenda.adicionarContato("Marcos", "555555");
        agenda.adicionarContato("Mithrandir", "444444");
        assertEquals("555555",agenda.pesquisarPorNome("Marcos")); // “555555”
    }
      @Test
    public void TestarAgendaTelefone()
    {
        AgendaContatos agenda = new AgendaContatos();
        agenda.adicionarContato("Marcos", "555555");
        agenda.adicionarContato("Mithrandir", "444444");
        assertEquals("Mithrandir",agenda.pesquisarPorNumero("444444")); // “555555”
    }
    @Test
    public void TestarCSV()
    {
        AgendaContatos agenda = new AgendaContatos();
        agenda.adicionarContato("Marcos", "555555");
        agenda.adicionarContato("Mithrandir", "444444");
        System.out.println(agenda.csv());
    }
}
