
import java.util.*;

public class AgendaContatos
{
    HashMap<String,String> agenda = new HashMap<>();
    public void adicionarContato(String nome, String telefone)
    {
        agenda.put(nome,telefone);
    }

    public String pesquisarPorNome(String nome)
    {
        return agenda.get(nome);
    }

    public String pesquisarPorNumero(String numero)
    {
        String name= "";    
        for(Map.Entry<String, String> entry : agenda.entrySet()){
            if(entry.getValue().equals(numero))
            {
                name = entry.getKey();
            }
        }
        return name;
    }

    public String csv()
    {
        String csv = "";
        for(Map.Entry<String, String> entry : agenda.entrySet()){
            csv += entry.getKey() + "," + entry.getValue() + "\n";
        }
        return csv;
    }
}

