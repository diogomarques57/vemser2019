let idTela = -1
const pokeApi = new PokeApi()
const form1 = document.getElementById( 'id' )

function animacao() {
  document.getElementById( 'dir' ).classList.add( 'slide-left' );
  document.getElementById( 'esq' ).classList.add( 'slide-right' );
  setTimeout( () => {
    document.getElementById( 'dir' ).style.animationPlayState = 'paused';
    document.getElementById( 'dir' ).style.marginLeft = '120px';
    document.getElementById( 'img' ).style.backgroundColor = 'white';
    document.getElementById( 'led' ).style.filter = 'grayScale(0)';
    document.getElementById( 'numero' ).disabled = false;
    document.getElementById( 'enviar' ).disabled = false;
  }, 900 );
}

function rederizacaoPokemon( pokemon ) {
  const dadosPokemon = document.getElementById( 'dados' );
  const id = dadosPokemon.querySelector( '.id' );
  const nome = dadosPokemon.querySelector( '.nome' );
  const tipo = dadosPokemon.querySelector( '.tipo' );
  const status = dadosPokemon.querySelector( '.status' );
  img.src = pokemon.imagem
  document.getElementById( 'img' ).appendChild( img );
  nome.innerHTML = pokemon.nome
  pokemon.converterAltura();
  pokemon.converterPeso();
  idTela = pokemon.id;
  id.innerHTML = `ID: ${ pokemon.id }`
  altura.innerHTML = `Altura: ${ pokemon.altura }`
  peso.innerHTML = `Peso: ${ pokemon.peso }`
  tipo.innerHTML = 'Tipos:'
  const list1 = document.getElementById( 'tipos' )
  list1.innerHTML = '';
  for ( let i = 0; i < pokemon.tipo.length; i += 1 ) {
    const item = document.createElement( 'li' );

    item.appendChild( document.createTextNode( pokemon.tipo[i] ) );


    list1.appendChild( item );
  }

  const list = document.getElementById( 'stats' )
  list.innerHTML = '';
  for ( let i = 0; i < pokemon.status.length; i += 1 ) {
    const item = document.createElement( 'li' );

    item.appendChild( document.createTextNode( pokemon.status[i] ) );

    list.appendChild( item );
  }
  status.innerHTML = 'Status:'
}

function jaFoiEscolhido( id ) {
  const todosOsCookies = document.cookie;
  if ( todosOsCookies.indexOf( id ) !== -1 ) {
    return true;
  } return false;
}

function buscarPokemonEspecifico() {
  const id = form1.numero.value
  if ( id >= 0 && id < 802 && id !== ( '' ) && id != idTela ) {
    const pokemonEspecifico = pokeApi.buscarEspecifico( id )
    pokemonEspecifico.then(
      pokemon => {
        const poke = new Pokemon( pokemon )
        rederizacaoPokemon( poke )
      },
    )
  } else {
    document.getElementById( 'led' ).style.filter = 'hue-rotate(-150deg)';
  }
}

function buscarPokemonRandom() {
  let id = Math.trunc( Math.random() * ( 802 - 1 ) + 1 )
  while ( jaFoiEscolhido( id ) ) {
    id = Math.trunc( Math.random() * ( 802 - 1 ) + 1 )
  }
  form1.numero.value = id
  document.cookie += ` ${ id } `
  buscarPokemonEspecifico()
}

form1.numero.addEventListener( 'blur', () => { buscarPokemonEspecifico() } )
document.getElementById( 'enviar' ).addEventListener( 'click', () => { buscarPokemonRandom() } )
document.getElementById( 'pokedex' ).addEventListener( 'click', () => { animacao() } )
