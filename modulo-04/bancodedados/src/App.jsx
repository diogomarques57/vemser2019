import React from 'react';
import './App.css';
import Login from './Login';
import Elfos from './Elfos';
import Dwarfs from './Dwarfs';
import Cadastrar from './Cadastrar';
import{ Link } from 'react-router-dom';
import {BrowserRouter as Router, Route } from 'react-router-dom';


function App() {
 
  
  return (
    <div className="App">
      <Router >
        <header className='header'>
        <nav>
          <ul>
            <li><Link to="/Login">Login</Link></li>
            <li><Link to="/Elfos">Elfos</Link></li>
            <li><Link to="/Dwarfs">Dwarfs</Link></li>
            <li><Link to="/Cadastrar">Cadastrar</Link></li>
          </ul>
     
        </nav>
        </header>
       
      <div className='container'>
        <Route path="/Login" exact component={ Login } />
        <Route path="/Elfos" exact component={ Elfos } />
        <Route path="/Dwarfs" exact component={ Dwarfs } />
        <Route path="/Cadastrar" exact component={ Cadastrar } />
      </div>
    
      </Router>
      <footer className="footer">
        <div className="text-center">
  
            <p>
                    &copy; Copyright 2019 - Banco Vemser. Todos os direitos reservados.<br />
                    DBC Company<br />
            </p>
        </div>
    </footer>
    </div>
  );
}

export default App;
