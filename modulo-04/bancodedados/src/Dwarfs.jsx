
import * as axios from 'axios';
import React, {Component} from 'react';


class Dwarfs extends Component {
   
    componentWillMount() {
        
        const { match: { params } } = this.props;
        var header = {
            headers: {Authorization : localStorage.getItem('Authorization')}
        }
        axios.get('http://localhost:8080/api/Dwarf/',header)
          .then(
            resp=>{
                var dwarfs = resp.data;
                this.setState({
                    Listadwarfs: dwarfs
                })
            })
   
    }

    estaPronto(){
        return this.state == null? false:true
        
    }
    
    render (){
        return(
        <React.Fragment>
            { this.estaPronto() ? 
            <div>
            {
                
            this.state.Listadwarfs.map((dwarf) => {  
            return(
                <React.Fragment>
                    Id : {dwarf.id} <br />
                    Nome : {dwarf.nome}  <br />
                    Vida : {dwarf.vida}  <br />
                    Tipo : {dwarf.tipo} <br />
                </React.Fragment>

                  )
                
            })
            }
            </div>
             : 'Carregando...' }
               
        </React.Fragment>
        )
        
    } }
export default Dwarfs;
