
import * as axios from 'axios';
import React, {Component} from 'react';


class Elfos extends Component {
   
    componentWillMount() {
        
        const { match: { params } } = this.props;
        var header = {
            headers: {Authorization : localStorage.getItem('Authorization')}
        }
        axios.get('http://localhost:8080/api/elfo/',header)
          .then(
            resp=>{
                var elfos = resp.data;
                console.log('Elfos',elfos)
                this.setState({
                    Listaelfos: elfos
                })
            })
   
    }

    estaPronto(){
        return this.state == null? false:true
        
    }
    
    render (){
        
        return(
        <React.Fragment>
            { this.estaPronto() ? 
            <div>
            {
                
            this.state.Listaelfos.map((elfo) => {  
            return(
                <React.Fragment>
                    Id : {elfo.id} <br />
                    Nome : {elfo.nome}  <br />
                    Vida : {elfo.vida}  <br />
                    Tipo : {elfo.tipo} <br />
                </React.Fragment>

                  )
                
            })
            }
            </div>
             : 'Carregando...' }
               
        </React.Fragment>
        )
        
    } }
export default Elfos;
