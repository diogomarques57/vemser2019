import React, {Component} from 'react';
import * as axios from 'axios';

export default class Login extends Component {

    constructor(props){
        super(props)
        this.state={
            nome : '',
            tipo : ''
        }
        this.trocaValoresState = this.trocaValoresState.bind(this)
    }

    trocaValoresState(evt){
        const  {name, value} = evt.target;
        this.setState({
            [name] : value,      
        })
        console.log("Nome : ",name,"Valor :", value);
        console.log(this.state)
    }

    logar(evt){
        evt.preventDefault();
        const { nome, tipo } = this.state
        if(nome && tipo)
        {
            const { match: { params } } = this.props;
            var header = {
                headers: {Authorization : localStorage.getItem('Authorization')}
            }
            this.state.tipo.includes( "ELFO" )?
            axios.post('http://localhost:8080/api/elfo/novo', {
                nome: this.state.nome,   
                tipo:this.state.tipo          
            },header)
            :
            axios.post('http://localhost:8080/api/Dwarf/novo', {
                nome: this.state.nome,   
                tipo:this.state.tipo          
            },header)      
        }}
    logout(evt){
        evt.preventDefault();
        localStorage.removeItem('Authorization');
        this.props.history.push('/Login')       
    }

    render() {
        return (
        <div className="login flip-in-hor-bottom"> 
            <React.Fragment>
                {console.log(localStorage.getItem( 'Authorization' ))}
                {
                <div>
                <h1>Cadastro</h1>
                <input type='text' name = 'nome' id='nome' placeholder='Digite o nome do personagem: ' onChange = { this.trocaValoresState } />
                <br />
                <select name="tipo" id="tipo" onChange = { this.trocaValoresState }>
                    <option value="ELFO">Elfo</option>
                    <option value="ELFOVERDE">Elfo verde</option>
                    <option value="ELFONOTURNO">Elfo noturno</option>
                    <option value="DWARF">Dwarf</option>
                    <option value="DWARFBARBALONGA">Dwarf barba longa</option>
                </select>
                <br />
                <button type='button' className = 'button button-blue' onClick={this.logar.bind(this)}> Cadastrar </button>
                </div>
                }
               {console.log(this.state)} 
            </React.Fragment>
        </div>
        
        )   
        
    }
}