import React, {Component} from 'react';
import * as axios from 'axios';

export default class Login extends Component {

    constructor(props){
        super(props)
        this.state={
            username : '',
            password : ''
        }
        this.trocaValoresState = this.trocaValoresState.bind(this)
    }

    trocaValoresState(evt){
        const  {name, value} = evt.target;
        this.setState({
            [name] : value,      
        })
    }

    logar(evt){
        evt.preventDefault();
        const { username, password } = this.state
        if(username  && password)
        {
            //executa regra de login
            axios.post('http://localhost:8080/login', {
                username: this.state.username,
                password : this.state.password
                
            }).then(
                resp=>{
                    localStorage.setItem( 'Authorization' , resp.headers.authorization )    
                    this.props.history.push('/login')   
                    var header = {
                        headers: {Authorization : localStorage.getItem('Authorization')}
                    }
                    axios.get('http://localhost:8080/api/elfo/',header)
                    .then(
                        resp=>{
                            console.log('Elfos',resp.data[0].nome)
                        })
                }  ) 
                
        }}
    logout(evt){
        evt.preventDefault();
        localStorage.removeItem('Authorization');
        this.props.history.push('/Login')       
    }

    render() {
        return (
        <div className="login flip-in-hor-bottom"> 
            <React.Fragment>
                {console.log(localStorage.getItem( 'Authorization' ))}
                {localStorage.getItem( 'Authorization' ) == null ?
                <div>
                <h1>Banco Vemser</h1>
                <input type='text' name = 'username' id='username' placeholder='Digite o seu nome' onChange = { this.trocaValoresState } />
                <br />
                <input type='password' name = 'password' id='password' placeholder='Digite sua senha' onChange = { this.trocaValoresState } />
                <br />
                <button type='button' className = 'button button-blue' onClick={this.logar.bind(this)}> Logar </button>
                </div>
                : 
                <div><h1>Logado</h1>
                <button type='button' className = 'button button-blue' onClick={this.logout.bind(this)}> Sair </button>
                </div>
                }
               {console.log(this.state)} 
            </React.Fragment>
        </div>
        
        )   
        
    }
}