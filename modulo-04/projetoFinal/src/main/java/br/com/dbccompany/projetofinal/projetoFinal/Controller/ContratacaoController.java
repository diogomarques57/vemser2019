package br.com.dbccompany.projetofinal.projetoFinal.Controller;


import br.com.dbccompany.projetofinal.projetoFinal.Entity.Contratacao;
import br.com.dbccompany.projetofinal.projetoFinal.Service.ContratacaoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping( "/api/contratacao" )
public class ContratacaoController {

    @Autowired
    ContratacaoService service;

    @GetMapping( value = "/" )
    @ResponseBody
    public List<Contratacao> todosContratacao(){
        return service.todosContratacoes();
    }

    @PostMapping( value = "/novo")
    @ResponseBody

    public Contratacao novoContratacao( @RequestBody Contratacao contratacao ) {

        return service.salvar( contratacao );
    }

    @PutMapping( value = "/editar/{id}" )
    @ResponseBody
    public Contratacao editarContratacao( @PathVariable Integer id, @RequestBody Contratacao contratacao ) {
        return service.editar( id, contratacao );
    }
}