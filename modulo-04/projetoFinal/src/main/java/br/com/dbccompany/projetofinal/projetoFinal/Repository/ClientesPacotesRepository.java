package br.com.dbccompany.projetofinal.projetoFinal.Repository;
import br.com.dbccompany.projetofinal.projetoFinal.Entity.Acessos;
import br.com.dbccompany.projetofinal.projetoFinal.Entity.ClientesPacotes;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ClientesPacotesRepository extends CrudRepository <ClientesPacotes, Integer> {
    List<ClientesPacotes> findAll();
}
