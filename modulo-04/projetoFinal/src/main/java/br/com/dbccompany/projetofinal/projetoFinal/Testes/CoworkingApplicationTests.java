package br.com.dbccompany.projetofinal.projetoFinal.Testes;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;

import br.com.dbccompany.projetofinal.projetoFinal.Entity.Clientes;
import br.com.dbccompany.projetofinal.projetoFinal.Entity.Usuarios;
import br.com.dbccompany.projetofinal.projetoFinal.Repository.UsuariosRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;
@RunWith(SpringRunner.class)
@DataJpaTest
public class CoworkingApplicationTests {
    @Autowired
    private TestEntityManager entityManager;
    @Autowired
    private UsuariosRepository usuariosRepository;
    @Test
    public void procurarUsuarioPorNome(){
        Usuarios usuarios = new Usuarios();
        usuarios.setNome("Joao");
        entityManager.persist(usuarios);
        entityManager.flush();
        Usuarios resposta = usuariosRepository.findByName(usuarios.getNome());
        assertEquals(resposta.getNome(),usuarios.getNome());
    }

    public void procurarClientesPorNome(){
        Clientes clientes = new Clientes();
        clientes.setNome("Joao");
        entityManager.persist(clientes);
        entityManager.flush();
        Usuarios resposta = usuariosRepository.findByName(clientes.getNome());
        assertEquals(resposta.getNome(),clientes.getNome());
    }
}
