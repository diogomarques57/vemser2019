package br.com.dbccompany.projetofinal.projetoFinal.Repository;
import br.com.dbccompany.projetofinal.projetoFinal.Entity.Acessos;
import br.com.dbccompany.projetofinal.projetoFinal.Entity.Contato;
import br.com.dbccompany.projetofinal.projetoFinal.Entity.Contratacao;
import br.com.dbccompany.projetofinal.projetoFinal.Entity.Espacos;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ContratacaoRepository extends CrudRepository <Contratacao, Integer> {
    List<Contratacao> findAll();
    Contratacao findByEspaco(Espacos espacos);
}
