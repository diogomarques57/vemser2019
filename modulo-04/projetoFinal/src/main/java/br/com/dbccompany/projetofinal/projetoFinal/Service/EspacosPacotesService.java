package br.com.dbccompany.projetofinal.projetoFinal.Service;


import br.com.dbccompany.projetofinal.projetoFinal.Entity.EspacosPacotes;
import br.com.dbccompany.projetofinal.projetoFinal.Entity.Util;
import br.com.dbccompany.projetofinal.projetoFinal.Repository.EspacosPacotesRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;


@Service
public class EspacosPacotesService {

    @Autowired
    private EspacosPacotesRepository espacoPacotesRepository;

    @Transactional( rollbackFor = Exception.class)
    public EspacosPacotes salvar(EspacosPacotes espacoPacotes){
        return espacoPacotesRepository.save(espacoPacotes);

    }

    @Transactional( rollbackFor = Exception.class)
    public EspacosPacotes editar( Integer id, EspacosPacotes espacoPacotes){
        espacoPacotes.setId(id);
        return espacoPacotesRepository.save(espacoPacotes);

    }

    public List<EspacosPacotes> todosEspacoPacotes(){
        return espacoPacotesRepository.findAll();
    }

    public EspacosPacotes getEspacoPacotes(Integer id){
        return espacoPacotesRepository.findById(id).get();
    }

}
