package br.com.dbccompany.projetofinal.projetoFinal.Entity;

import javax.persistence.*;
import java.time.LocalDate;
import java.io.Serializable;
import java.util.List;

@Entity
@IdClass(IdSaldoCliente.class)
public class SaldoCliente implements Serializable {
    @Id
    @ManyToOne
    @JoinColumn(name = "ID_CLIENTE")
    private Clientes clientes;

    @Id
    @ManyToOne
    @JoinColumn(name = "ID_ESPACOS")
    private Espacos espacos;

    @Column(name = "TIPO")
    private TipoContratacao tipo;

    @Column (name = "QUANTIDADE")
    private Integer quantidade;
    @Column ( name = "VENCIMENTO" )
    private LocalDate vencimento;

    @OneToMany(mappedBy = "saldoCliente")
    private List<Acessos> acessos;

    public Clientes getClientes() {
        return clientes;
    }

    public void setClientes(Clientes clientes) {
        this.clientes = clientes;
    }

    public Espacos getEspacos() {
        return espacos;
    }

    public void setEspacos(Espacos espacos) {
        this.espacos = espacos;
    }

    public TipoContratacao getTipo() {
        return tipo;
    }

    public void setTipo(TipoContratacao tipo) {
        this.tipo = tipo;
    }

    public Integer getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(Integer quantidade) {
        this.quantidade = quantidade;
    }

    public LocalDate getVencimento() {
        return vencimento;
    }

    public void setVencimento(LocalDate vencimento) {
        this.vencimento = vencimento;
    }

    public List<Acessos> getAcessos() {
        return acessos;
    }

    public void setAcessos(List<Acessos> acessos) {
        this.acessos = acessos;
    }
}
