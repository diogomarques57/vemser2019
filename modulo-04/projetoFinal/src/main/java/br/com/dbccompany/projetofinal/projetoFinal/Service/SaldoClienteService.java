package br.com.dbccompany.projetofinal.projetoFinal.Service;

import br.com.dbccompany.projetofinal.projetoFinal.Entity.*;
import br.com.dbccompany.projetofinal.projetoFinal.Repository.ClientesRepository;
import br.com.dbccompany.projetofinal.projetoFinal.Repository.EspacosRepository;
import br.com.dbccompany.projetofinal.projetoFinal.Repository.SaldoClienteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.*;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Optional;


@Service
public class SaldoClienteService {

    @Autowired
    private SaldoClienteRepository saldoClienteRepository;

    @Autowired
    private ClientesRepository clientesRepository;

    @Autowired
    private EspacosRepository espacosRepository;

    @Transactional( rollbackFor = Exception.class)
    public void salvar(Integer idCliente, Integer idEspaco, TipoContratacao tipoContratacao, Integer quantidade, Integer prazo){
        IdSaldoCliente id = new IdSaldoCliente(idCliente,idEspaco);
        Optional<SaldoCliente> saldoCliente = saldoClienteRepository.findById(id);
        SaldoCliente saldo = saldoCliente.get();
        if(saldo == null){
            saldo = new SaldoCliente();
            Clientes cliente = clientesRepository.findById(idCliente).get();
            Espacos espaco = espacosRepository.findById(idEspaco).get();
            saldo.setClientes(cliente);
            saldo.setEspacos(espaco);
        }
        Calendar calendar = Calendar.getInstance();
        if(saldo.getVencimento()==null || calendar.after(saldo.getVencimento())){
            saldo.setVencimento(calendar.getTime().toInstant()
                    .atZone(ZoneId.systemDefault())
                    .toLocalDate());
        }
        Date input = new Date();
        input.from(saldo.getVencimento().atStartOfDay().atZone(ZoneId.systemDefault()).toInstant());
        calendar.setTime(input);
        switch (tipoContratacao){
            case HORAS:
                calendar.add(Calendar.HOUR,quantidade+prazo);
                break;
            case DIARIAS:
                calendar.add(Calendar.DAY_OF_YEAR,quantidade+prazo);
                break;
            case MESES:
                calendar.add(Calendar.MONTH,quantidade+prazo);
                break;
            case MINUTOS:
                calendar.add(Calendar.MINUTE,quantidade+prazo);
                break;
            case SEMANAS:
                calendar.add(Calendar.WEEK_OF_YEAR,quantidade+prazo);
                break;
            case TURNOS:
                calendar.add(Calendar.HOUR,(quantidade*5)+prazo);
                break;
        }
        LocalDate localDate = LocalDateTime.ofInstant(calendar.toInstant(), calendar.getTimeZone().toZoneId()).toLocalDate();
        saldo.setVencimento(localDate);
        saldo.setTipo(tipoContratacao);
        if (saldo.getQuantidade() == null){
            saldo.setQuantidade(0);
        }
        saldo.setQuantidade(saldo.getQuantidade()+quantidade);
        saldoClienteRepository.save(saldo);
    }

    @Transactional( rollbackFor = Exception.class)
    public SaldoCliente editar( Integer id, SaldoCliente saldoCliente){
        //saldoCliente.setId(id);
        return saldoClienteRepository.save(saldoCliente);

    }

    public List<SaldoCliente> todosSaldoClientes(){
        return saldoClienteRepository.findAll();
    }


}
