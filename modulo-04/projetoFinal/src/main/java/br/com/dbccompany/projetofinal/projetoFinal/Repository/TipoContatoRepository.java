package br.com.dbccompany.projetofinal.projetoFinal.Repository;
import br.com.dbccompany.projetofinal.projetoFinal.Entity.ClientesPacotes;
import br.com.dbccompany.projetofinal.projetoFinal.Entity.TipoContato;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TipoContatoRepository extends CrudRepository <TipoContato, Integer> {
    List<TipoContato> findAll();
}
