package br.com.dbccompany.projetofinal.projetoFinal.Entity;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
public class Acessos {
    @SequenceGenerator(allocationSize = 1,name= "ACESSOS_SEQ", sequenceName = "ACESSOS_SEQ" )
    @GeneratedValue( generator = "ACESSOS_SEQ", strategy = GenerationType.SEQUENCE )
    @Id
    @Column ( name = "ID_ACESSOS" )
    private Integer id;

    @Column ( name = "ENTRADA")
    private Boolean is_entrada;
    @Column ( name = "DATA" )
    private LocalDate data;
    @Column (name = "EXCECAO")
    private Boolean is_excecao;



    public SaldoCliente getSaldoCliente() {
        return saldoCliente;
    }

    public void setSaldoCliente(SaldoCliente saldoCliente) {
        this.saldoCliente = saldoCliente;
    }

    @ManyToOne (fetch=FetchType.LAZY, cascade = CascadeType.ALL )
    @JoinColumns(
        {
            @JoinColumn(name = "ID_CLIENTESALDOCLIENTE"),
            @JoinColumn(name = "ID_ESPACOSALDOCLIENTE")
        }
    )

    private SaldoCliente saldoCliente;

    public Integer getEspacoSaldoCliente() {
        return espacoSaldoCliente;
    }

    public void setEspacoSaldoCliente(Integer espacoSaldoCliente) {
        this.espacoSaldoCliente = espacoSaldoCliente;
    }

    private Integer espacoSaldoCliente;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Boolean getIs_entrada() {
        return is_entrada;
    }

    public void setIs_entrada(Boolean is_entrada) {
        this.is_entrada = is_entrada;
    }

    public LocalDate getData() {
        return data;
    }

    public void setData(LocalDate data) {
        this.data = data;
    }

    public Boolean getIs_excecao() {
        return is_excecao;
    }

    public void setIs_excecao(Boolean is_excecao) {
        this.is_excecao = is_excecao;
    }


}
