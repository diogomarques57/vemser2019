package br.com.dbccompany.projetofinal.projetoFinal.Entity;

import org.hibernate.validator.constraints.Length;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import java.time.LocalDate;
import java.util.List;

@Entity

public class Usuarios {
    @SequenceGenerator(allocationSize = 1,name= "USUARIOS_SEQ", sequenceName = "USUARIOS_SEQ" )
    @GeneratedValue( generator = "USUARIOS_SEQ", strategy = GenerationType.SEQUENCE )
    @Id
    @Column(name = "ID_USUARIO")
    private Integer id;

    @NotBlank
    @Column(nullable = false, unique = true)
    private String login;

    @NotBlank
    @Pattern(regexp = "^[A-Za-z0-9]+$", message= "Senha deve possuir apenas letras ou números")
    @Length(min = 6, message = "Senha deve ter pelo menos 6 caracteres.")
    @Column(nullable = false)
    private String senha;

    @Column (name = "NOME")
    private String nome;
    @Column ( name = "CPF",length = 9)
    private String cpf;
    @Column( name = "DATA_DE_NASCIMENTO")
    private LocalDate dataNascimento;

    @OneToMany
    private List<Contato> contatos;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public LocalDate getDataNascimento() {
        return dataNascimento;
    }

    public void setDataNascimento(LocalDate dataNascimento) {
        this.dataNascimento = dataNascimento;
    }

    public List<Contato> getContatos() {
        return contatos;
    }

    public void setContatos(List<Contato> contatos) {
        this.contatos = contatos;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getSenha() {
        return senha;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }
}
