package br.com.dbccompany.projetofinal.projetoFinal.Entity;

public enum TipoContratacao {
    MINUTOS, HORAS, TURNOS, DIARIAS, SEMANAS, MESES
}
