package br.com.dbccompany.projetofinal.projetoFinal.Controller;


import br.com.dbccompany.projetofinal.projetoFinal.Entity.Pagamentos;
import br.com.dbccompany.projetofinal.projetoFinal.Service.PagamentosService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping( "/api/pagamentos" )
public class PagamentosController {

    @Autowired
    PagamentosService service;

    @GetMapping( value = "/" )
    @ResponseBody
    public List<Pagamentos> todosPagamentos(){
        return service.todosPagamentos();
    }

    @PostMapping( value = "/novo")
    @ResponseBody

    public Pagamentos novoPagamentos( @RequestBody Pagamentos pagamentos ) {

        return service.salvar( pagamentos );
    }

    @PutMapping( value = "/editar/{id}" )
    @ResponseBody
    public Pagamentos editarPagamentos( @PathVariable Integer id, @RequestBody Pagamentos pagamentos ) {
        return service.editar( id, pagamentos );
    }
}