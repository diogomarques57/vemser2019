package br.com.dbccompany.projetofinal.projetoFinal.Repository;
import br.com.dbccompany.projetofinal.projetoFinal.Entity.Acessos;
import br.com.dbccompany.projetofinal.projetoFinal.Entity.Pacotes;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PacotesRepository extends CrudRepository <Pacotes, Integer> {
    List<Pacotes> findAll();
}
