package br.com.dbccompany.projetofinal.projetoFinal.Service;
import br.com.dbccompany.projetofinal.projetoFinal.Security.*;
import br.com.dbccompany.projetofinal.projetoFinal.Entity.Usuarios;
import br.com.dbccompany.projetofinal.projetoFinal.Repository.UsuariosRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;


@Service
public class UsuariosService {

    @Autowired
    private UsuariosRepository usuariosRepository;

    @Transactional( rollbackFor = Exception.class)
    public Usuarios salvar( Usuarios usuarios){
        Criptografia psw = new Criptografia();
        usuarios.setSenha(psw.criptografar(usuarios.getSenha()));

        return usuariosRepository.save(usuarios);

    }

    @Transactional( rollbackFor = Exception.class)
    public Usuarios editar( Integer id, Usuarios usuarios){
        usuarios.setId(id);
        return usuariosRepository.save(usuarios);

    }

    public List<Usuarios> todosUsuarios(){
        return usuariosRepository.findAll();
    }

    public Usuarios getUsuarios(Integer id){
        return usuariosRepository.findById(id).get();
    }


}
