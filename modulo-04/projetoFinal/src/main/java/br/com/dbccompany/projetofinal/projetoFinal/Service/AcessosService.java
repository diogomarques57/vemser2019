package br.com.dbccompany.projetofinal.projetoFinal.Service;
import br.com.dbccompany.projetofinal.projetoFinal.Entity.Acessos;
import br.com.dbccompany.projetofinal.projetoFinal.Entity.Contratacao;
import br.com.dbccompany.projetofinal.projetoFinal.Entity.Espacos;
import br.com.dbccompany.projetofinal.projetoFinal.Entity.SaldoCliente;
import br.com.dbccompany.projetofinal.projetoFinal.Repository.AcessosRepository;

import br.com.dbccompany.projetofinal.projetoFinal.Repository.ContratacaoRepository;
import br.com.dbccompany.projetofinal.projetoFinal.Repository.EspacosRepository;
import jdk.vm.ci.meta.Local;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.awt.*;
import java.time.LocalDate;
import java.util.Calendar;
import java.util.List;
import java.util.concurrent.ExecutionException;


@Service
public class AcessosService {

    @Autowired
    private AcessosRepository acessosRepository;
    @Autowired
    private SaldoClienteService saldoClientesService;

    @Autowired
    private EspacosRepository espacosRepository;

    @Autowired
    private ContratacaoRepository contratacaoRepository;
    @Transactional( rollbackFor = Exception.class)
    public Acessos salvar( Acessos acessos) throws Exception {
        LocalDate hoje = LocalDate.now();
        SaldoCliente saldoCliente = acessos.getSaldoCliente();
        if(acessos.getData()==null){

            acessos.setData(hoje);
        }
        if(acessos.getIs_entrada()){
            LocalDate vencimento = acessos.getData();
            LocalDate dataEntrada = hoje;
            if (dataEntrada.compareTo(vencimento) <= 0 || saldoCliente.getQuantidade()<=0){
                return null;
            }
        }
        else{
            List<Acessos> acesso = acessosRepository.findAll();
            Acessos ultimoAcesso = acesso.get(acesso.size()-1);
            Calendar calendar = null;
            Integer quantidade = ultimoAcesso.getSaldoCliente().getQuantidade();
            Espacos espaco = espacosRepository.findById(ultimoAcesso.getEspacoSaldoCliente()).get();
            Contratacao cont = contratacaoRepository.findByEspaco(espaco);
            Integer prazo = cont.getPrazo();
            switch (saldoCliente.getTipo()){
                case HORAS:
                    calendar.add(Calendar.HOUR,quantidade+prazo);
                    break;
                case DIARIAS:
                    calendar.add(Calendar.DAY_OF_YEAR,quantidade+prazo);
                    break;
                case MESES:
                    calendar.add(Calendar.MONTH,quantidade+prazo);
                    break;
                case MINUTOS:
                    calendar.add(Calendar.MINUTE,quantidade+prazo);
                    break;
                case SEMANAS:
                    calendar.add(Calendar.WEEK_OF_YEAR,quantidade+prazo);
                    break;
                case TURNOS:
                    calendar.add(Calendar.HOUR,(quantidade*5)+prazo);
                    break;
            }
        }
        return acessosRepository.save(acessos);

    }

    @Transactional( rollbackFor = Exception.class)
    public Acessos editar( Integer id, Acessos acessos){
        acessos.setId(id);
        return acessosRepository.save(acessos);

    }

    public List<Acessos> todosAcessos(){
        return acessosRepository.findAll();
    }

    public Acessos getAcessos(Integer id){
        return acessosRepository.findById(id).get();
    }

}
