package br.com.dbccompany.projetofinal.projetoFinal.Entity;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.temporal.TemporalAmount;
import java.util.Calendar;
import java.util.Date;

public class Util {
    public LocalDate converter(LocalDate data, TipoContratacao tipo, Integer quantidade, Integer prazo)
    {
        Calendar calendar = null;
        switch (tipo)
        {
            case HORAS: calendar.add(Calendar.HOUR, quantidade + prazo);
            break;
            case MESES: calendar.add(Calendar.MONTH, quantidade + prazo);
            break;
            case TURNOS: calendar.add(Calendar.HOUR,(quantidade*5) + prazo);
            break;
            case DIARIAS: calendar.add(Calendar.DAY_OF_MONTH, quantidade + prazo);
            break;
            case SEMANAS: calendar.add(Calendar.WEEK_OF_MONTH, quantidade + prazo);
            break;

        }
        data = LocalDateTime.ofInstant(calendar.toInstant(), calendar.getTimeZone().toZoneId()).toLocalDate();
        return data;
    }

}
