package br.com.dbccompany.projetofinal.projetoFinal.Service;

import br.com.dbccompany.projetofinal.projetoFinal.Entity.Pacotes;
import br.com.dbccompany.projetofinal.projetoFinal.Repository.PacotesRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;


@Service
public class PacotesService {

    @Autowired
    private PacotesRepository pacotesRepository;

    @Transactional( rollbackFor = Exception.class)
    public Pacotes salvar( Pacotes pacotes){
        Double total = 0.00;
        for (int i = 0; i < pacotes.getEspacosPacotes().size(); i++) {
           total += pacotes.getEspacosPacotes().get(i).getEspaco().getValor();
        }
        pacotes.setValor(total);

        return pacotesRepository.save(pacotes);

    }

    @Transactional( rollbackFor = Exception.class)
    public Pacotes editar( Integer id, Pacotes pacotes){
        pacotes.setId(id);
        return pacotesRepository.save(pacotes);

    }

    public List<Pacotes> todosPacotes(){
        return pacotesRepository.findAll();
    }

    public Pacotes getPacotes(Integer id){
        return pacotesRepository.findById(id).get();
    }

}
