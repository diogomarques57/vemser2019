package br.com.dbccompany.projetofinal.projetoFinal.Entity;

import javax.persistence.*;

@Entity

public class EspacosPacotes {
    @Id
    @Column ( name = "ID_ESPACOSXPACOTES")
    private Integer id;


    @Column ( name = "TIPO_CONTRATACAO")
    private TipoContratacao tipo;

    @Column ( name = "QUANTIDADE")
    private Integer quantidade;

    @Column ( name = "PRAZO" )
    private Integer prazo;

    @ManyToOne
    @JoinColumn(name = "ID_ESPACO")
    private Espacos espaco;

    @ManyToOne
    @JoinColumn(name = "ID_PACOTE")
    private Pacotes pacotes;

    public Espacos getEspaco() {
        return espaco;
    }

    public void setEspaco(Espacos espaco) {
        this.espaco = espaco;
    }

    public Pacotes getPacotes() {
        return pacotes;
    }

    public void setPacotes(Pacotes pacotes) {
        this.pacotes = pacotes;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public TipoContratacao getTipo() {
        return tipo;
    }

    public void setTipo(TipoContratacao tipo) {
        this.tipo = tipo;
    }

    public Integer getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(Integer quantidade) {
        this.quantidade = quantidade;
    }

    public Integer getPrazo() {
        return prazo;
    }

    public void setPrazo(Integer prazo) {
        this.prazo = prazo;
    }
}
