package br.com.dbccompany.projetofinal.projetoFinal.Controller;


import br.com.dbccompany.projetofinal.projetoFinal.Entity.SaldoCliente;
import br.com.dbccompany.projetofinal.projetoFinal.Service.SaldoClienteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping( "/api/saldoCliente" )
public class SaldoClienteController {

    @Autowired
    SaldoClienteService service;

    @GetMapping( value = "/" )
    @ResponseBody
    public List<SaldoCliente> todosSaldoCliente(){
        return service.todosSaldoClientes();
    }

    @PostMapping( value = "/novo")
    @ResponseBody

    public SaldoCliente novoSaldoCliente( @RequestBody SaldoCliente saldoCliente ) {

        return service.salvar( saldoCliente );
    }

    @PutMapping( value = "/editar/{id}" )
    @ResponseBody
    public SaldoCliente editarSaldoCliente( @PathVariable Integer id, @RequestBody SaldoCliente saldoCliente ) {
        return service.editar( id, saldoCliente );
    }
}