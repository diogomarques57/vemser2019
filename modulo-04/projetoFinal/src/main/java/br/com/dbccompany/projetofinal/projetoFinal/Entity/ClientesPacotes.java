package br.com.dbccompany.projetofinal.projetoFinal.Entity;

import javax.persistence.*;

@Entity
public class ClientesPacotes {
    @SequenceGenerator(allocationSize = 1,name= "CLIENTES_SEQ", sequenceName = "CLIENTES_SEQ" )

    @GeneratedValue( generator = "CLIENTES_SEQ", strategy = GenerationType.SEQUENCE )

    @Id
    @Column( name = "ID_CLIENTES_X_PACOTES" )
    Integer id;

    @Column ( name = "QUANTIDADE")
    Integer quantidade;

    @ManyToOne
    @JoinColumn(name = "ID_CLIENTE")
    private Clientes clientes;

    @ManyToOne
    @JoinColumn(name = "ID_PACOTE")
    private Pacotes pacotes;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(Integer quantidade) {
        this.quantidade = quantidade;
    }

    public Clientes getClientes() {
        return clientes;
    }

    public void setClientes(Clientes clientes) {
        this.clientes = clientes;
    }

    public Pacotes getPacotes() {
        return pacotes;
    }

    public void setPacotes(Pacotes pacotes) {
        this.pacotes = pacotes;
    }


}
