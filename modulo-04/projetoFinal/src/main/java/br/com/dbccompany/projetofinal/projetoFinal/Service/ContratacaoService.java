package br.com.dbccompany.projetofinal.projetoFinal.Service;

import br.com.dbccompany.projetofinal.projetoFinal.*;
import br.com.dbccompany.projetofinal.projetoFinal.Entity.Contratacao;
import br.com.dbccompany.projetofinal.projetoFinal.Entity.SaldoCliente;
import br.com.dbccompany.projetofinal.projetoFinal.Repository.ContratacaoRepository;
import br.com.dbccompany.projetofinal.projetoFinal.Repository.SaldoClienteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.util.List;


@Service
public class ContratacaoService {

    @Autowired
    private ContratacaoRepository contratacaoRepository;

    @Autowired
    private SaldoClienteRepository saldoClienteRepository;

    @Transactional( rollbackFor = Exception.class)
    public Contratacao salvar( Contratacao contratacao){
        SaldoCliente sC = new SaldoCliente();
        LocalDate lD = LocalDate.now();
        double valor = contratacao.getEspacos().getValor();
        Integer quantidade = contratacao.getQuantidade();
        lD.plusDays(contratacao.getPrazo() + lD.getDayOfMonth());
        sC.setVencimento(lD);
        contratacao.getClientes().getSaldoClientes().add(sC);
        saldoClienteRepository.save(sC);
        return contratacaoRepository.save(contratacao);

    }

    @Transactional( rollbackFor = Exception.class)
    public Contratacao editar( Integer id, Contratacao contratacao){
        contratacao.setId(id);
        Double total = contratacao.getQuantidade() * contratacao.getEspacos().getValor() ;

        return contratacaoRepository.save(contratacao);

    }

    public List<Contratacao> todosContratacoes(){
        return contratacaoRepository.findAll();
    }

    public Contratacao getContratacao(Integer id){
        return contratacaoRepository.findById(id).get();
    }

}
