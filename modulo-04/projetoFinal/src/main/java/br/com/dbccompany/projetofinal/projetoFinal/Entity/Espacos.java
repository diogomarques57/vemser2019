package br.com.dbccompany.projetofinal.projetoFinal.Entity;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import java.util.List;

@Entity

public class Espacos {
    @SequenceGenerator(allocationSize = 1,name= "ESPACOS_SEQ", sequenceName = "ESPACOS_SEQ" )
    @GeneratedValue( generator = "ESPACOS_SEQ", strategy = GenerationType.SEQUENCE )
    @Id
    @Column (name= "ID_ESPACOS")
    private Integer id;

    @NotBlank
    @Column (name = "NOME",unique = true)
    private String nome;

    @OneToMany(mappedBy="espacos")
    private List<SaldoCliente> saldoClientes;

    @NotBlank
    @Column (name = "QUANTIDADEDEPESSOAS")
    private Integer qtdPessoas;

    @NotBlank
    @Column (name = "VALOR")
    private Double valor;

    @OneToMany
    private List<Contratacao> contratacao;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Integer getQtdPessoas() {
        return qtdPessoas;
    }

    public void setQtdPessoas(Integer qtdPessoas) {
        this.qtdPessoas = qtdPessoas;
    }

    public Double getValor() {
        return valor;
    }

    public void setValor(Double valor) {
        this.valor = valor;
    }

    public List<Contratacao> getContratacao() {
        return contratacao;
    }

    public void setContratacao(List<Contratacao> contratacao) {
        this.contratacao = contratacao;
    }

    public List<SaldoCliente> getSaldoClientes() {
        return saldoClientes;
    }

    public void setSaldoClientes(List<SaldoCliente> saldoClientes) {
        this.saldoClientes = saldoClientes;
    }


    
}
