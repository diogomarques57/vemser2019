package br.com.dbccompany.projetofinal.projetoFinal.Entity;

import javax.persistence.*;


@Entity

public class Pagamentos {
    @SequenceGenerator(allocationSize = 1,name= "PAGAMENTOS_SEQ", sequenceName = "PAGAMENTOS_SEQ" )
    @GeneratedValue( generator = "PAGAMENTOS_SEQ", strategy = GenerationType.SEQUENCE )
    @Id
    @Column(name="ID_PAGAMENTOS")
    private Integer id;

    @ManyToOne
    @JoinColumn(name = "ID_CLIENTES_PACOTES")
    private ClientesPacotes clientesPacotes;

    @ManyToOne
    @JoinColumn(name = "ID_CONTRATACAO")
    private Contratacao contratacao;

    public ClientesPacotes getClientesPacotes() {
        return clientesPacotes;
    }

    public void setClientesPacotes(ClientesPacotes clientesPacotes) {
        this.clientesPacotes = clientesPacotes;
    }

    public Contratacao getContratacao() {
        return contratacao;
    }

    public void setContratacao(Contratacao contratacao) {
        this.contratacao = contratacao;
    }

    //id_clientes_pacotes
    //id_contratacao
    @Column(name = "TIPO")
    private TipoPagamento tipo;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public TipoPagamento getTipo() {
        return tipo;
    }

    public void setTipo(TipoPagamento tipo) {
        this.tipo = tipo;
    }
}
