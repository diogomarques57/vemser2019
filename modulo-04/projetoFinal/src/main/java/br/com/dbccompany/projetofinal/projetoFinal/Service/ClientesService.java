package br.com.dbccompany.projetofinal.projetoFinal.Service;

import br.com.dbccompany.projetofinal.projetoFinal.Entity.Clientes;
import br.com.dbccompany.projetofinal.projetoFinal.Repository.ClientesRepository;
import br.com.dbccompany.projetofinal.projetoFinal.Repository.ContatoRepository;
import br.com.dbccompany.projetofinal.projetoFinal.Repository.TipoContatoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;


@Service
public class ClientesService {

    @Autowired
    private ClientesRepository clientesRepository;
    @Autowired
    private ContatoRepository contatoRepository;
    @Autowired
    private TipoContatoRepository tipoContatoRepository;

    @Transactional( rollbackFor = Exception.class)
    public Clientes salvar( Clientes clientes){
        Boolean ContatoValido = false;

        if(clientes.getContatos() != null) {

            tipoContatoRepository.save(clientes.getContatos().getTipo());

          if(clientes.getContatos().getTipo().getNome().contains( "telefone") || clientes.getContatos().getTipo().getNome().contains("email"))
          {

              contatoRepository.save(clientes.getContatos());
              ContatoValido = true;
          }
        }
        if(ContatoValido) {
            return clientesRepository.save(clientes);
        }
        return null;
    }

    @Transactional( rollbackFor = Exception.class)
    public Clientes editar( Integer id, Clientes clientes){
        clientes.setId(id);
        return clientesRepository.save(clientes);

    }

    public List<Clientes> todosClientes(){
        return clientesRepository.findAll();
    }

    public Clientes getClientes(Integer id){
        return clientesRepository.findById(id).get();
    }

}
