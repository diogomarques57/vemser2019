package br.com.dbccompany.projetofinal.projetoFinal.Repository;
import br.com.dbccompany.projetofinal.projetoFinal.Entity.Acessos;
import br.com.dbccompany.projetofinal.projetoFinal.Entity.IdSaldoCliente;
import br.com.dbccompany.projetofinal.projetoFinal.Entity.Pagamentos;
import br.com.dbccompany.projetofinal.projetoFinal.Entity.SaldoCliente;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface SaldoClienteRepository extends CrudRepository <SaldoCliente, IdSaldoCliente> {
    List<SaldoCliente> findAll();
}
