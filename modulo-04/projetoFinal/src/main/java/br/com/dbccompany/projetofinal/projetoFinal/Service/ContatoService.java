package br.com.dbccompany.projetofinal.projetoFinal.Service;

import br.com.dbccompany.projetofinal.projetoFinal.Entity.Contato;
import br.com.dbccompany.projetofinal.projetoFinal.Repository.ContatoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;


@Service
public class ContatoService {

    @Autowired
    private ContatoRepository contatoRepository;

    @Transactional( rollbackFor = Exception.class)
    public Contato salvar( Contato contato){

        return contatoRepository.save(contato);

    }

    @Transactional( rollbackFor = Exception.class)
    public Contato editar( Integer id, Contato contato){
        contato.setId(id);
        return contatoRepository.save(contato);

    }

    public List<Contato> todosContatos(){
        return contatoRepository.findAll();
    }

    public Contato getContato(Integer id){
        return contatoRepository.findById(id).get();
    }

}
