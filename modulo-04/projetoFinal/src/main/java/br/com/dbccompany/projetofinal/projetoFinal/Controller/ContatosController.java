package br.com.dbccompany.projetofinal.projetoFinal.Controller;


import br.com.dbccompany.projetofinal.projetoFinal.Entity.Contato;
import br.com.dbccompany.projetofinal.projetoFinal.Service.ContatoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping( "/api/contatos" )
public class ContatosController {

    @Autowired
    ContatoService service;

    @GetMapping( value = "/" )
    @ResponseBody
    public List<Contato> todosContato(){
        return service.todosContatos();
    }

    @PostMapping( value = "/novo")
    @ResponseBody

    public Contato novoContato( @RequestBody Contato contatos ) {

        return service.salvar( contatos );
    }

    @PutMapping( value = "/editar/{id}" )
    @ResponseBody
    public Contato editarContato( @PathVariable Integer id, @RequestBody Contato contatos ) {
        return service.editar( id, contatos );
    }
}