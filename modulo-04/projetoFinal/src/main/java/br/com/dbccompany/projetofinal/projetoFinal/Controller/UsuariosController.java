package br.com.dbccompany.projetofinal.projetoFinal.Controller;


import br.com.dbccompany.projetofinal.projetoFinal.Entity.Usuarios;
import br.com.dbccompany.projetofinal.projetoFinal.Service.UsuariosService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping( "/api/usuarios" )
public class UsuariosController {

    @Autowired
    UsuariosService service;

    @GetMapping( value = "/" )
    @ResponseBody
    public List<Usuarios> todosUsuarios(){
        return service.todosUsuarios();
    }

    @PostMapping( value = "/novo")
    @ResponseBody

    public Usuarios novoUsuarios( @RequestBody Usuarios usuarios ) {

        return service.salvar( usuarios );
    }

    @PutMapping( value = "/editar/{id}" )
    @ResponseBody
    public Usuarios editarUsuarios( @PathVariable Integer id, @RequestBody Usuarios usuarios ) {
        return service.editar( id, usuarios );
    }
}