package br.com.dbccompany.projetofinal.projetoFinal.Entity;

import com.sun.javafx.beans.IDProperty;

import javax.persistence.*;
import java.util.List;

@Entity

public class Pacotes {
    @SequenceGenerator(allocationSize = 1,name= "PACOTE_SEQ", sequenceName = "PACOTE_SEQ" )
    @GeneratedValue( generator = "PACOTE_SEQ", strategy = GenerationType.SEQUENCE )
    @Id
    @Column(name = "ID_PACOTE")
    private Integer id;

    public List<ClientesPacotes> getClientesPacotes() {
        return clientesPacotes;
    }

    public void setClientesPacotes(List<ClientesPacotes> clientesPacotes) {
        this.clientesPacotes = clientesPacotes;
    }

    public List<EspacosPacotes> getEspacosPacotes() {
        return espacosPacotes;
    }

    public void setEspacosPacotes(List<EspacosPacotes> espacosPacotes) {
        this.espacosPacotes = espacosPacotes;
    }

    public List<ClientesPacotes> getClientes() {
        return clientes;
    }

    public void setClientes(List<ClientesPacotes> clientes) {
        this.clientes = clientes;
    }

    public List<EspacosPacotes> getEspacos() {
        return espacos;
    }

    public void setEspacos(List<EspacosPacotes> espacos) {
        this.espacos = espacos;
    }

    @OneToMany
    @Column (name = "CLIENTES_X_PACOTES")
    private List<ClientesPacotes> clientesPacotes;

    @OneToMany
    @Column(name = "ESPACOS_X_PACOTES")
    private List<EspacosPacotes> espacosPacotes;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Double getValor() {
        return valor;
    }

    public void setValor(Double valor) {
        this.valor = valor;
    }

    @Column(name="VALOR")
    private Double valor;

    @OneToMany
    private List<ClientesPacotes> clientes;
    @OneToMany
    private List<EspacosPacotes> espacos;
}
