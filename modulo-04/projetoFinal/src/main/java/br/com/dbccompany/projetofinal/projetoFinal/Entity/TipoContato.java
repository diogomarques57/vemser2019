package br.com.dbccompany.projetofinal.projetoFinal.Entity;

import com.fasterxml.jackson.annotation.JsonManagedReference;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
public class TipoContato {
    @SequenceGenerator(allocationSize = 1,name= "TIPOCONTATO_SEQ", sequenceName = "TIPOCONTATO_SEQ" )
    @GeneratedValue( generator = "TIPOCONTATO_SEQ", strategy = GenerationType.SEQUENCE )
    @Id
    @Column(name = "ID_TIPOCONTATO")
    private Integer id;

    @Column(name = "NOME")
    private String nome;

    @OneToMany( cascade = CascadeType.ALL, mappedBy = "tipo")
    @JsonManagedReference
    private List<Contato> contatos;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public List<Contato> getContatos() {
        return contatos;
    }

    public void setContatos(List<Contato> contatos) {
        this.contatos = contatos;
    }
}
