package br.com.dbccompany.projetofinal.projetoFinal.Controller;


import br.com.dbccompany.projetofinal.projetoFinal.Entity.Acessos;
import br.com.dbccompany.projetofinal.projetoFinal.Service.AcessosService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping( "/api/acessos" )
public class AcessosController {

    @Autowired
    AcessosService service;

    @GetMapping( value = "/" )
    @ResponseBody
    public List<Acessos> todosAcessos(){
        return service.todosAcessos();
    }

    @PostMapping( value = "/novo")
    @ResponseBody

    public Acessos novoAcessos( @RequestBody Acessos acessos ) {

        return service.salvar( acessos );
    }

    @PutMapping( value = "/editar/{id}" )
    @ResponseBody
    public Acessos editarAcessos( @PathVariable Integer id, @RequestBody Acessos acessos ) {
        return service.editar( id, acessos );
    }
}