package br.com.dbccompany.projetofinal.projetoFinal.Entity;

import javax.persistence.*;

@Entity
public class Contratacao {
    @SequenceGenerator(allocationSize = 1,name= "CONTRATACAO_SEQ", sequenceName = "CONTRATACAO_SEQ" )
    @GeneratedValue( generator = "CONTRATACAO_SEQ", strategy = GenerationType.SEQUENCE )

    @Id
    @Column(name = "ID_CONTRATACAO")
    private Integer id;

    @ManyToOne
    @JoinColumn(name="ID_CLIENTE", nullable=false)
    private Clientes clientes;


    @ManyToOne
    @JoinColumn(name = "ID_ESPACO", nullable = false)
    private Espacos espacos;

    //id Espacos
    //id Clientes


    @Enumerated(EnumType.STRING)
    @Column(name = "TIPO")
    private TipoContratacao tipo;
    @Column(name = "QUANTIDADE")
    private Integer quantidade;

    @Column(name = "PRAZO")
    private Integer prazo;

    @Column(name = "DESCONTO",nullable = true)
    private Integer desconto;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Clientes getClientes() {
        return clientes;
    }

    public void setClientes(Clientes clientes) {
        this.clientes = clientes;
    }

    public Espacos getEspacos() {
        return espacos;
    }

    public void setEspacos(Espacos espacos) {
        this.espacos = espacos;
    }

    public TipoContratacao getTipo() {
        return tipo;
    }

    public void setTipo(TipoContratacao tipo) {
        this.tipo = tipo;
    }

    public Integer getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(Integer quantidade) {
        this.quantidade = quantidade;
    }

    public Integer getDesconto() {
        return desconto;
    }

    public void setDesconto(Integer desconto) {
        this.desconto = desconto;
    }

    public Integer getPrazo() {
        return prazo;
    }

    public void setPrazo(Integer prazo) {
        this.prazo = prazo;
    }


}
