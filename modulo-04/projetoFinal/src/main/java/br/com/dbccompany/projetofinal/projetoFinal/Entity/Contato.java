package br.com.dbccompany.projetofinal.projetoFinal.Entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import javax.persistence.*;
import java.util.List;

@Entity
public class Contato {
    @SequenceGenerator(allocationSize = 1,name= "CONTATO_SEQ", sequenceName = "CONTATO_SEQ" )
    @GeneratedValue( generator = "CONTATO_SEQ", strategy = GenerationType.SEQUENCE )
    @Id
    @Column (name = "ID_CONTATO")
    private Integer id;
    @Column (name = "VALOR")
    private String valor;

    @ManyToOne
    @JsonBackReference
    @JoinColumn(name = "TIPO")
    private TipoContato tipo;

    @OneToMany
    @JsonManagedReference
    private List<Clientes> clientes;

    public TipoContato getTipo() {
        return tipo;
    }

    public void setTipo(TipoContato tipo) {
        this.tipo = tipo;
    }
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getValor() {
        return valor;
    }

    public void setValor(String valor) {
        this.valor = valor;
    }


    public List<Clientes> getClientes() {
        return clientes;
    }

    public void setClientes(List<Clientes> clientes) {
        this.clientes = clientes;
    }
}
