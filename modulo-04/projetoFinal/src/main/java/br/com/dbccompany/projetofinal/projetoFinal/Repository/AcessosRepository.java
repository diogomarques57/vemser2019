package br.com.dbccompany.projetofinal.projetoFinal.Repository;
import br.com.dbccompany.projetofinal.projetoFinal.Entity.Acessos;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import br.com.dbccompany.projetofinal.projetoFinal.Entity.*;

import java.util.List;

@Repository
public interface AcessosRepository extends CrudRepository <Acessos, Integer> {
    List<Acessos> findAll();
}
