package br.com.dbccompany.projetofinal.projetoFinal.Entity;

import com.fasterxml.jackson.annotation.JsonBackReference;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import java.time.LocalDate;
import java.util.Date;
import java.util.List;

@Entity
public class Clientes {
    @SequenceGenerator(allocationSize = 1,name= "CLIENTES_SEQ", sequenceName = "CLIENTES_SEQ" )
    @GeneratedValue( generator = "CLIENTES_SEQ", strategy = GenerationType.SEQUENCE )
    @Id
    @Column(name = "ID_CLIENTE")
    private Integer id;

    @NotBlank
    @Column(name = "NOME")
    private String nome;

    @NotBlank
    @Column(name = "CPF", unique = true, nullable = false)
    private String cpf;

    @Column(name = "DATADENASCIMENTO")
    private LocalDate datanasc;

    @ManyToOne
    @JoinColumn(name="ID_CONTATO")
    @JsonBackReference
    private Contato contatos;

    public List<ClientesPacotes> getClientesPacotes() {
        return clientesPacotes;
    }

    public void setClientesPacotes(List<ClientesPacotes> clientesPacotes) {
        this.clientesPacotes = clientesPacotes;
    }

    @OneToMany
    private List<Acessos> acessos;

    @OneToMany
    private List<Contratacao> contratacao;

    @OneToMany
    private List<ClientesPacotes> clientesPacotes;

    @OneToMany(mappedBy="clientes")
    private List<SaldoCliente> saldoClientes;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public LocalDate getDatanasc() {
        return datanasc;
    }

    public void setDatanasc(LocalDate datanasc) {
        this.datanasc = datanasc;
    }

    public Contato getContatos() {
        return contatos;
    }

    public void setContatos(Contato contatos) {
        this.contatos = contatos;
    }

    public List<Contratacao> getContratacao() {
        return contratacao;
    }

    public void setContratacao(List<Contratacao> contratacao) {
        this.contratacao = contratacao;
    }

    public List<SaldoCliente> getSaldoClientes() {
        return saldoClientes;
    }

    public void setSaldoClientes(List<SaldoCliente> saldoClientes) {
        this.saldoClientes = saldoClientes;
    }

    public List<Acessos> getAcessos() {
        return acessos;
    }

    public void setAcessos(List<Acessos> acessos) {
        this.acessos = acessos;
    }
}
