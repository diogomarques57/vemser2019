package br.com.dbccompany.projetofinal.projetoFinal.Entity;

import br.com.dbccompany.projetofinal.projetoFinal.Entity.Clientes;
import br.com.dbccompany.projetofinal.projetoFinal.Entity.Espacos;

import java.io.Serializable;

public class IdSaldoCliente implements Serializable {
    private Integer clientes;
    private Integer espacos;

    // must have a default construcot
    public IdSaldoCliente() { }

    public IdSaldoCliente(Integer cliente, Integer espaco) {
        this.clientes = cliente;
        this.espacos = espaco;
    }

    public Integer getClientes() {
        return clientes;
    }

    public void setClientes(Integer clientes) {
        this.clientes = clientes;
    }

    public Integer getEspacos() {
        return espacos;
    }

    public void setEspacos(Integer espacos) {
        this.espacos = espacos;
    }
}