package br.com.dbccompany.projetofinal.projetoFinal.Service;

import br.com.dbccompany.projetofinal.projetoFinal.Entity.*;
import br.com.dbccompany.projetofinal.projetoFinal.Repository.PagamentosRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;


@Service
public class PagamentosService {

    @Autowired
    private PagamentosRepository pagamentosRepository;
    @Autowired
    private SaldoClienteService saldoClienteService;

    @Transactional( rollbackFor = Exception.class)
    public Pagamentos salvar( Pagamentos pagamentos){
        Contratacao contratacao = pagamentos.getContratacao();
        ClientesPacotes clientesPacotes = pagamentos.getClientesPacotes();
        if(contratacao != null){
            saldoClienteService.salvar(contratacao.getClientes().getId(),contratacao.getEspacos().getId(),contratacao.getTipo(),contratacao.getQuantidade(),contratacao.getPrazo());
        }
        if (clientesPacotes != null){
            Clientes cliente = clientesPacotes.getClientes();
            List<EspacosPacotes> espacosPacotes = clientesPacotes.getPacotes().getEspacosPacotes();
            for (EspacosPacotes espacoPacote : espacosPacotes) {
                saldoClienteService.salvar(cliente.getId(),espacoPacote.getEspaco().getId(),espacoPacote.getTipo(),espacoPacote.getQuantidade(),espacoPacote.getPrazo());
            }
        }
        return pagamentosRepository.save(pagamentos);

    }

    @Transactional( rollbackFor = Exception.class)
    public Pagamentos editar( Integer id, Pagamentos pagamentos){
        pagamentos.setId(id);
        return pagamentosRepository.save(pagamentos);

    }

    public List<Pagamentos> todosPagamentos(){
        return pagamentosRepository.findAll();
    }

    public Pagamentos getPagamentos(Integer id){
        return pagamentosRepository.findById(id).get();
    }

}
