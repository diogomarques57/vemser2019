package br.com.dbccompany.vemserSpring.Repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import br.com.dbccompany.vemserSpring.Entity.*;

import java.util.List;

@Repository
public interface InventarioRepository extends CrudRepository <Inventario, Integer> {
    List<Inventario> findAll();

}
