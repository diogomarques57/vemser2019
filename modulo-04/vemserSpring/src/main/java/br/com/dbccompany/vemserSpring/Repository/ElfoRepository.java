package br.com.dbccompany.vemserSpring.Repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import br.com.dbccompany.vemserSpring.Entity.*;

import java.util.List;

@Repository
public interface ElfoRepository extends CrudRepository <Elfo, Integer> {
    Elfo findByVida( Double vida );
    List<Elfo> findAll();
}
