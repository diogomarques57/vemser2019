package br.com.dbccompany.vemserSpring.Entity;

import javax.persistence.Entity;

@Entity
public class ElfoNoturno extends Elfo {

    public ElfoNoturno(){
        super.setStatus(Status.RECEM_CRIADO);
        super.setTipo(TipoPersonagem.ELFONOTURNO);
        this.qtdXpPorAtaque = 3;
        this.qtdDanoRecebidoPorAtaque = 15.0;
        super.setVida(90.00);
    }

}

