package br.com.dbccompany.vemserSpring.Entity;

import javax.persistence.Entity;

@Entity
public class Dwarf extends Personagem{
    Inventario inventario;

    private boolean escudoEquipado = false;

    public Dwarf(){
        tipo = TipoPersonagem.DWARF;
        super.setClasse();
        super.setVida(180.00);
    }


    public void desequiparEscudo(){
        this.escudoEquipado = false;
    }




    public String imprimirResultado(){
        return "Dwarf";
    }

}