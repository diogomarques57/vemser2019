package br.com.dbccompany.vemserSpring.Entity;
import java.util.*;
import javax.persistence.*;

@Entity
public class Item{
    @SequenceGenerator(allocationSize = 1,name= "ITEM_SEQ", sequenceName = "ITEM_SEQ" )
    @GeneratedValue( generator = "ITEM_SEQ", strategy = GenerationType.SEQUENCE )
    @Id
    @Column (name = "ID_ITEM")
    private Integer id;

    @OneToMany(mappedBy="item")
    private List<InventarioXItem> lstInventarioItem;

    private Integer quantidade;

    @Column (name = "DESCRICAO")
    private String descricao;

    @Column (name = "TIPO")
    private TipoItem tipo;

    public Integer getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public TipoItem getTipo() {
        return tipo;
    }

    public void setTipo(TipoItem tipo) {
        this.tipo = tipo;
    }

    public List<InventarioXItem> getLstInventarioItem() {
        return lstInventarioItem;
    }

    public void setLstInventarioItem(List<InventarioXItem> lstInventarioItem) {
        this.lstInventarioItem = lstInventarioItem;
    }

    public int getQuantidade(){
        return this.quantidade;
    }

    public void setQuantidade(int quantidade){
        this.quantidade = quantidade;
    }

    public String getDescricao(){
        return this.descricao;
    }

    public void setDescricao(String descricao){
        this.descricao = descricao;
    }

    public boolean equals(Object obj){
        Item outroItem = (Item) obj;
        return this.getQuantidade() == outroItem.getQuantidade() && this.getDescricao().equals(outroItem.getDescricao());
    }

}
