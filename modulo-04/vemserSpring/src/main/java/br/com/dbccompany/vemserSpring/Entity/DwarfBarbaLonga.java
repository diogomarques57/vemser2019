package br.com.dbccompany.vemserSpring.Entity;

import javax.persistence.Entity;

@Entity
public class DwarfBarbaLonga extends Dwarf
{

    public DwarfBarbaLonga(){
        super.setTipo(TipoPersonagem.DWARF);
        super.setStatus(Status.RECEM_CRIADO);
        super.setVida(200.00);
    }

    public void sofrerDano(double dano){
        DadoD6 dado = new DadoD6();
        if(dado.sortear()<5){
            super.sofrerDano(dano);
        }
    }
}