package br.com.dbccompany.vemserSpring.Entity;

import javax.persistence.Entity;
import java.util.*;

@Entity
public class ElfoVerde extends Elfo {

    private final ArrayList<String> DESCRICOES_VALIDAS = new ArrayList<>(
            Arrays.asList(
                    "Espada de aço valariano",
                    "Flecha de vidro",
                    "Arco de vidro"
            )
    );

    public ElfoVerde(){
        tipo = TipoPersonagem.ELFOVERDE;
        this.qtdXpPorAtaque = 2;
        super.setVida(110.00);
    }




}

