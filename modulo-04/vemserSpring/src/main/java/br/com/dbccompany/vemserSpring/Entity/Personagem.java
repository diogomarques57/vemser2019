package br.com.dbccompany.vemserSpring.Entity;

import org.hibernate.engine.internal.Cascade;

import javax.persistence.*;
import javax.persistence.Entity;


@Entity
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
public abstract class Personagem {
    @SequenceGenerator(allocationSize = 1,name= "PERSONAGENS_SEQ", sequenceName = "PERSONAGENS_SEQ" )
    @GeneratedValue( generator = "PERSONAGENS_SEQ", strategy = GenerationType.SEQUENCE )
    @Id
    @Column (name = "ID_PERSONAGENS")
    private Integer id;
    @Column (name = "NOME")
    private String nome;
    @Column (name = "STATUS")
    private Status status;
    @Column (name = "VIDA")
    protected double vida;

    private String classe;

    @Column (name = "EXPERIENCIA")
    protected int experiencia;

    @Enumerated(EnumType.STRING)
    @Column (name = "TIPO_PERSONAGEM")
    protected TipoPersonagem tipo;

    @JoinColumn(name = "ID_INVENTARIO")
    @OneToOne(cascade = CascadeType.ALL)
    protected Inventario inventario;

    public void setClasse() {
        this.classe = this.getClass().getSimpleName();
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public TipoPersonagem getTipo() {
        return tipo;
    }

    public void setInventario(Inventario inventario) {
        this.inventario = inventario;
    }

    public void setTipo(TipoPersonagem tipo) {
        this.tipo = tipo;
    }

    protected int qtdXpPorAtaque;
    protected double qtdDanoRecebidoPorAtaque;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }


    public String getNome(){
        return nome;
    }

    public void setNome(String nome){
        this.nome = nome;
    }

    public double getVida(){
        return vida;
    }

    protected void setVida(double qtdVida){
        this.vida = qtdVida;
    }

    protected void curarVida(double cura){
        this.vida += cura;
    }

    public Status getStatus(){
        return this.status;
    }

    public int getExperiencia(){
        return this.experiencia;
    }

    protected void aumentarXp(){
        experiencia +=this.qtdXpPorAtaque;
    }



    protected Inventario getInventario(){
        return this.inventario;
    }

    public String getClasse(){
        return this.getClass().getSimpleName();
    }

    protected boolean podeSofrerDano(){
        return this.vida > 0;
    }

    protected void sofrerDano(double dano){
        if(podeSofrerDano() && (this.status != Status.MORTO)){

            this.vida -= this.vida >= dano ? dano : this.vida;
            if(this.vida <=0){
                status= Status.MORTO;
            }
            else{
                status = Status.SOFREU_DANO;
            }
        }
    }

    //protected abstract String imprimirResultado();
}
