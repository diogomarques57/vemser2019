package br.com.dbccompany.vemserSpring.Service;

import br.com.dbccompany.vemserSpring.Entity.Inventario;
import br.com.dbccompany.vemserSpring.Entity.InventarioXItem;
import br.com.dbccompany.vemserSpring.Entity.Item;
import br.com.dbccompany.vemserSpring.Repository.InventarioRepository;
import br.com.dbccompany.vemserSpring.Repository.InventarioXItemRepository;
import br.com.dbccompany.vemserSpring.Repository.ItemRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;


@Service
public class InventarioXItemService {

    @Autowired
    private InventarioXItemRepository inventarioXItemRepository;
    @Autowired
    private ItemRepository itemRepository;
    @Autowired
    private InventarioRepository inventarioRepository;

    @Transactional( rollbackFor = Exception.class)
    public InventarioXItem salvar( InventarioXItem inventarioXItem){

        Item item = inventarioXItem.getItem();

        if(item.getId() == null)
        {
            Item it = itemRepository.save(item);
        }
        Inventario inventario = inventarioXItem.getInventario();
        if(inventario.getId() == null)
        {
            Inventario in = inventarioRepository.save(inventario);
        }



        inventarioXItem.setInventario(inventario);
        inventarioXItem.setItem(item);

        return inventarioXItemRepository.save(inventarioXItem);

    }

    @Transactional( rollbackFor = Exception.class)
    public InventarioXItem editar( Integer id, InventarioXItem inventarioXItem){
        inventarioXItem.setId(id);
        return inventarioXItemRepository.save(inventarioXItem);

    }

    public List<InventarioXItem> todosInventarioXItems(){
        return inventarioXItemRepository.findAll();
    }


}
