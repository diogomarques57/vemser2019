package br.com.dbccompany.vemserSpring;
import br.com.dbccompany.vemserSpring.Service.DwarfService;
import br.com.dbccompany.vemserSpring.Service.ElfoService;
import br.com.dbccompany.vemserSpring.Service.InventarioService;
import br.com.dbccompany.vemserSpring.Service.ItemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.data.repository.CrudRepository;
import br.com.dbccompany.vemserSpring.Entity.Elfo;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import br.com.dbccompany.vemserSpring.Entity.*;

import java.util.Optional;
import java.util.UUID;

@SpringBootApplication
public class VemserSpringApplication {
	public static void main(String[] args) {
		SpringApplication.run(VemserSpringApplication.class, args);
	}
}
