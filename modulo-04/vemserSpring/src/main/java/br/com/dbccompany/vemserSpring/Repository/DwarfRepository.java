package br.com.dbccompany.vemserSpring.Repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import br.com.dbccompany.vemserSpring.Entity.*;

import java.util.List;

@Repository
public interface DwarfRepository extends CrudRepository <Dwarf, Integer> {
    Dwarf findByVida( Double vida );
    List<Dwarf> findAll();
}
