package br.com.dbccompany.vemserSpring.Entity;

public class ItemSempreExistente extends Item
{

    public void setQuantidade(int novaQuantidade){
        boolean podeAlterar = novaQuantidade >0;
        this.setQuantidade(podeAlterar?novaQuantidade:1);
    }
}
