package br.com.dbccompany.vemserSpring.Entity;

import java.util.*;
import javax.persistence.*;

@Entity
public class Inventario{
    @SequenceGenerator(allocationSize = 1,name= "INVENTARIOS_SEQ", sequenceName = "INVENTARIOS_SEQ" )
    @GeneratedValue( generator = "INVENTARIOS_SEQ", strategy = GenerationType.SEQUENCE )
    @Id
    @Column (name = "ID_INVENTARIOS")
    private Integer id;
    private Integer tamanho;

    public Integer getTamanho() {
        return tamanho;
    }

    public void setTamanho(Integer tamanho) {
        this.tamanho = tamanho;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @OneToOne(mappedBy="inventario")
    protected Personagem personagem;


    @OneToMany(mappedBy="inventario")
    private List<InventarioXItem> lstInventarioItem;

}
