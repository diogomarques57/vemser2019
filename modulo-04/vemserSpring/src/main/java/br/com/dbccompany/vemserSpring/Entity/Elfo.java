package br.com.dbccompany.vemserSpring.Entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Elfo extends Personagem {
    private static int qtdElfos=0;

    public Elfo(){
        super.setTipo(TipoPersonagem.ELFO);
        super.setStatus(Status.RECEM_CRIADO);
        super.setVida(100.00);
        super.setClasse();
    }

    public int compareTo(String d) {
        return (this.getClass().getName()).compareTo(d.getClass().getName());
    }


    protected void finalize() throws Throwable{
        Elfo.qtdElfos --;
    }

    public static int qtdElfos(){
        return qtdElfos;
    }



    public String imprimirResultado(){
        return "Elfo";
    }
}