package br.com.dbccompany.vemserSpring.Entity;

import java.util.*;

public class ExercitoDeElfos{
    private ArrayList<Elfo> exercito = new ArrayList(0);
    private HashMap<Status, ArrayList<Elfo>> porStatus = new HashMap<>();

    public void alistar(Elfo elfo){
        if(elfo.getClass().getName().equals( "ElfoNoturno") || elfo.getClass().getName().equals( "ElfoVerde")){
            exercito.add(elfo);
            ArrayList<Elfo> elfoStatus = porStatus.get(elfo.getStatus());
            if(elfoStatus == null){
                elfoStatus = new ArrayList<>();
                porStatus.put(elfo.getStatus(), elfoStatus);
            }
            elfoStatus.add(elfo);
        }
    }

    public ArrayList<Elfo> getExercito(){
        return this.exercito;
    }

    public ArrayList<Elfo> getListaPorStatus(Status status){
        return porStatus.get(status);
    }
}
