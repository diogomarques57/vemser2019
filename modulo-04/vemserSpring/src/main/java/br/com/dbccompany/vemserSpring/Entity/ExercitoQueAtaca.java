package br.com.dbccompany.vemserSpring.Entity;


public class ExercitoQueAtaca extends ExercitoDeElfos
{
    private Estrategia estrategia;

    public ExercitoQueAtaca(Estrategia estrategia)
    {
        this.estrategia = estrategia;
    }

    public void trocarEstrategia(Estrategia estrategia)
    {
        this.estrategia = estrategia;
    }

}
