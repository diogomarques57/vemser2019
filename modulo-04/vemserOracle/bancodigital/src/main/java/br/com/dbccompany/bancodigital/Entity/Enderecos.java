package br.com.dbccompany.bancodigital.Entity;


import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import javax.persistence.SequenceGenerator;

@Entity
@SequenceGenerator(allocationSize = 1,name= "ENDERECOS_SEQ", sequenceName = "ENDERECOS_SEQ" )
public class Enderecos extends AbstractEntity{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@Column (name = "ID_ENDERECOS")
	@GeneratedValue( generator = "ENDERECOS_SEQ", strategy = GenerationType.SEQUENCE )
	private Integer id;
	private String nome;
	
	//@OneToOne
	//@OneToMany
	//@ManyToOne
	//@ManyToMany

	@ManyToOne
	@JoinColumn(name = "fk_id_cliente")
	private Clientes cliente;
	
	@ManyToOne
	@JoinColumn(name = "fk_id_bairro")
	private Bairros bairro;

	@OneToMany ( mappedBy = "endereco", cascade = CascadeType.ALL )
	private List<Agencias> agencias = new ArrayList<>();
	
	
	@ManyToMany ( cascade = CascadeType.ALL )
	@JoinTable ( name = "enderecos_x_clientes", 
			joinColumns = {  
					@JoinColumn (name = "id_enderecos" )},
					inverseJoinColumns = { @JoinColumn (name = "id_clientes")}
					
		)
	List<Clientes> clientes = new ArrayList<>();
	
	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Bairros getBairro() {
		return bairro;
	}

	public void setBairro(Bairros bairro) {
		this.bairro = bairro;
	}

	@Override
	public Integer getId() {
		// TODO Auto-generated method stub
		return id;
	}

	@Override
	public void setId(Integer id) {
		this.id = id;
	}
	

}
