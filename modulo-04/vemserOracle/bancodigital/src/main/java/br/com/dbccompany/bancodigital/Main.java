package br.com.dbccompany.bancodigital;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import br.com.dbccompany.bancodigital.Dto.AgenciasDTO;
import br.com.dbccompany.bancodigital.Dto.BairrosDTO;
import br.com.dbccompany.bancodigital.Dto.BancosDTO;
import br.com.dbccompany.bancodigital.Dto.CidadesDTO;
import br.com.dbccompany.bancodigital.Dto.ClientesDTO;
import br.com.dbccompany.bancodigital.Dto.CorrentistasDTO;
import br.com.dbccompany.bancodigital.Dto.EnderecosDTO;
import br.com.dbccompany.bancodigital.Dto.EstadosDTO;
import br.com.dbccompany.bancodigital.Dto.PaisesDTO;
import br.com.dbccompany.bancodigital.Service.AgenciasService;
import br.com.dbccompany.bancodigital.Service.BairrosService;
import br.com.dbccompany.bancodigital.Service.BancosService;
import br.com.dbccompany.bancodigital.Service.CidadesService;
import br.com.dbccompany.bancodigital.Service.ClientesService;
import br.com.dbccompany.bancodigital.Service.CorrentistasService;
import br.com.dbccompany.bancodigital.Service.EnderecosService;
import br.com.dbccompany.bancodigital.Service.EstadosService;
import br.com.dbccompany.bancodigital.Service.PaisesService;

public class Main {
	private static final Logger LOG = Logger.getLogger(Main.class.getName());
	public static void main(String[] args) {
		
		PaisesService service = new PaisesService();
 		EstadosService serviceE = new EstadosService();
		BancosService serviceB = new BancosService();
		CidadesService serviceC = new CidadesService();
		BairrosService serviceBairros = new BairrosService();
		EnderecosService serviceEnderecos = new EnderecosService();
		AgenciasService serviceAgencias = new AgenciasService();
		CorrentistasService serviceCorrentistas = new CorrentistasService();
		ClientesService serviceClientes = new ClientesService();
 		
		ClientesDTO clientes = new ClientesDTO();
		ClientesDTO clientes2 = new ClientesDTO();
		CorrentistasDTO correntistas = new CorrentistasDTO();
		CorrentistasDTO correntistas2 = new CorrentistasDTO();
		
		AgenciasDTO agencias = new AgenciasDTO();
		AgenciasDTO agencias2 = new AgenciasDTO();
		
		List<CorrentistasDTO> Lcorrentistas = new ArrayList<>();
		List<CorrentistasDTO> Lcorrentistas2 = new ArrayList<>();

		List<AgenciasDTO> Lagencias = new ArrayList<>();
		List<AgenciasDTO> Lagencias2 = new ArrayList<>();
		
 		PaisesDTO paises = new PaisesDTO();
 		EstadosDTO estados = new EstadosDTO();
		BancosDTO bancos = new BancosDTO();
		BancosDTO bancos2 = new BancosDTO();
		CidadesDTO cidades = new CidadesDTO();
		BairrosDTO bairros = new BairrosDTO();
		EnderecosDTO enderecos = new EnderecosDTO();
		EnderecosDTO enderecos2 = new EnderecosDTO();
		EnderecosDTO enderecosAgencia = new EnderecosDTO();
		EnderecosDTO enderecosAgencia2 = new EnderecosDTO();
		
		paises.setNome("Brasil");
		estados.setNome("Rio Grande do Sul");
		cidades.setNome("Canoas");
		bairros.setNome("Igara");
		enderecos.setNome("Rua A");
		enderecos.setNome("Rua B");
		enderecosAgencia.setNome("Rua do Banco A");
		enderecosAgencia2.setNome("Rua do Banco B");
		
		estados.setPaises(paises);
		cidades.setEstados(estados);
		bairros.setCidades(cidades);
		enderecos.setBairros(bairros);
		enderecos2.setBairros(bairros);
		enderecosAgencia.setBairros(bairros);
		enderecosAgencia2.setBairros(bairros);
		
		bancos.setNome("Inter");
		bancos2.setNome("Nubank");
		bancos.setCodigo(154);
		bancos2.setCodigo(445);
		
		agencias.setNome("Agencia Inter");
		agencias2.setNome("Agencia Nubank");
		agencias.setEnderecos(enderecosAgencia);
		agencias2.setEnderecos(enderecosAgencia2);
		agencias.setBancos(bancos);
		agencias2.setBancos(bancos2);
		
		correntistas.setTipo("PF");
		correntistas2.setTipo("PF");
		
		Lagencias.add(agencias);
		Lagencias2.add(agencias2);
		
		correntistas.setAgencias(Lagencias);
		correntistas2.setAgencias(Lagencias2);
		
		correntistas.setSaldo(100.00);
		correntistas2.setSaldo(100.00);
		
		clientes.setNome("Cliente A");
		clientes2.setNome("Cliente B");
		clientes.setEnderecos(enderecos);
		clientes2.setEnderecos(enderecos2);
		
		clientes.setConjuge("NENHUM");
		clientes2.setConjuge("NENHUM");
		
		Lcorrentistas.add(correntistas);
		Lcorrentistas2.add(correntistas2);
		
		clientes.setCorrentistas(Lcorrentistas);
		clientes2.setCorrentistas(Lcorrentistas2);
		
		
		correntistas.retirar(100.00);
		correntistas2.depositar(100.00);
		
		
		service.salvarPaises(paises);
		serviceE.salvarEstados(estados);
		serviceB.salvarBancos(bancos);
		serviceB.salvarBancos(bancos2);
		serviceC.salvarCidades(cidades);
		serviceBairros.salvarBairros(bairros);
		serviceEnderecos.salvarEnderecos(enderecos);
		serviceEnderecos.salvarEnderecos(enderecos2);
		serviceEnderecos.salvarEnderecos(enderecosAgencia);
		serviceEnderecos.salvarEnderecos(enderecosAgencia2);
		serviceAgencias.salvarAgencias(agencias);
		serviceAgencias.salvarAgencias(agencias2);
		serviceCorrentistas.salvarCorrentistas(correntistas);
		serviceCorrentistas.salvarCorrentistas(correntistas2);
		serviceClientes.salvarClientes(clientes);
		serviceClientes.salvarClientes(clientes2);
		
		
		
//		serviceCorrentistas.salvarCorrentistas(correntistas);
//		serviceClientes.salvarClientes(clientes);
//		serviceClientes.salvarClientes(clientes);
		System.exit(0);
 		
//		PaisesService serviceP = new PaisesService();
//		EstadosService serviceE = new EstadosService();
//		CidadesService serviceC = new CidadesService();
//		BairrosService serviceB = new BairrosService();
//		EnderecosService serviceEndereco = new EnderecosService();
//		
//		BancosService bancosService = new BancosService();
//		AgenciasService agenciasService = new AgenciasService();
//		CorrentistasService correntistasService = new CorrentistasService();
//		ClientesService clientesService = new ClientesService();
//		
//		
//		Paises pais = new Paises ();
//		Estados  estado = new Estados();
//		Cidades cidade = new Cidades();
//		Bairros bairro = new Bairros();
//		Enderecos endereco = new Enderecos();
//		Enderecos endereco2 = new Enderecos();
//		Enderecos endereco3 = new Enderecos();
//		Enderecos endereco4 = new Enderecos();
//		List<Enderecos> enderecos = new ArrayList<>();
//		List<Enderecos> enderecos2 = new ArrayList<>();
//		
//		Bancos banco = new Bancos();
//		Bancos banco2 = new Bancos();
//		Agencias agencia = new Agencias();
//		Agencias agencia2 = new Agencias();
//		Correntistas correntista = new Correntistas();
//		Correntistas correntista2 = new Correntistas();
//		Clientes cliente = new Clientes();
//		Clientes cliente2 = new Clientes();
//		List<Correntistas> correntistas = new ArrayList<>();
//		List<Correntistas> correntistas2 = new ArrayList<>();
//		List<Clientes> clientes = new ArrayList<>();
//		List<Clientes> clientes2 = new ArrayList<>();
//		List<Agencias> agencias = new ArrayList<>();
//		List<Agencias> agencias2 = new ArrayList<>();
//		
//		Emails email = new Emails();
//		List<Emails> emails = new ArrayList<>();
//		
//		Telefones telefone = new Telefones();
//		List<Telefones> telefones = new ArrayList<>();
//		
//		email.setEnderecoemail("email@email.com.br");
//		
//		telefone.setNumero("51548694");
//		
//		telefones.add(telefone);
//		
//		emails.add(email);
//		
//		clientes.add(cliente);
//		clientes2.add(cliente2);
//		
//		enderecos.add(endereco);
//		enderecos2.add(endereco2);
//		
//		pais.setNome("Brasil");
//		estado.setNome("Rio Grande do Sul");
//		cidade.setNome("Canoas");
//		bairro.setNome("Igara");
//		
//		endereco.setNome("Rua A");
//		endereco2.setNome("Rua B");
//		endereco3.setNome("Rua C");
//		endereco4.setNome("Rua D");
//		
//		banco.setNome("Nubank");
//		banco.setCodigo(154);
//
//		banco2.setNome("Inter");
//		banco2.setCodigo(445);
//		
//		agencia.setNome("Alpha");
//		agencia.setCodigo(500);
//		agencia.setEndereco(endereco3);
//		agencia.setBanco(banco);
//		
//		
//		agencia2.setNome("Beta");
//		agencia2.setCodigo(778);
//		agencia2.setEndereco(endereco4);
//		agencia2.setBanco(banco2);
//		
//		
//		agencias.add(agencia);
//		agencias2.add(agencia2);
//		
//		cliente.setNome("Cliente");
//		cliente2.setNome("Cliente 2");
//		
//		cliente.setConjuge("Nenhum");
//		cliente.setCpf("8456764");
//		cliente.setDataNascimento("04/04/88");
//		cliente.setEstadoCivil(EstadoCivil.SOLTEIRO);
//		cliente.setRg("1568421");
//		cliente.setEmails(emails);
//		cliente.setEnderecos(enderecos);
//		cliente.setTelefones(telefones);
//		
//		cliente2.setConjuge("Nenhum");
//		cliente2.setCpf("41568915");
//		cliente2.setDataNascimento("04/04/90");
//		cliente2.setEstadoCivil(EstadoCivil.SOLTEIRO);
//		cliente2.setRg("4567812");
//		cliente2.setEmails(emails);
//		cliente2.setEnderecos(enderecos2);
//		cliente2.setTelefones(telefones);
//	
//		
//		correntista.setTipo("PF");
//		correntista.setAgencias(agencias);
//		correntista.setClientes(clientes);
//		correntistas.add(correntista);
//		
//		correntista2.setTipo("PF");
//		correntista2.setAgencias(agencias2);
//		correntista2.setClientes(clientes2);
//		correntistas2.add(correntista2);
//			
//		
//		estado.setPais(pais);
//		cidade.setEstado(estado);
//		bairro.setCidades(cidade);
//		endereco.setBairro(bairro);
//		endereco2.setBairro(bairro);
//		endereco3.setBairro(bairro);
//		endereco4.setBairro(bairro);
//		
//		agencia.setBanco(banco);
//		agencia.setEndereco(endereco);
//		correntista.setAgencias(agencias);
//		cliente.setCorrentistas(correntistas);
//		agencia.setBanco(banco);
//
//		serviceP.salvarPaises(pais);
//		serviceE.salvarEstados(estado);
//		serviceC.salvarCidades(cidade);
//		serviceB.salvarBairros(bairro);
//		serviceEndereco.salvarEnderecos(endereco);
//		serviceEndereco.salvarEnderecos(endereco2);
//		serviceEndereco.salvarEnderecos(endereco3);
//		serviceEndereco.salvarEnderecos(endereco4);
//		bancosService.salvarBancos(banco);
//		bancosService.salvarBancos(banco2);
//		agenciasService.salvaragencias(agencia);
//		agenciasService.salvaragencias(agencia2);
//		correntistasService.salvarCorrentistas(correntista);
//		correntistasService.salvarCorrentistas(correntista2);
//		clientesService.salvarClientes(cliente);
//		clientesService.salvarClientes(cliente2);
//		
//		
//		

		System.exit(0);
		
//		
	}
}
