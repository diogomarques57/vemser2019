package br.com.dbccompany.bancodigital.Service;

import java.util.logging.Level;
import java.util.logging.Logger;

import org.hibernate.Transaction;

import br.com.dbccompany.bancodigital.Entity.HIbernateUtil;
import br.com.dbccompany.bancodigital.Dao.ClientesDAO;
import br.com.dbccompany.bancodigital.Dto.ClientesDTO;
import br.com.dbccompany.bancodigital.Entity.Clientes;
import br.com.dbccompany.bancodigital.Entity.Clientes;

public class ClientesService {
	
	private static final ClientesDAO CLIENTES_DAO = new ClientesDAO();
	private static final Logger LOG = Logger.getLogger(ClientesService.class.getName());
	
	public void salvarClientes(ClientesDTO clientasDTO)
	{
		
		boolean started = HIbernateUtil.beginTransaction();
		Transaction transaction = HIbernateUtil.getSession().getTransaction();
		
		Clientes clientas = CLIENTES_DAO.parseFrom(clientasDTO);
		
		
		try {
			Clientes clientasRes = CLIENTES_DAO.buscar(clientas.getId());
			if(clientasRes == null) {
				CLIENTES_DAO.criar(clientas);
			}
			else {
				clientas.setId(clientasRes.getId());
				CLIENTES_DAO.atualizar(clientas);
			}
			if(started)
			{
				transaction.commit();
			}
			clientasDTO.setIdClientes(clientas.getId());
			}catch (Exception e) {
				transaction.rollback();
				LOG.log(Level.SEVERE, e.getMessage(),e);
			}

	}
	
	public void salvarClientes(Clientes clientes)
	{
		
		boolean started = HIbernateUtil.beginTransaction();
		Transaction transaction = HIbernateUtil.getSession().getTransaction();
		
		try {
			Clientes clientesRes = CLIENTES_DAO.buscar(clientes.getId());
			if(clientesRes == null) {
				CLIENTES_DAO.criar(clientes);
			}
			else {
				clientes.setId(clientesRes.getId());
				CLIENTES_DAO.atualizar(clientes);
			}
			if(started)
			{
				transaction.commit();
			}
			}catch (Exception e) {
				transaction.rollback();
				LOG.log(Level.SEVERE, e.getMessage(),e);
			}

	}
}
