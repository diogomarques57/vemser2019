package br.com.dbccompany.bancodigital.Dto;

import java.util.List;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;

import br.com.dbccompany.bancodigital.Entity.EstadoCivil;

public class ClientesDTO {
	
	private Integer idClientes;
	private String nome;
	private String cpf;
	private String rg;
	private String conjuge;
	private String dataNascimento;
	@Enumerated(EnumType.STRING)
	private EstadoCivil estadoCivil;
	
	private TelefonesDTO telefones;
	private EmailsDTO emails;
	private List<CorrentistasDTO> correntistas;
	private EnderecosDTO enderecos;
	
	public Integer getIdClientes() {
		return idClientes;
	}
	public void setIdClientes(Integer idClientes) {
		this.idClientes = idClientes;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getCpf() {
		return cpf;
	}
	public void setCpf(String cpf) {
		this.cpf = cpf;
	}
	public String getRg() {
		return rg;
	}
	public void setRg(String rg) {
		this.rg = rg;
	}
	public String getConjuge() {
		return conjuge;
	}
	public void setConjuge(String conjuge) {
		this.conjuge = conjuge;
	}
	public String getDataNascimento() {
		return dataNascimento;
	}
	public void setDataNascimento(String dataNascimento) {
		this.dataNascimento = dataNascimento;
	}
	public EstadoCivil getEstadoCivil() {
		return estadoCivil;
	}
	public void setEstadoCivil(EstadoCivil estadoCivil) {
		this.estadoCivil = estadoCivil;
	}
	public TelefonesDTO getTelefones() {
		return telefones;
	}
	public void setTelefones(TelefonesDTO telefones) {
		this.telefones = telefones;
	}
	public EmailsDTO getEmails() {
		return emails;
	}
	public void setEmails(EmailsDTO emails) {
		this.emails = emails;
	}
	
	public EnderecosDTO getEnderecos() {
		return enderecos;
	}
	public void setEnderecos(EnderecosDTO enderecos) {
		this.enderecos = enderecos;
	}
	
	public List<CorrentistasDTO> getCorrentistas() {
		return correntistas;
	}
	public void setCorrentistas(List<CorrentistasDTO> correntistas) {
		this.correntistas = correntistas;
	}
	
	

}
