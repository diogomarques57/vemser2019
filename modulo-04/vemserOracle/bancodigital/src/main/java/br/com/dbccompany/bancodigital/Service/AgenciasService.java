package br.com.dbccompany.bancodigital.Service;

import java.util.logging.Level;
import java.util.logging.Logger;

import org.hibernate.Transaction;

import br.com.dbccompany.bancodigital.Dao.AgenciasDAO;
import br.com.dbccompany.bancodigital.Dto.AgenciasDTO;
import br.com.dbccompany.bancodigital.Entity.Agencias;
import br.com.dbccompany.bancodigital.Entity.Agencias;
import br.com.dbccompany.bancodigital.Entity.HIbernateUtil;

public class AgenciasService {
	
	private static final AgenciasDAO AGENCIAS_DAO = new AgenciasDAO();
	private static final Logger LOG = Logger.getLogger(AgenciasService.class.getName());
	
	public void salvarAgencias(AgenciasDTO agenciasDTO)
	{
		
		boolean started = HIbernateUtil.beginTransaction();
		Transaction transaction = HIbernateUtil.getSession().getTransaction();
		
		Agencias agencias = AGENCIAS_DAO.parseFrom(agenciasDTO);
		
		
		try {
			Agencias agenciasRes = AGENCIAS_DAO.buscar(agencias.getId());
			if(agenciasRes == null) {
				AGENCIAS_DAO.criar(agencias);
			}
			else {
				agencias.setId(agenciasRes.getId());
				AGENCIAS_DAO.atualizar(agencias);
			}
			if(started)
			{
				transaction.commit();
			}
			agenciasDTO.setIdAgencias(agencias.getId());
			
			}catch (Exception e) {
				transaction.rollback();
				LOG.log(Level.SEVERE, e.getMessage(),e);
			}

	}
	
	public void salvaragencias(Agencias agencias)
	{
		
		boolean started = HIbernateUtil.beginTransaction();
		Transaction transaction = HIbernateUtil.getSession().getTransaction();
		
		try {
			Agencias agenciasRes = AGENCIAS_DAO.buscar(agencias.getId());
			if(agenciasRes == null) {
				AGENCIAS_DAO.criar(agencias);
			}
			else {
				agencias.setId(agenciasRes.getId());
				AGENCIAS_DAO.atualizar(agencias);
			}
			if(started)
			{
				transaction.commit();
			}
			}catch (Exception e) {
				transaction.rollback();
				LOG.log(Level.SEVERE, e.getMessage(),e);
			}

	}
}
