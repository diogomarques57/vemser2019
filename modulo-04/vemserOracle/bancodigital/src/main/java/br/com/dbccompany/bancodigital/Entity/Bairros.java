package br.com.dbccompany.bancodigital.Entity;

import java.util.ArrayList;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;

@Entity
@SequenceGenerator(allocationSize = 1,name= "BAIRROS_SEQ", sequenceName = "BAIRROS_SEQ" )
public class Bairros extends AbstractEntity {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@Column (name = "ID_BAIRROS")
	@GeneratedValue( generator = "BAIRROS_SEQ", strategy = GenerationType.SEQUENCE )
	
	private Integer id;
	private String nome;
	
	//@OneToOne
	//@OneToMany
	//@ManyToOne
	//@ManyToMany
	
	@ManyToOne
	@JoinColumn(name = "fk_id_cidade")
	private Cidades cidades;
	
	@OneToMany ( mappedBy = "bairro", cascade = CascadeType.ALL )
	private List<Enderecos> endereco = new ArrayList<>();

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	@Override
	public Integer getId() {
		// TODO Auto-generated method stub
		return id;
	}
	
	@Override
	public void setId(Integer id) {
		this.id = id;
	}

	public Cidades getCidades() {
		return cidades;
	}

	public void setCidades(Cidades cidades) {
		this.cidades = cidades;
	}

	public List<Enderecos> getEndereco() {
		return endereco;
	}

	public void setEndereco(List<Enderecos> endereco) {
		this.endereco = endereco;
	}



}
