package br.com.dbccompany.bancodigital.Dao;

import br.com.dbccompany.bancodigital.Dto.EnderecosDTO;
import br.com.dbccompany.bancodigital.Entity.Enderecos;

public class EnderecosDAO extends AbstractDAO<Enderecos> {

	BairrosDAO dao = new BairrosDAO();
	public Enderecos parseFrom(EnderecosDTO dto) {
		Enderecos enderecos = null;
		if(dto.getIdEnderecos() != null)
		{
			enderecos = buscar(dto.getIdEnderecos());
		}
		else {
			enderecos = new Enderecos();
		}
		enderecos.setNome(dto.getNome());
		enderecos.setBairro(dao.parseFrom(dto.getBairros()));
		return enderecos;
	}
	@Override
	protected Class<Enderecos> getEntityClass() {
		return Enderecos.class;
	}
	
}
