package br.com.dbccompany.bancodigital.Dao;

import br.com.dbccompany.bancodigital.Dto.AgenciasDTO;
import br.com.dbccompany.bancodigital.Entity.Agencias;
import br.com.dbccompany.bancodigital.Entity.Agencias;

public class AgenciasDAO extends AbstractDAO<Agencias> {
	BancosDAO dao = new BancosDAO();
	
	public Agencias parseFrom(AgenciasDTO dto) {
		Agencias agencias = null;
		if(dto.getIdAgencias() != null)
		{
			agencias = buscar(dto.getIdAgencias());
		}
		else {
			agencias = new Agencias();
		}
		agencias.setNome(dto.getNome());
		agencias.setBanco(dao.parseFrom(dto.getBancos()));
		return agencias;
	}

	@Override
	protected Class<Agencias> getEntityClass() {
		return Agencias.class;
	}

	
}
