package br.com.dbccompany.bancodigital.Entity;



import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;

import javax.persistence.ManyToOne;

import javax.persistence.SequenceGenerator;



@Entity

public class Emails extends AbstractEntity {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Id
	@SequenceGenerator(allocationSize = 1,name= "ID_EMAILS", sequenceName = "ID_EMAILS" )
	@Column (name = "ID_EMAILS")
	@GeneratedValue( generator = "ID_EMAILS", strategy = GenerationType.SEQUENCE )
	private Integer id;
	private String enderecoemail;
	
	@ManyToOne
	@JoinColumn(name = "fk_id_cliente")
	private Clientes clientes;
	
	public String getEnderecoemail() {
		return enderecoemail;
	}
	public void setEnderecoemail(String enderecoemail) {
		this.enderecoemail = enderecoemail;
	}
	
	
	
	@Override
	public Integer getId() {
		// TODO Auto-generated method stub
		return id;
	}
	
	@Override
	public void setId(Integer id) {
		this.id = id;
	}
}
