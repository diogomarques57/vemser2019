package br.com.dbccompany.bancodigital.Entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.*;
import javax.persistence.SequenceGenerator;

@Entity
@Inheritance( strategy = InheritanceType.JOINED)
public class Elfo extends AbstractEntity {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(allocationSize = 1,name= "ELFO_SEQ", sequenceName = "ELFO_SEQ" )
	@GeneratedValue( generator = "ELFO_SEQ", strategy = GenerationType.SEQUENCE )
	private Integer id;
	
	private String nome;

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	@Override
	public Integer getId() {
		// TODO Auto-generated method stub
		return id;
	}
	
	@Override
	public void setId(Integer id) {
		this.id = id;
	}
}
