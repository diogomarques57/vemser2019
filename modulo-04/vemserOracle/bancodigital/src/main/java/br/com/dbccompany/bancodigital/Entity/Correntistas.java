package br.com.dbccompany.bancodigital.Entity;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.SequenceGenerator;



@Entity
@SequenceGenerator(allocationSize = 1,name= "CORRENTISTAS_SEQ", sequenceName = "CORRENTISTAS_SEQ" )
public class Correntistas extends AbstractEntity {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@Column (name = "ID_CORRENTISTAS")
	@GeneratedValue( generator = "CORRENTISTAS_SEQ", strategy = GenerationType.SEQUENCE )
	private Integer id;
	private String razaoSocial;
	private String cnpj;
	private String tipo;
	private Double saldo;
	
	@ManyToMany ( cascade = CascadeType.ALL )
	@JoinTable ( name = "agencias_x_correntistas", 
					joinColumns = { 
							@JoinColumn (name = "id_agencias" )},
							inverseJoinColumns = { @JoinColumn (name = "id_correntistas")}
							
				)
	private List<Agencias> agencias = new ArrayList<>();
	
	@ManyToMany ( mappedBy = "correntistas" )
	private List<Clientes> clientes = new ArrayList<>();
	
	
	
	@Override
	public Integer getId() {
		// TODO Auto-generated method stub
		return id;
	}
	
	
	public String getRazaoSocial() {
		return razaoSocial;
	}
	public void setRazaoSocial(String razaoSocial) {
		this.razaoSocial = razaoSocial;
	}
	public String getCnpj() {
		return cnpj;
	}
	public void setCnpj(String cnpj) {
		this.cnpj = cnpj;
	}
	public String getTipo() {
		return tipo;
	}
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	

	@Override
	public void setId(Integer id) {
		this.id = id;
	}


	public List<Clientes> getClientes() {
		return clientes;
	}


	public void setClientes(List<Clientes> clientes) {
		this.clientes = clientes;
	}


	public List<Agencias> getAgencias() {
		return agencias;
	}


	public void setAgencias(List<Agencias> agencias) {
		this.agencias = agencias;
	}


	public Double getSaldo() {
		return saldo;
	}


	public void setSaldo(Double saldo) {
		this.saldo = saldo;
	}



	




	
}
