package br.com.dbccompany.bancodigital.Service;

import java.util.logging.Level;
import java.util.logging.Logger;

import org.hibernate.Transaction;

import br.com.dbccompany.bancodigital.Dao.BairrosDAO;
import br.com.dbccompany.bancodigital.Dao.CidadesDAO;
import br.com.dbccompany.bancodigital.Dto.BairrosDTO;
import br.com.dbccompany.bancodigital.Dto.BairrosDTO;
import br.com.dbccompany.bancodigital.Entity.Bairros;
import br.com.dbccompany.bancodigital.Entity.Bairros;
import br.com.dbccompany.bancodigital.Entity.HIbernateUtil;

public class BairrosService {
	
	private static final BairrosDAO BAIRROS_DAO = new BairrosDAO();
	private static final Logger LOG = Logger.getLogger(BairrosService.class.getName());
	CidadesDAO dao = new CidadesDAO();
	
	public void salvarBairros(BairrosDTO bairrosDTO)
	{
		
		boolean started = HIbernateUtil.beginTransaction();
		Transaction transaction = HIbernateUtil.getSession().getTransaction();
		
		Bairros bairros = BAIRROS_DAO.parseFrom(bairrosDTO);
		
		
		try {
			Bairros bairrosRes = BAIRROS_DAO.buscar(bairros.getId());
			if(bairrosRes == null) {
				BAIRROS_DAO.criar(bairros);
			}
			else {
				bairros.setId(bairrosRes.getId());
				BAIRROS_DAO.atualizar(bairros);
			}
			if(started)
			{
				transaction.commit();
			}
			bairrosDTO.setIdBairros(bairros.getId());
			}catch (Exception e) {
				transaction.rollback();
				LOG.log(Level.SEVERE, e.getMessage(),e);
			}

	}
	
	public void salvarBairros(Bairros bairros)
	{
		
		boolean started = HIbernateUtil.beginTransaction();
		Transaction transaction = HIbernateUtil.getSession().getTransaction();
		
		try {
			Bairros bairrosRes = BAIRROS_DAO.buscar(1);
			if(bairrosRes == null) {
				BAIRROS_DAO.criar(bairros);
			}
			else {
				bairros.setId(bairrosRes.getId());
				BAIRROS_DAO.atualizar(bairros);
			}
			if(started)
			{
				transaction.commit();
			}
			}catch (Exception e) {
				transaction.rollback();
				LOG.log(Level.SEVERE, e.getMessage(),e);
			}

	}
	
	

	
}
