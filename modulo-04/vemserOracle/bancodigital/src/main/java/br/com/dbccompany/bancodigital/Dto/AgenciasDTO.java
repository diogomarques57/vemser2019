package br.com.dbccompany.bancodigital.Dto;

import br.com.dbccompany.bancodigital.Entity.Bancos;
import br.com.dbccompany.bancodigital.Entity.Enderecos;


public class AgenciasDTO {
	
	private Integer idAgencias;
	private String nome;
	private BancosDTO bancos;
	private EnderecosDTO enderecos;
	
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public BancosDTO getBancos() {
		return bancos;
	}
	public void setBancos(BancosDTO bancos) {
		this.bancos = bancos;
	}
	
	
	public Integer getIdAgencias() {
		return idAgencias;
	}
	public void setIdAgencias(Integer idAgencias) {
		this.idAgencias = idAgencias;
	}

	public EnderecosDTO getEnderecos() {
		return enderecos;
	}
	public void setEnderecos(EnderecosDTO enderecos) {
		this.enderecos = enderecos;
	}
}
