package br.com.dbccompany.bancodigital.Dao;

import java.util.ArrayList;
import java.util.List;

import br.com.dbccompany.bancodigital.Dto.AgenciasDTO;
import br.com.dbccompany.bancodigital.Dto.CorrentistasDTO;
import br.com.dbccompany.bancodigital.Entity.Agencias;
import br.com.dbccompany.bancodigital.Entity.Correntistas;

public class CorrentistasDAO extends AbstractDAO<Correntistas> {

	AgenciasDAO dao = new AgenciasDAO();
	public Correntistas parseFrom(CorrentistasDTO dto) {
		Correntistas correntistas = null;
		if(dto.getIdCorrentistas() != null)
		{
			correntistas = buscar(dto.getIdCorrentistas());
		}
		else {
			correntistas = new Correntistas();
		}
		
		
		correntistas.setTipo(dto.getTipo());
		correntistas.setSaldo(dto.getSaldo());
		List<AgenciasDTO> agenciasDTO = dto.getAgencias();
		List<Agencias> agencias = new ArrayList<>();
		for(int i = 0; i < agenciasDTO.size();i++)
		{
			AgenciasDTO agencia = agenciasDTO.get(i);
			agencias.add(dao.parseFrom(agencia));
		}
		correntistas.setAgencias(agencias);;
		
		return correntistas;
	}
	
	@Override
	protected Class<Correntistas> getEntityClass() {
		return Correntistas.class;
	}
	
}
