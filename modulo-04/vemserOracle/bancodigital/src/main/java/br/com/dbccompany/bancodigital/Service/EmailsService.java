package br.com.dbccompany.bancodigital.Service;

import java.util.logging.Level;
import java.util.logging.Logger;

import org.hibernate.Transaction;

import br.com.dbccompany.bancodigital.Entity.HIbernateUtil;
import br.com.dbccompany.bancodigital.Dao.EmailsDAO;
import br.com.dbccompany.bancodigital.Entity.Emails;

public class EmailsService {
	
	private static final EmailsDAO EMAILS_DAO = new EmailsDAO();
	private static final Logger LOG = Logger.getLogger(EmailsService.class.getName());
	public void salvarEmails(Emails emails)
	{
		
		boolean started = HIbernateUtil.beginTransaction();
		Transaction transaction = HIbernateUtil.getSession().getTransaction();
		
		try {
			Emails emailsRes = EMAILS_DAO.buscar(emails.getId());
			if(emailsRes == null) {
				EMAILS_DAO.criar(emails);
			}
			else {
				emails.setId(emailsRes.getId());
				EMAILS_DAO.atualizar(emails);
			}
			if(started)
			{
				transaction.commit();
			}
			}catch (Exception e) {
				transaction.rollback();
				LOG.log(Level.SEVERE, e.getMessage(),e);
			}

	}
}
