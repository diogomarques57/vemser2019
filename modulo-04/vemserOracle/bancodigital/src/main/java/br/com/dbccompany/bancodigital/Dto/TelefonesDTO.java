package br.com.dbccompany.bancodigital.Dto;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;

import br.com.dbccompany.bancodigital.Entity.TelefonesType;

public class TelefonesDTO {
	
	private Integer idTelefones;
	private String numero;
	@Enumerated(EnumType.STRING)
	private TelefonesType tiposTelefone;
	
	public Integer getIdTelefones() {
		return idTelefones;
	}
	public void setIdTelefones(Integer idTelefones) {
		this.idTelefones = idTelefones;
	}
	public String getNumero() {
		return numero;
	}
	public void setNumero(String numero) {
		this.numero = numero;
	}
	public TelefonesType getTiposTelefone() {
		return tiposTelefone;
	}
	public void setTiposTelefone(TelefonesType tiposTelefone) {
		this.tiposTelefone = tiposTelefone;
	}
	

	

}
