package br.com.dbccompany.bancodigital.Dto;

public class EnderecosDTO {
	
	private Integer idEnderecos;
	private String nome;
	
	private BairrosDTO bairros;

	public Integer getIdEnderecos() {
		return idEnderecos;
	}

	public void setIdEnderecos(Integer idEnderecos) {
		this.idEnderecos = idEnderecos;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public BairrosDTO getBairros() {
		return bairros;
	}

	public void setBairros(BairrosDTO bairros) {
		this.bairros = bairros;
	}

	
	
}
