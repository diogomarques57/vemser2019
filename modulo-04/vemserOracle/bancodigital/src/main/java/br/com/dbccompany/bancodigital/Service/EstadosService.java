package br.com.dbccompany.bancodigital.Service;

import java.util.logging.Level;
import java.util.logging.Logger;

import org.hibernate.Transaction;

import br.com.dbccompany.bancodigital.Dao.EstadosDAO;
import br.com.dbccompany.bancodigital.Dto.EstadosDTO;
import br.com.dbccompany.bancodigital.Entity.Estados;
import br.com.dbccompany.bancodigital.Entity.HIbernateUtil;

public class EstadosService {
	
	private static final EstadosDAO ESTADOS_DAO = new EstadosDAO();
	private static final Logger LOG = Logger.getLogger(EstadosService.class.getName());
	public void salvarEstados(Estados estados)
	{
		
		boolean started = HIbernateUtil.beginTransaction();
		Transaction transaction = HIbernateUtil.getSession().getTransaction();
		
		try {
			Estados estadosRes = ESTADOS_DAO.buscar(1);
			if(estadosRes == null) {
				ESTADOS_DAO.criar(estados);
			}
			else {
				estados.setId(estadosRes.getId());
				ESTADOS_DAO.atualizar(estados);
			}
			if(started)
			{
				transaction.commit();
			}
			}catch (Exception e) {
				transaction.rollback();
				LOG.log(Level.SEVERE, e.getMessage(),e);
			}

	}
	
	public void salvarEstados(EstadosDTO estadosDTO)
	{
		
		boolean started = HIbernateUtil.beginTransaction();
		Transaction transaction = HIbernateUtil.getSession().getTransaction();
		
		Estados estados = ESTADOS_DAO.parseFrom(estadosDTO);
		
		
		try {
			Estados estadosRes = ESTADOS_DAO.buscar(estados.getId());
			if(estadosRes == null) {
				ESTADOS_DAO.criar(estados);
			}
			else {
				estados.setId(estadosRes.getId());
				ESTADOS_DAO.atualizar(estados);
			}
			if(started)
			{
				transaction.commit();
			}
			estadosDTO.setIdEstados(estados.getId());
			}catch (Exception e) {
				transaction.rollback();
				LOG.log(Level.SEVERE, e.getMessage(),e);
			}

	}
	
	
}
