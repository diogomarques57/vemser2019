package br.com.dbccompany.bancodigital.Entity;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;



@Entity

public class Clientes extends AbstractEntity {
	
	private static final long serialVersionUID = 1L;
	@Id
	@Column (name = "ID_CLIENTES")
	@SequenceGenerator(allocationSize = 1,name= "ID_CLIENTES", sequenceName = "ID_CLIENTES" )
	@GeneratedValue( generator = "ID_CLIENTES", strategy = GenerationType.SEQUENCE )
	private Integer id;
	@Column (name = "nome", length = 100, nullable = false)
	private String nome;
	@Column (name = "cpf", length = 100, nullable = true)
	private String cpf;
	@Column (name = "rg", length = 100, nullable = true)
	private String rg;
	@Column (name = "conjuge", length = 100, nullable = true)
	private String conjuge;
	@Column (name = "dataNascimento", length = 100, nullable = true)
	private String dataNascimento;
	
	@Enumerated(EnumType.STRING)
	private EstadoCivil estadoCivil;
	
	
	@ManyToMany ( cascade = CascadeType.ALL )
	@JoinTable ( name = "correntistas_x_clientes", 
					joinColumns = { 
							@JoinColumn (name = "id_correntistas" )},
							inverseJoinColumns = { @JoinColumn (name = "id_clientes")}
							
				)
	private List<Correntistas> correntistas = new ArrayList<>();
	
	@ManyToMany( mappedBy = "clientes", cascade = CascadeType.ALL )
	private List<Enderecos> enderecos = new ArrayList<>();
	
	@ManyToMany ( mappedBy = "clientes" )
	private List<Telefones> telefones = new ArrayList<>();
	

	
	@OneToMany( mappedBy = "clientes", cascade = CascadeType.ALL )
	private List<Emails> emails = new ArrayList<>();
	
	
	@Override
	public Integer getId()
	{
		return id;
	}
	
	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	public String getRg() {
		return rg;
	}

	public void setRg(String rg) {
		this.rg = rg;
	}

	public String getConjuge() {
		return conjuge;
	}

	public void setConjuge(String conjuge) {
		this.conjuge = conjuge;
	}

	public String getDataNascimento() {
		return dataNascimento;
	}

	public void setDataNascimento(String dataNascimento) {
		this.dataNascimento = dataNascimento;
	}

	public EstadoCivil getEstadoCivil() {
		return estadoCivil;
	}

	public void setEstadoCivil(EstadoCivil estadoCivil) {
		this.estadoCivil = estadoCivil;
	}

	public List<Enderecos> getEnderecos() {
		return enderecos;
	}

	public void setEnderecos(List<Enderecos> enderecos) {
		this.enderecos = enderecos;
	}


	public List<Emails> getEmails() {
		return emails;
	}

	public void setEmails(List<Emails> emails) {
		this.emails = emails;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
	
	public List<Telefones> getTelefones() {
		return telefones;
	}

	public void setTelefones(List<Telefones> telefones) {
		this.telefones = telefones;
	}

	public List<Correntistas> getCorrentistas() {
		return correntistas;
	}

	public void setCorrentistas(List<Correntistas> correntistas) {
		this.correntistas = correntistas;
	}

	@Override
	public void setId(Integer id) {
		this.id = id;
	}
	
	
}
