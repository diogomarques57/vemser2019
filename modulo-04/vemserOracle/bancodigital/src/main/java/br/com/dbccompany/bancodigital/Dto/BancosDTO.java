package br.com.dbccompany.bancodigital.Dto;

import br.com.dbccompany.bancodigital.Entity.Agencias;
import br.com.dbccompany.bancodigital.Entity.Correntistas;
import br.com.dbccompany.bancodigital.Entity.Enderecos;

public class BancosDTO {
	
	private Integer idBancos;
	private String nome;
	private Integer codigo;
	
	public Integer getIdBancos() {
		return idBancos;
	}
	public void setIdBancos(Integer idBancos) {
		this.idBancos = idBancos;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public Integer getCodigo() {
		return codigo;
	}
	public void setCodigo(Integer codigo) {
		this.codigo = codigo;
	}

	
	

}
