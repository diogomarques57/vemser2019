package br.com.dbccompany.bancodigital.Service;

import java.util.logging.Level;
import java.util.logging.Logger;

import org.hibernate.Transaction;

import br.com.dbccompany.bancodigital.Entity.HIbernateUtil;
import br.com.dbccompany.bancodigital.Dao.CidadesDAO;
import br.com.dbccompany.bancodigital.Dto.CidadesDTO;
import br.com.dbccompany.bancodigital.Entity.Cidades;
import br.com.dbccompany.bancodigital.Entity.Cidades;

public class CidadesService {
	
	private static final CidadesDAO CIDADES_DAO = new CidadesDAO();
	private static final Logger LOG = Logger.getLogger(CidadesService.class.getName());
	public void salvarCidades(Cidades cidades)
	{
		
		boolean started = HIbernateUtil.beginTransaction();
		Transaction transaction = HIbernateUtil.getSession().getTransaction();
		
		try {
			Cidades cidadesRes = CIDADES_DAO.buscar(1);
			if(cidadesRes == null) {
				CIDADES_DAO.criar(cidades);
			}
			else {
				cidades.setId(cidadesRes.getId());
				CIDADES_DAO.atualizar(cidades);
			}
			if(started)
			{
				transaction.commit();
			}
			}catch (Exception e) {
				transaction.rollback();
				LOG.log(Level.SEVERE, e.getMessage(),e);
			}

	}
	
	public void salvarCidades(CidadesDTO cidadesDTO)
	{
		
		boolean started = HIbernateUtil.beginTransaction();
		Transaction transaction = HIbernateUtil.getSession().getTransaction();
		
		Cidades cidades = CIDADES_DAO.parseFrom(cidadesDTO);
		
		
		try {
			Cidades cidadesRes = CIDADES_DAO.buscar(cidades.getId());
			if(cidadesRes == null) {
				CIDADES_DAO.criar(cidades);
			}
			else {
				cidades.setId(cidadesRes.getId());
				CIDADES_DAO.atualizar(cidades);
			}
			if(started)
			{
				transaction.commit();
			}
			cidadesDTO.setIdCidades(cidades.getId());
			}catch (Exception e) {
				transaction.rollback();
				LOG.log(Level.SEVERE, e.getMessage(),e);
			}

	}
}
