import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Main {

    public static void main(String[] args){
        Connection conn = Connector.connect();
        // Criar tabela Paises -----------------------------------------------------------
        try{
            ResultSet rs = conn.prepareStatement("select tname from tab where tname = 'PAISES'").executeQuery();
            if(!rs.next())
            {
                conn.prepareStatement(
                        "CREATE TABLE PAISES"
                        + "(\n"
                        + "ID_PAIS INTEGER NOT NULL PRIMARY KEY,\n"
                        + "NOME VARCHAR(100) NOT NULL \n"
                        + ")"
                ).execute();
            }
            PreparedStatement pst = conn.prepareStatement("insert into paises(id_pais, nome)"

                                                            + "values (paises_seq.nextval, ?)");

            ResultSet esrs = conn.prepareStatement("select nome from paises where nome = 'Brasil'").executeQuery();
            if(!esrs.next()) {
                pst.setString(1, "Brasil");
                pst.executeUpdate();
            }

            esrs = conn.prepareStatement("select nome from paises where nome = 'Argentina'").executeQuery();
            if(!esrs.next()) {
                pst.setString(1, "Argentina");
                pst.executeUpdate();
            }

            esrs = conn.prepareStatement("select nome from paises where nome = 'EUA'").executeQuery();
            if(!esrs.next()) {
                pst.setString(1, "EUA");
                pst.executeUpdate();
            }

            esrs = conn.prepareStatement("select nome from paises where nome = 'England'").executeQuery();
            if(!esrs.next()) {
                pst.setString(1, "England");
                pst.executeUpdate();
            }

            rs = conn.prepareStatement("select * from paises").executeQuery();
            while(rs.next())
            {
                System.out.println(String.format("Nome do Pais: %s",rs.getString("nome")));
            }
        }catch (SQLException ex){
            Logger.getLogger(Connector.class.getName()).log(Level.SEVERE, "Erro na Consulta do Main", ex);
        }
        // Criar tabela Estados -----------------------------------------------------------
        try{
            ResultSet rs = conn.prepareStatement("select tname from tab where tname = 'ESTADOS'").executeQuery();
            if(!rs.next())
            {
                conn.prepareStatement(
                        "CREATE TABLE ESTADOS"
                                + "(\n"
                                + "ID_ESTADO INTEGER NOT NULL PRIMARY KEY,\n"
                                + "NOME VARCHAR(100) NOT NULL \n"
                                + ")"
                ).execute();
            }
            PreparedStatement pst = conn.prepareStatement("insert into estados(id_estado, nome)"
                    + "values (estados_seq.nextval, ?)");
            rs = conn.prepareStatement("select * from estados").executeQuery();
            ResultSet esrs = conn.prepareStatement("select nome from estados where nome = 'California'").executeQuery();
            if(!esrs.next()) {
                pst.setString(1, "California");
                pst.executeUpdate();
            }

            esrs = conn.prepareStatement("select nome from estados where nome = 'Para'").executeQuery();
            if(!esrs.next()) {
                pst.setString(1, "Para");
                pst.executeUpdate();
            }

            esrs = conn.prepareStatement("select nome from estados where nome = 'Bahia'").executeQuery();
            if(!esrs.next()) {
                pst.setString(1, "Bahia");
                pst.executeUpdate();
            }

            esrs = conn.prepareStatement("select nome from estados where nome = 'NA'").executeQuery();
            if(!esrs.next()) {
                pst.setString(1, "NA");
                pst.executeUpdate();
            }

            esrs = conn.prepareStatement("select nome from estados where nome = 'WEB'").executeQuery();
            if(!esrs.next()) {
                pst.setString(1, "WEB");
                pst.executeUpdate();
            }

            esrs = conn.prepareStatement("select nome from estados where nome = 'Londres'").executeQuery();
            if(!esrs.next()) {
                pst.setString(1, "Londres");
                pst.executeUpdate();
            }

            esrs = conn.prepareStatement("select nome from estados where nome = 'Boroughs'").executeQuery();
            if(!esrs.next()) {
                pst.setString(1, "Boroughs");
                pst.executeUpdate();
            }

            esrs = conn.prepareStatement("select nome from estados where nome = 'Sao Paulo'").executeQuery();
            if(!esrs.next()) {
                pst.setString(1, "Sao Paulo");
                pst.executeUpdate();
            }

            esrs = conn.prepareStatement("select nome from estados where nome = 'Buenos Aires'").executeQuery();
            if(!esrs.next()) {
                pst.setString(1, "Buenos Aires");
                pst.executeUpdate();
            }

            while(rs.next())
            {
                System.out.println(String.format("Nome do Estado: %s",rs.getString("nome")));
            }
        }catch (SQLException ex){
            Logger.getLogger(Connector.class.getName()).log(Level.SEVERE, "Erro na Consulta do Main", ex);
        }

        // Criar tabela Cidades -----------------------------------------------------------
        try{
            ResultSet rs = conn.prepareStatement("select tname from tab where tname = 'CIDADES'").executeQuery();
            if(!rs.next())
            {
                conn.prepareStatement(
                        "CREATE TABLE CIDADES"
                                + "(\n"
                                + "ID_CIDADE INTEGER NOT NULL PRIMARY KEY,\n"
                                + "NOME VARCHAR(100) NOT NULL \n"
                                + ")"
                ).execute();
            }
            PreparedStatement pst = conn.prepareStatement("insert into cidades(id_cidade, nome)"
                    + "values (cidades_seq.nextval, ?)");
                    rs = conn.prepareStatement("select * from cidades").executeQuery();
            ResultSet esrs = conn.prepareStatement("select nome from cidades where nome = 'Porto Alegre'").executeQuery();
            if(!esrs.next()) {
                pst.setString(1, "Porto Alegre");
                pst.executeUpdate();
            }

            esrs = conn.prepareStatement("select nome from cidades where nome = 'Sao Paulo'").executeQuery();
            if(!esrs.next()) {
                pst.setString(1, "Sao Paulo");
                pst.executeUpdate();
            }

            esrs = conn.prepareStatement("select nome from cidades where nome = 'NA'").executeQuery();
            if(!esrs.next()) {
                pst.setString(1, "NA");
                pst.executeUpdate();
            }

            esrs = conn.prepareStatement("select nome from cidades where nome = 'San francisco'").executeQuery();
            if(!esrs.next()) {
                pst.setString(1, "San Francisco");
                pst.executeUpdate();
            }

            esrs = conn.prepareStatement("select nome from cidades where nome = 'Londres'").executeQuery();
            if(!esrs.next()) {
                pst.setString(1, "Londres");
                pst.executeUpdate();
            }

            esrs = conn.prepareStatement("select nome from cidades where nome = 'ITU'").executeQuery();
            if(!esrs.next()) {
                pst.setString(1, "ITU");
                pst.executeUpdate();
            }

            esrs = conn.prepareStatement("select nome from cidades where nome = 'Buenos Aires'").executeQuery();
            if(!esrs.next()) {
                pst.setString(1, "Buenos Aires");
                pst.executeUpdate();
            }

            while(rs.next())
            {
                System.out.println(String.format("Nome da cidade: %s",rs.getString("nome")));
            }
        }catch (SQLException ex){
            Logger.getLogger(Connector.class.getName()).log(Level.SEVERE, "Erro na Consulta do Main", ex);
        }

        // Criar tabela Bairros -----------------------------------------------------------
        try{
            ResultSet rs = conn.prepareStatement("select tname from tab where tname = 'BAIRROS'").executeQuery();
            if(!rs.next())
            {
                conn.prepareStatement(
                        "CREATE TABLE BAIRROS"
                                + "(\n"
                                + "ID_BAIRRO INTEGER NOT NULL PRIMARY KEY,\n"
                                + "NOME VARCHAR(100) NOT NULL \n"
                                + ")"
                ).execute();
            }
            PreparedStatement pst = conn.prepareStatement("insert into bairros(id_bairro, nome)"
                    + "values (bairros_seq.nextval, ?)");
                    rs = conn.prepareStatement("select * from bairros").executeQuery();
            ResultSet esrs = conn.prepareStatement("select nome from bairros where nome = 'NA'").executeQuery();

            if(!esrs.next()) {
                pst.setString(1, "NA");
                pst.executeUpdate();
            }


            esrs = conn.prepareStatement("select nome from bairros where nome = 'NA'").executeQuery();

            if(!esrs.next()) {
                pst.setString(1, "NA");
                pst.executeUpdate();
            }


            esrs = conn.prepareStatement("select nome from bairros where nome = 'Qualquer'").executeQuery();
            if(!esrs.next()) {
                pst.setString(1, "Qualquer");
                pst.executeUpdate();
            }

            esrs = conn.prepareStatement("select nome from bairros where nome = 'Croydon'").executeQuery();
            if(!esrs.next()) {
                pst.setString(1, "Croydon");
                pst.executeUpdate();
            }

            esrs = conn.prepareStatement("select nome from bairros where nome = 'Caminito'").executeQuery();
            if(!esrs.next()) {
                pst.setString(1, "Caminito");
                pst.executeUpdate();
            }

            esrs = conn.prepareStatement("select nome from bairros where nome = 'BETWEEN HYDE AND POWELL STREETS'").executeQuery();
            if(!esrs.next()) {
                pst.setString(1, "BETWEEN HYDE AND POWELL STREETS");
                pst.executeUpdate();
            }


            while(rs.next())
            {
                System.out.println(String.format("Nome do bairro: %s",rs.getString("nome")));
            }

        }catch (SQLException ex){
            Logger.getLogger(Connector.class.getName()).log(Level.SEVERE, "Erro na Consulta do Main", ex);
        }
    }


}
