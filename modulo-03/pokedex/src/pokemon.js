class Pokemon {// eslint-disable-line no-unused-vars
  constructor( obj ) {
    this.id = obj.id
    this.nome = obj.name
    this.altura = obj.height
    this.peso = obj.weight

    this.tipo = []
    this.status = []
    let i = 0
    while ( i < obj.types.length ) {
      this.tipo.push( obj.types[i].type.name )
      i += 1
    }
    this.tipo.reverse()
    i = 0
    while ( i < obj.stats.length ) {
      this.status.push( `${ obj.stats[i].stat.name } :  ${ obj.stats[i].base_stat }` )
      i += 1
    }
    this.status.reverse()
    this.imagem = obj.sprites.front_default
  }

  converterPeso() {
    this.peso = `${ this.peso * 100 }g`
  }

  converterAltura() {
    this.altura = `${ this.altura * 10 }cm`
  }
}
