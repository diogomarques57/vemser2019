import React, {Component} from 'react';
import './App.css';
import series from './series'

Array.prototype.invalidas = function () {
  let tam = series.length;
  let invalidas = []
  this.series = Array

  for(let i = 0; i < tam;i++)
  {
    const isEmpty = Object.values(series[i]).some(x => (x == null || x == '' || x == undefined))
    if(series[i].anoEstreia > 2019 || isEmpty)
    {
      invalidas.push(series[i])
    }

  }
  return invalidas
}
Array.prototype.filtrarPorAno = function (ano) {
  let tam = series.length;
  let filtradas = []
  this.series = Array

  for(let i = 0; i < tam;i++)
  {
    if(series[i].anoEstreia > ano)
    {
      filtradas.push(series[i])
    }
  }
  return filtradas
}

Array.prototype.filtrarPorAno = function (ano) {
  let tam = series.length;
  let filtradas = []
  this.series = Array

  for(let i = 0; i < tam;i++)
  {
    if(series[i].anoEstreia > ano)
    {
      filtradas.push(series[i])
    }
  }
  return filtradas
}

Array.prototype.souUmAtor = function (nome) {
  let tam = series.length;
  this.series = Array

  for(let i = 0; i < tam;i++)
  {
    for(let j = 0;j < series[i].elenco.length;j++)
      if(series[i].elenco[j].match(nome))
      {
        return true
      }
  }
  return false
}

Array.prototype.mediaDeEpisodios = function () {
  let tam = series.length;
  this.series = Array
  let soma = 0
  for(let i = 0; i < tam;i++)
  {
     soma += series[i].numeroEpisodios
  }
  return soma/tam
}

Array.prototype.totalSalarios = function (index) {
  let tam = series.length;
  this.series = Array
  let soma = 0
  const tElencoseries = series[index].elenco.length
  const tDiretor = series[index].diretor.length
  soma += tElencoseries*40000 + tDiretor*100000
  return soma
}

Array.prototype.queroGenero = function (generoR) {
  let tam = series.length;
  let filtradas = []
  this.series = Array

  for(let i = 0; i < tam;i++)
  {
    if(series[i].genero.indexOf(generoR) != -1)
    {
      filtradas.push(series[i])
    }
  }
  return filtradas
}

Array.prototype.queroTitulo = function (tituloR) {
  let tam = series.length;
  let filtradas = []
  this.series = Array

  for(let i = 0; i < tam;i++)
  {
    let nome = series[i].titulo
    if(nome.indexOf(tituloR) != -1)
    {
      filtradas.push(series[i])
    }
  }
  return filtradas
}

Array.prototype.creditos = function (index) {
  let tam = series.length;
  this.series = Array
  let creditos = []
  let elenco = []
  let diretores = []

  for (let i = 0; i < series[index].elenco.length; i++) {
    var nome = series[index].elenco[i];
    var tmp = nome.split(" ");
    nome = tmp[1] + " " + tmp[0];
    elenco.push(nome)
    elenco.sort().reverse()
  }

  for (let i = 0; i < series[index].diretor.length; i++) {
    var nome = series[index].diretor[i];
    var tmp = nome.split(" ");
    nome = tmp[1] + " " + tmp[0];
    diretores.push(nome)
    diretores.sort().reverse()
}

creditos.push('Elenco: ',elenco,'Diretores: ',diretores)
  return creditos
}

Array.prototype.abreviados = function () {
  let tam = series.length;
  this.series = Array
  let hash ='#'
  for(let i = 0; i < tam;i++)
  {
    for (let j = 0; j < series[i].elenco.length; j++)
     {
        if(series[i].elenco[j].indexOf('.') != -1)
          {
            const nome = series[i].elenco[j]
            const a = series[i].elenco[j].indexOf('.')
            const letra =  nome[a-1]
            hash += letra
          }
    }

    if(hash.length < series[i].elenco.length-1)
    {
      hash = '#'
    }
    
  }
  return hash
}



class App extends Component {
  constructor( props ){
    
    super( props )
    this.series = series
    
  
  }
  

 
  render() {
    // const { episodio, exibirMensagem } = this.state
    return (
     <div className="App">
        <div className="App-Header container">
          
          {  
            
          }
        </div>    
      </div>
   )
   }

  };
export default App;
