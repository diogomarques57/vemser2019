import React from 'react';
import logo from './logo.svg';
import './App.css';
import Login from './Login'
import Agencias from './Agencias'
import{ Link } from 'react-router-dom';
import { PrivateRoute } from './PrivateRoute';
import * as axios from 'axios';
import {BrowserRouter as Router, Route } from 'react-router-dom';
import DadosCliente from './DadosCliente';
import DadosAgencia from './DadosAgencia';
import DadosTipo from './DadosTipo'
import ListaClientes from './ListaClientes'
import ListaAgencias from './ListaAgencias'
import ListaTipos from './ListaTipos'
import ListaContas from './ListaContas'
import Banco from './Banco.css'


function App() {
 
  
  return (
    <div className="App">
      <Router >
        <header className='header'>
        <nav>
          <ul>
            <li><Link to="/Login">Login</Link></li>
            <li><Link to="/ListaAgencias">Agencias</Link></li>
            <li><Link to="/ListaClientes">Clientes</Link></li>
            <li><Link to="/ListaTipos"> Tipos de Conta </Link></li>
            <li><Link to="/ListaContas"> Contas de Clientes </Link></li>
          </ul>
     
        </nav>
        </header>
       
      <div className='container'>
      <PrivateRoute path ="/ListaAgencias" component= { ListaAgencias } />
        <PrivateRoute path ="/ListaClientes" component= { ListaClientes } />
        <PrivateRoute path ="/ListaContas" component= { ListaContas } />
        <PrivateRoute path ="/ListaTipos" component= { ListaTipos } />
        <Route path="/Login" exact component={ Login } />
        <PrivateRoute path="/DadosCliente/:id" exact component={ DadosCliente } />
        <PrivateRoute path="/DadosAgencia/:id" exact component={ DadosAgencia } />
        <PrivateRoute path="/DadosTipo/:id" exact component={ DadosTipo } />
      </div>
    
      </Router>
      <footer className="footer">
        <div className="text-center">
  
            <p>
                    &copy; Copyright 2019 - Banco Vemser. Todos os direitos reservados.<br />
                    DBC Company<br />
            </p>
        </div>
    </footer>
    </div>
  );
}

export default App;
