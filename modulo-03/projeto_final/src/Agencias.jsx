import React, {Component} from 'react';
import * as axios from 'axios';
import{ Link } from 'react-router-dom';
import Agencia from './Agencia'
import ListaClientes from './ListaClientes'
import ListaAgencias from './ListaAgencias'
import ListaTipos from './ListaTipos'
import ListaContas from './ListaContas'
import {BrowserRouter as Router, Route } from 'react-router-dom';

export default class Agencias extends Component {
    constructor(props){
        super(props)
        this.state = {
            agencias : [],
            clientes : []
        }        
    }

    componentDidMount() {
        var header = {
            headers: {Authorization : localStorage.getItem('Authorization')}
        }
        axios.get('http://localhost:1337/agencias', header).then(
            resp=>{
                var agen = resp.data.agencias;
                
                this.setState({
                    agencias : agen

                })
            })
        axios.get('http://localhost:1337/clientes', header).then(
            resp=>{
                var clien = resp.data.clientes;
                
                this.setState({
                    clientes : clien

                })
                console.log(clien)
            })
    }



        

    render() {
        const {agencias,clientes} = this.state
        return (
        <div className="App">
            <header className="App-header"> 
                <ListaAgencias/>
                <ListaClientes/>
                <ListaTipos />
                <ListaContas />
            </header>
        </div>
        
        )   
        
    }
}
 