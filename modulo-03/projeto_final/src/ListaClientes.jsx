import DadosCliente from './DadosCliente';
import {Route} from 'react-router-dom';
import{ Link } from 'react-router-dom';
import * as axios from 'axios';
import React, {Component} from 'react';
import usuario from './icon-user.png'


class ListaClientes extends Component {
   
    componentDidMount() {
        let header = {
            headers: {Authorization : localStorage.getItem('Authorization')}
        }
        axios.get('http://localhost:1337/clientes', header).then(
            resp=>{
                let clien = resp.data.clientes;
                
                this.setState({
                    clientes : clien,
                    todos : clien

                })
            })
    }

    filtrar(evt){
        let tam = this.state.todos.length;
        let filtrada = [];
        const todos = this.state.todos;

        for (let i = 0; i < tam;i++)
        {
            if(todos[i].nome.match(evt.target.value))
            {
                filtrada.push(todos[i]);
            }
        }

       this.setState({
            clientes : filtrada 
       })
    }

    estaPronto(){
        return this.state == null ? false:true 
    }

    render (){
    return(
    <React.Fragment>
        { this.estaPronto() ? 
        <div>
              <div>
                <input type='text' onBlur={this.filtrar.bind(this)}/>
            </div>
        {
            
        this.state.clientes.map((cliente) => {  
        return(
        <Route>
           
            <Link className = 'link ' to={`/DadosCliente/${cliente.id -1}`}>

                <div className='item usuario row flip-in-hor-bottom'>
                    <img className='col col-50' src={usuario} />
                    <h3 className='col col-50 link'> {cliente.nome}</h3>
                </div>
                

            </Link>
            
        </Route>
              )
            
        })
        }
        </div>
         : 'Carregando...' }
           
    </React.Fragment>
    )
    
} }
export default ListaClientes
