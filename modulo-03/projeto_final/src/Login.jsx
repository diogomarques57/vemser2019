import React, {Component} from 'react';
import * as axios from 'axios';

export default class Login extends Component {

    constructor(props){
        super(props)
        this.state={
            email : '',
            senha : ''
        }
        this.trocaValoresState = this.trocaValoresState.bind(this)
    }

    trocaValoresState(evt){
        const  {name, value} = evt.target;
        this.setState({
            [name] : value,      
        })
    }

    logar(evt){
        evt.preventDefault();
        const { email, senha } = this.state
        if(email  && senha)
        {
            //executa regra de login
            axios.post('http://localhost:1337/login', {
                email:this.state.email,
                senha : this.state.senha
            }).then(
                resp=>{
                    localStorage.setItem( 'Authorization' , resp.data.token )    
                    this.props.history.push('/Login')                
                }  ) 
                
        }}
    logout(evt){
        evt.preventDefault();
        localStorage.removeItem('Authorization');
        this.props.history.push('/Login')       
    }

    render() {
        return (
        <div className="login flip-in-hor-bottom"> 
            <React.Fragment>
                {console.log(localStorage.getItem( 'Authorization' ))}
                {localStorage.getItem( 'Authorization' ) == null ?
                <div>
                <h1>Banco Vemser</h1>
                <input type='email' name = 'email' id='nome' placeholder='Digite o seu email' onChange = { this.trocaValoresState } />
                <br />
                <input type='password' name = 'senha' id='senha' placeholder='Digite sua senha' onChange = { this.trocaValoresState } />
                <br />
                <button type='button' className = 'button button-blue' onClick={this.logar.bind(this)}> Logar </button>
                </div>
                : 
                <div><h1>Logado</h1>
                <button type='button' className = 'button button-blue' onClick={this.logout.bind(this)}> Sair </button>
                </div>
                }
            </React.Fragment>
        </div>
        
        )   
        
    }
}