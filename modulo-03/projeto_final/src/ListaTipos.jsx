
import {Route} from 'react-router-dom';
import{ Link } from 'react-router-dom';
import * as axios from 'axios';
import React, {Component} from 'react';
import DadosTipo from './DadosTipo'


class ListatiposConta extends Component {
   
    componentDidMount() {
        var header = {
            headers: {Authorization : localStorage.getItem('Authorization')}
        }
        axios.get('http://localhost:1337/tipoContas', header).then(
            resp=>{
                
                var tipo = resp.data.tipos;
                console.log('Tipo',tipo)
                this.setState({
                    tiposContas : tipo
                })
            })
    }
    estaPronto(){
        return this.state == null ? false:true 
    }

    render (){
    return(
    <React.Fragment>
        { this.estaPronto() ? 
        <div className='flip-in-hor-bottom'>
        {
        this.state.tiposContas.map((tipoC) => {  
        return(
        <Route>
            
            <Link className = 'link ' to={`/DadosTipo/${tipoC.id -1}`}>
            <div className='item tipo'>
            <h4>{tipoC.nome}</h4>
            </div>
            </Link>
        </Route>
              )
            
        })
        }
        </div>
         : 'Carregando...' }
           
    </React.Fragment>
    )
    
} }
export default ListatiposConta
