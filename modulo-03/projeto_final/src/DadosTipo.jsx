
import * as axios from 'axios';
import React, {Component} from 'react';


class DadosTipo extends Component {
   
    componentWillMount() {
        const { match: { params } } = this.props;
        var header = {
            headers: {Authorization : localStorage.getItem('Authorization')}
        }
        axios.get(`http://localhost:1337/tiposConta/${params.id}`,header)
          .then(
            resp=>{
                var tipo = resp.data.tipos;
                {console.log(tipo)}
                this.setState({
                    tipoConta : tipo
                })
            })
   
    }

    estaPronto(){
        return this.state == null? false:true
        
    }
    render ()
    {
        return(
        <React.Fragment>
            <div>
            { this.estaPronto() ?  
                <article className='swing-in-left-fwd'>
               <div className='itemc'>
               <h1> Nome : {this.state.tipoConta.nome} </h1> <br />                
               </div>
                <h2> ID: {this.state.tipoConta.id}  </h2> <br />
                </article>   
            : 
                
                'Carregando...'}
            </div>
        </React.Fragment>
        )
    
    }
} 
export default DadosTipo;
