
import {Route} from 'react-router-dom';
import{ Link } from 'react-router-dom';
import * as axios from 'axios';
import React, {Component} from 'react';
import usuario from './icon-user.png'

class DadosCliente extends Component {
   
    componentWillMount() {
        const { match: { params } } = this.props;
        var header = {
            headers: {Authorization : localStorage.getItem('Authorization')}
        }
        axios.get(` http://localhost:1337/cliente/${params.id}`,header)
          .then(
            resp=>{
                var cliente = resp.data.cliente;
                console.log('Cliente',cliente)
                this.setState({
                    cliente : cliente
                })
            })
   
    }

    estaPronto(){
        return this.state == null? false:true
        
    }
    render ()
    {
        return(
        <React.Fragment>
            <div>
            { this.estaPronto() ?  
                <article className='swing-in-left-fwd'>
                <div className='itemc'>
                <h1>{this.state.cliente.nome}</h1> 
                </div><br />
                <img src={usuario}  />
                <h3>CPF : {this.state.cliente.cpf} <br />
                Agencia :  <Link className='link'to={`/DadosAgencia/${this.state.cliente.agencia.id -1}`}>{this.state.cliente.agencia.nome}</Link> <br />
                </h3>
                </article>   
            : 
                
                'Carregando...'}
            </div>
        </React.Fragment>
        )
    
    }
} 
export default DadosCliente;
