
import * as axios from 'axios';
import React, {Component} from 'react';
import Agencia from './Agencia';

class DadosAgencia extends Component {
   
    componentWillMount() {
        const { match: { params } } = this.props;
        var header = {
            headers: {Authorization : localStorage.getItem('Authorization')}
        }
        axios.get(` http://localhost:1337/agencia/${params.id}`,header)
          .then(
            resp=>{
                var agen = resp.data.agencias
                var agent = new Agencia(agen.id,agen.nome,agen.endereco.bairro,agen.endereco.cidade,agen.endereco.logradouro,agen.endereco.numero,agen.endereco.uf)
                console.log(agent)
                this.setState({
                    agencia : agent
                })
            })
   
    }

    estaPronto(){
        return this.state == null? false:true
        
    }

    setarDigital(evt){
        sessionStorage.setItem(this.state.agencia.id,evt.target.checked)
        
        const aux = this.state.agencia;
        console.log(this.state.agencia)
        this.setState({
            agencia : aux
        })
        this.state.agencia.atualizar();
    }
    

    render ()
    {
        return(
        <React.Fragment>
            <div>
            { this.estaPronto() ?  
                <article className='swing-in-left-fwd'>
               
                <h1>{this.state.agencia.nome}</h1> <br />
                <h3> Endereço </h3>
               
                    <p>
                    Bairro : {this.state.agencia.bairro} <br />
                    Cidade :  {this.state.agencia.cidade} <br />
                    Logradouro : {this.state.agencia.logradouro} <br />
                    Numero : {this.state.agencia.numero} <br />
                    UF : {this.state.agencia.uf} <br />
                    </p>
                    <label className="box">
                        <input type="checkbox" onChange={this.setarDigital.bind(this)} checked = {this.state.agencia.is_digital === 'true' ? true : false } />
                        <span className="checkmark"></span> <h4 className='glowT'>Digital</h4>
                    </label>
                </article>   
                
            : 
                
                'Carregando...'}
            </div>
        </React.Fragment>
        )
    
    }
} 
export default DadosAgencia;
