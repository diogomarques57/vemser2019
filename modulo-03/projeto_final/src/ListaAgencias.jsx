
import {Route} from 'react-router-dom';
import{ Link } from 'react-router-dom';
import * as axios from 'axios';
import React, {Component} from 'react';
import Agencia from './Agencia';

class ListaAgencias extends Component {
   
    componentDidMount() {
        var header = {
            headers: {Authorization : localStorage.getItem('Authorization')}
        }
        axios.get('http://localhost:1337/agencias', header).then(
            resp=>{
                var agen = resp.data.agencias;
                var agent = agen.map(agen => new Agencia(agen.id,agen.nome,agen.endereco.bairro,agen.endereco.cidade,agen.endereco.logradouro,agen.endereco.numero,agen.endereco.uf))
                console.log(agent)
                this.setState({
                    agencias : agent,
                    todos : agent
                })
            })
    }

    

    filtrar(evt){
        let tam = this.state.todos.length;
        let filtrada = [];
        const todos = this.state.todos;
        
        for (let i = 0; i < tam;i++)
        {
            if(todos[i].nome.match(evt.target.value))
            {
            
                filtrada.push(todos[i]);
            }
        }

       this.setState({
            agencias : filtrada 
       })
    }

    filtrarDigitais(evt){
        let tam = this.state.todos.length;
        let filtrada = [];
        const todos = this.state.todos;
        
        for (let i = 0; i < tam;i++)
        {
            if(todos[i].is_digital === 'true')
            {       
                filtrada.push(todos[i]);
            }
        }

       this.setState({
            agencias : filtrada 
       })
    }

    estaPronto(){
        return this.state == null ? false:true 
    }

    render (){
    return(
    <React.Fragment>
        { this.estaPronto() ? 
        <div>
            <div>
                <input type='text' onBlur={this.filtrar.bind(this)}/>
                <button className='button button-blue' onClick={this.filtrarDigitais.bind(this)}>Digitais</button>
            </div>
        
        {
        this.state.agencias.map((agencia) => {  
        return(
            
             <Route >
                 <div className='item agencia flip-in-hor-bottom'>
                <Link className = 'link' to={`/DadosAgencia/${agencia.id -1}`}>
                    <div className='link '>
                        <h1>{agencia.nome}</h1>
                    </div>  
                    
                </Link> 
                {agencia.is_digital === 'true' ? 
                
                <h4 className='glowT'> Agencia Digital </h4>
                : ''}
                </div>
               
              
            </Route>
          
              )
            
        })
        }
        </div>
         : 'Carregando...' }
           
    </React.Fragment>
    )
    
} }
export default ListaAgencias
