
import {Route} from 'react-router-dom';
import{ Link } from 'react-router-dom';
import * as axios from 'axios';
import React, {Component} from 'react';
import DadosTipo from './DadosTipo';
import Banco from './Banco.css';


class ListaContas extends Component {
   
    componentDidMount() {
        var header = {
            headers: {Authorization : localStorage.getItem('Authorization')}
        }
        axios.get('http://localhost:1337/conta/clientes', header).then(
            resp=>{
                var conta = resp.data.cliente_x_conta;
                console.log('Conta',conta)
                this.setState({
                    Contas : conta,
                    todos : conta
                })
            })
    }
    filtrar(evt){
        let tam = this.state.todos.length;
        let filtrada = [];
        const todos = this.state.todos;

        for (let i = 0; i < tam;i++)
        {
            if(todos[i].cliente.nome.match(evt.target.value))
            {
                filtrada.push(todos[i]);
            }
        }

       this.setState({
            Contas : filtrada 
       })
    }
    estaPronto(){
        return this.state == null ? false:true 
    }

    render (){
    return(
    <React.Fragment>
        { this.estaPronto() ? 
        <div>
            <input type='text' onBlur={this.filtrar.bind(this)}/>
        {
        this.state.Contas.map((conta) => {  
        return(
        <Route>     
           <div className='item usuario flip-in-hor-bottom'>
                <Link className = "link" to={`/DadosCliente/${conta.cliente.id -1}`}>
                
                <h4>          
                Cliente : {conta.cliente.nome}<br />
                </h4>

                </Link> 
                Tipo de conta: {conta.tipo.nome}<br />
                
            
            </div>
            
            
            
           
        </Route>
              )
            
        })
        }
        </div>
         : 'Carregando...' }
           
    </React.Fragment>
    )
    
} }
export default ListaContas
