export default class Agencia {
    constructor ( id,nome, bairro, cidade, logradouro, numero, uf ){
        this.nome = nome
        this.id = id
        this.bairro = bairro
        this.cidade = cidade
        this.logradouro = logradouro
        this.numero = numero 
        this.uf = uf
        this.is_digital = sessionStorage.getItem(this.id)
    
    }

    atualizar()
    {
        this.is_digital = sessionStorage.getItem(this.id);
    }
}