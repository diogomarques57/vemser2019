import React from 'react';



function formatarNome(nome)
{

    nome = nome.replace(/\s/g,'');
    nome = nome.replace('.','');
    nome = nome.toLowerCase();

    return nome + " banner";

}


const Titles = props => { 
    return (
    <React.Fragment>

        { props.series.map((serie) => { 
            
            return (
                <React.Fragment>
                <div className = 'pull-left '>
                    <div className=" article fade-in">
                        <div className = {formatarNome(serie.titulo)}  />
                        <h1>{serie.titulo}</h1>
                        <h4>{"Lancamento : " + serie.anoEstreia}</h4>
                        <h4>{"Genero : " + serie.genero.toString()}</h4>
                        {/* <h4>{serie.diretor.toString()}</h4>
                        <h4>{serie.elenco.toString()}</h4> */}
                        <h4>{"Numero de Temporadas : " + serie.temporadas}</h4>
                        <h4>{"Numero de Episodios: " + serie.numeroEpisodios}</h4>
                        <h4>{"Distribuidora : " + serie.distribuidora}</h4>
                    </div>
                   
                </div>
                
                </React.Fragment> )
        }) }
           
    </React.Fragment>
    )
    
} 
export default Titles;