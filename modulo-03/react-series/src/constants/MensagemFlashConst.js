const Mensagens= {
    SUCESSO: {
        REGISTRAROTA: 'Registramos sua nota!'
    },
    ERRO : {
        NOTA_INVALIDA: 'Informar uma nota valida (1-5)',
        CAMPO_OBRIGATORIO: 'Campo obrigatório'
    }
}
export default Mensagens