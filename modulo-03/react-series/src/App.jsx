/* eslint-disable no-extend-native */
import React, {Component} from 'react';
import{ Link } from 'react-router-dom';

import {BrowserRouter as Router, Route } from 'react-router-dom';
import './css/animation.css'
import './css/App.css'
import './css/button.css'
import './css/dropdown.css'
import './css/general.css'
import './css/header.css'
import './css/index.css'
import JsFlix from './modules/JsFlix';
import Home from './modules/Home';
import JsMirror from './modules/JsMirror'
import Login from './modules/Login'
import JsMirrorAvaliadas from './modules/JsMirrorAvaliadas'
import {PrivateRoute} from './components/PrivateRoute'


export default class App extends Component {
  logout(){
    localStorage.removeItem('Authorization');
  }
  render() {

    return (
      <div className="App">
        <div className="App-Header ">
        <Router>
        <header className="header ">
            <nav className="clearfix">
              <ul>
                <li><Link to="/Home">Home</Link></li>
                <li><Link to="/JsMirror">JsMirror</Link></li>
                <li><Link to="/Flix">Flix</Link></li>
                <li><Link to="/JsMirrorAvaliadas">Avaliadas</Link></li>
                <button type = 'button' onClick={this.logout.bind( this )}> Logout </button>
             </ul>
            </nav>
         
        </header>
        
          <React.Fragment>
            <div>
              <PrivateRoute path ="/Home" exact component= { Home } />
              <Route path ="/Login" exact component= { Login } />
              <PrivateRoute path="/Flix" component={ JsFlix } />
              <PrivateRoute path="/JsMirror" component={ JsMirror } />
              <Route path ="/JsMirrorAvaliadas" exact component= { JsMirrorAvaliadas }/>
            </div>
          </React.Fragment>
        </Router>
        </div>
      </div>
    );
    }};
