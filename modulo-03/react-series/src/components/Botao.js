import React from 'react';
import PropTypes from 'prop-types';

const Botao = ({cor, texto, quandoClicar}) =>
<React.Fragment>
    <button className = {`btn ${cor}`} onClick={quandoClicar}> {texto} </button>
</React.Fragment>

BotaoDoSistema.propTypes = {
    texto : PropTypes.string.isRequired,
    quandoClicar : PropTypes.func.isRequired,
    cor : PropTypes.oneOf('verde','azul')
}