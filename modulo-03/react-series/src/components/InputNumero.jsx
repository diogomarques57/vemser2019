// Crie um componente chamado MeuInputNumero com as seguintes funcionalidades:
// Poder receber uma informação para ficar no placeholder do input original (vide o atributo que é utilizado hoje no App.js)
// Poder receber a informação de quando ele deve ser exibido na tela ou não.
// Poder receber um texto para colocar em um span ao lado do input (olhe o código do App.js hoje, onde está a mensagem Qual sua nota para este episódio?. Esta é a informação que deve ser parametrizada. Caso não seja informada, não renderize o span.
// Caso o preenchimento dele seja obrigatório, deve ser possível informar isso para o componente.
// Caso seja obrigatório, deve ser exibida uma borda vermelha em torno do campo e o seguinte texto ao seu lado: “ obrigatório 
// Por fim, passe a utilizar este componente na tela do ReactMirror.js invés da chamada geraCampoNota
import React from 'react';


const InputNumero = props => {
  const {exibir,ph,obrigatorio,mi,funcao} = props

  return (
      <React.Fragment>
        {exibir ?( mi != '' ? <span>{mi} <br /></span> : ''):''} 
        {exibir ? 
          <input 
            type = "number" 
            onBlur = {funcao} 
            placeholder = {ph} 
            className={obrigatorio ? 'border-red' : ''}>
          </input> : ''
        }
        {exibir ?(obrigatorio ? ' obrigatório ' : ''):''}
      </React.Fragment>     
  )
} 
export default InputNumero;




