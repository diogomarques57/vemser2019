import React from 'react'

const EpisodioPadrao = props => {
    let episodio = JSON.parse(localStorage.getItem('ListaA')[props.index])
    return(
        <React.Fragment>
             <div className="row">
                <div className="col col-100">
                    <img className = "banner" src={ episodio.thumbUrl } alt={ episodio.nome }></img>
                    
                </div>
            </div>   
            <div className="row">
                <div className="col col-100">
                    <h2>{ episodio.nome }</h2>
                    <h4> Duracao: { episodio.duracaoEmMin }</h4>
                    <h4> Temp/Ep: { episodio.temporadaEpisodio }</h4>
                    <h4> Já Assisti? { episodio.assistido ? 'Sim' : 'Não' }, { episodio.qtdVezesAssistido } vez(es) </h4>
                    <h4>Nota: {episodio.nota || 'Sem Nota'}</h4>
                 </div>
            </div>
            
                        <button className="button button-red" onClick={  props.sortearNoComp /* .bind( this )*/ }> Próximo </button>
                        <button className="button button-green" onClick={ props.marcarNoComp /* .bind( this )*/ }> Já Assisti </button>   
                

        </React.Fragment>
    )
}

export default EpisodioPadrao