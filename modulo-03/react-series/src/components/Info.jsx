import React from 'react';

function formatarNome(nome)
{
    nome = nome.replace(/\s/g,'');
    nome = nome.replace('.','');
    nome = nome.toLowerCase();

    return nome + ' banner';

}

const info = props => {
   const classe = `${formatarNome(props.texto[0])}-lg`
    return (
        <React.Fragment>
    
         <div className= "center">
            <div >
                <h1>Creditos</h1>
                <div className = {classe} />
                <h2> {props.texto[0]}</h2>
            </div>
        </div>
        <div className = 'row'>
            <div className = 'col col-50 article'>
                
                <h3>Diretores: </h3>
                { props.texto[1].map((text) => { 
                    
                    return (
                <React.Fragment>
                    
                    <p>{text}</p>
                    
                    </React.Fragment> )
                }) }
                <br />
            </div>
            
            <div className = 'col col-50 article'>
                <h3>Elenco: </h3>
                { props.texto[2].map((text) => { 
                    return (
                    <React.Fragment>
                        
                        <p>{text}</p>
                        
                        </React.Fragment> )
                    }) }
                        <br />
            </div>
        </div>
            
     
          

      
        </React.Fragment>
    )
    
} 
export default info;