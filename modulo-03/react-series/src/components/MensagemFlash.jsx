import React from 'react';
import PropTypes from 'prop-types';

const MensagemFlash = props => {
    
    let { cor = 'verde', tempo = 3000, exibirMensagem, classeMensagem='fade-in', mensagem } = props
    
    function apagar() {
        if(exibirMensagem == true)
        {
            setTimeout(() => props.fade(),tempo)
            setTimeout(() => props.apagar(),tempo +1000)
        }
    }

    return (
        <React.Fragment>
            <div className={classeMensagem}>
            <h5 className={cor}>
                
                    { exibirMensagem ?
                         mensagem  :   ''
                    }
                    {apagar()}
                </h5> 
            </div>
        </React.Fragment>
    )
   

} 
export default MensagemFlash;

MensagemFlash.propTypes = {
    exibirMensagem: PropTypes.bool.isRequired,
    mensagem: PropTypes.string.isRequired,
    cor: PropTypes.oneOf(['verde','vermelho'])
}


MensagemFlash.defaultProps = {
cor : 'verde',
tempo : 3000
}