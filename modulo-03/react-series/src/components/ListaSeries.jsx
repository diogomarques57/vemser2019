import PropTypes from 'prop-types';


function _sortear( min, max ){
  min = Math.ceil( min )
  max = Math.floor( max )
  return Math.floor( Math.random() * (max - min) )
}

export default class ListaSeries {

    constructor(){
        this.series = JSON.parse('[{"titulo":"Stranger Things","anoEstreia":2016,"diretor":["Matt Duffer","Ross Duffer"],"genero":["Suspense","Ficcao Cientifica","Drama"],"elenco":["Winona Ryder","David Harbour","Finn Wolfhard","Millie Bobby Brown","Gaten Matarazzo","Caleb McLaughlin","Natalia Dyer","Charlie Heaton","Cara Buono","Matthew Modine","Noah Schnapp"],"temporadas":2,"numeroEpisodios":17,"distribuidora":"Netflix"},{"titulo":"Game Of Thrones","anoEstreia":2011,"diretor":["David Benioff","D. B. Weiss","Carolyn Strauss","Frank Doelger","Bernadette Caulfield","George R. R. Martin"],"genero":["Fantasia","Drama"],"elenco":["Peter Dinklage","Nikolaj Coster-Waldau","Lena Headey","Emilia Clarke","Kit Harington","Aidan Gillen","Iain Glen ","Sophie Turner","Maisie Williams","Alfie Allen","Isaac Hempstead Wright"],"temporadas":7,"numeroEpisodios":67,"distribuidora":"HBO"},{"titulo":"The Walking Dead","anoEstreia":2010,"diretor":["Jolly Dale","Caleb Womble","Paul Gadd","Heather Bellson"],"genero":["Terror","Suspense","Apocalipse Zumbi"],"elenco":["Andrew Lincoln","Jon Bernthal","Sarah Wayne Callies","Laurie Holden","Jeffrey DeMunn","Steven Yeun","Chandler Riggs ","Norman Reedus","Lauren Cohan","Danai Gurira","Michael Rooker ","David Morrissey"],"temporadas":9,"numeroEpisodios":122,"distribuidora":"AMC"},{"titulo":"Band of Brothers","anoEstreia":20001,"diretor":["Steven Spielberg","Tom Hanks","Preston Smith","Erik Jendresen","Stephen E. Ambrose"],"genero":["Guerra"],"elenco":["Damian Lewis","Donnie Wahlberg","Ron Livingston","Matthew Settle","Neal McDonough"],"temporadas":1,"numeroEpisodios":10,"distribuidora":"HBO"},{"titulo":"The JS Mirror","anoEstreia":2017,"diretor":["Lisandro","Jaime","Edgar"],"genero":["Terror","Caos","JavaScript"],"elenco":["Daniela Amaral da Rosa","Antônio Affonso Vidal Pereira da Rosa","Gustavo Lodi Vidaletti","Bruno Artêmio Johann Dos Santos","Márlon Silva da Silva","Izabella Balconi de Moura","Diovane Mendes Mattos","Luciano Maciel Figueiró","Igor Ceriotti Zilio","Alexandra Peres","Vitor Emanuel da Silva Rodrigues","Raphael Luiz Lacerda","Guilherme Flores Borges","Ronaldo José Guastalli","Vinícius Marques Pulgatti"],"temporadas":1,"numeroEpisodios":40,"distribuidora":"DBC"},{"titulo":"10 Days Why","anoEstreia":2010,"diretor":["Brendan Eich"],"genero":["Caos","JavaScript"],"elenco":["Brendan Eich","Bernardo Bosak"],"temporadas":10,"numeroEpisodios":10,"distribuidora":"JS"},{"titulo":"Mr. Robot","anoEstreia":2018,"diretor":["Sam Esmail"],"genero":["Drama","Techno Thriller","Psychological Thriller"],"elenco":["Rami Malek","Carly Chaikin","Portia Doubleday","Martin Wallström","Christian Slater"],"temporadas":3,"numeroEpisodios":32,"distribuidora":"USA Network"},{"titulo":"Narcos","anoEstreia":2015,"diretor":["Paul Eckstein","Mariano Carranco","Tim King","Lorenzo O Brien"],"genero":["Documentario","Crime","Drama"],"elenco":["Wagner Moura","Boyd Holbrook","Pedro Pascal","Joann Christie","Mauricie Compte","André Mattos","Roberto Urbina","Diego Cataño","Jorge A. Jiménez","Paulina Gaitán","Paulina Garcia"],"temporadas":3,"numeroEpisodios":30,"distribuidora":null},{"titulo":"Westworld","anoEstreia":2016,"diretor":["Athena Wickham"],"genero":["Ficcao Cientifica","Drama","Thriller","Acao","Aventura","Faroeste"],"elenco":["Anthony I. Hopkins","Thandie N. Newton","Jeffrey S. Wright","James T. Marsden","Ben I. Barnes","Ingrid N. Bolso Berdal","Clifton T. Collins Jr.","Luke O. Hemsworth"],"temporadas":2,"numeroEpisodios":20,"distribuidora":"HBO"},{"titulo":"Breaking Bad","anoEstreia":2008,"diretor":["Vince Gilligan","Michelle MacLaren","Adam Bernstein","Colin Bucksey","Michael Slovis","Peter Gould"],"genero":["Acao","Suspense","Drama","Crime","Humor Negro"],"elenco":["Bryan Cranston","Anna Gunn","Aaron Paul","Dean Norris","Betsy Brandt","RJ Mitte"],"temporadas":5,"numeroEpisodios":62,"distribuidora":"AMC"}]');
    }

    get todas () {
        return this.series;
    }
    
    get seriesAleatorias() {
      const indice = _sortear( 0, this.todos.length )
      return this.series[ indice ]
  }

    invalidas () {
        let series = this.series
        let tam = series.length;
        let invalidas = []
        
        for(let i = 0; i < tam;i++)
        {
          const isEmpty = Object.values(series[i]).some(x => (x == null || x == '' || x == undefined))
          if(series[i].anoEstreia > 2019 || isEmpty)
          {
            invalidas.push(series[i])
          }
      
        }
        return invalidas
      }
      filtrarPorAno (ano) {
        let series = this.series
        let tam = series.length;
        let filtradas = []
        
        for(let i = 0; i < tam;i++)
        {
          if(series[i].anoEstreia > ano)
          {
            filtradas.push(series[i])
          }
        }
        return filtradas
      }
      
      souUmAtor (nome) {
        let series = this.series
        let tam = series.length;
        
      
        for(let i = 0; i < tam;i++)
        {
          for(let j = 0;j < series[i].elenco.length;j++)
            if(series[i].elenco[j].match(nome))
            {
              return true
            }
        }
        return false
      }
      
      mediaDeEpisodios () {
        let series = this.series
        let tam = series.length;
        
        let soma = 0
        for(let i = 0; i < tam;i++)
        {
           soma += series[i].numeroEpisodios
        }
        return soma/tam
      }
      
      totalSalarios (index) {
        let series = this.series
        if(index <= series.length && index != '')
        {
        let soma = 0
        const tElencoseries = series[index].elenco.length
        const tDiretor = series[index].diretor.length
        soma += tElencoseries*40000 + tDiretor*100000
        return soma
        }
        else
        return false
      }
      
      queroGenero (generoR) {
        let series = this.series
        let tam = series.length;
        let filtradas = []
       
        generoR = generoR.toString()
      
        for(let i = 0; i < tam;i++)
        {
          let genero = series[i].genero
          if(genero.indexOf(generoR) != -1)
          {   
            filtradas.push(series[i])
          }
        }
        return filtradas
      }
      
      queroTitulo (tituloR) {
        let series = this.series
        let tam = series.length;
        let filtradas = []
        
        for(let i = 0; i < tam;i++)
        {
          let nome = series[i].titulo
          if(nome.indexOf(tituloR) != -1)
          {
            filtradas.push(series[i])
          }
        }
        return filtradas
      }
      
      creditos (index) {
        let series = this.series
        let creditos = []
        let elenco = []
        let diretores = []
        
        if(index <= series.length && index != '')
        {
        for (let i = 0; i < series[index].elenco.length; i++) {
          var nome = series[index].elenco[i];
          var tmp = nome.split(" ");
          nome = tmp[1] + " " + tmp[0];
          elenco.push(nome)
          elenco.sort().reverse()
        }
        
        for (let i = 0; i < series[index].diretor.length; i++) {
          var nome = series[index].diretor[i];
          var tmp = nome.split(" ");
          nome = tmp[1] + " " + tmp[0];
          diretores.push(nome)
          diretores.sort().reverse()
      }
        creditos.push(diretores,elenco)
        return creditos
      }
      else
      return false
      }
      
      abreviados () {
        let series = this.series
        let tam = series.length;
        
        let hash ='#'
        for(let i = 0; i < tam;i++)
        {
          for (let j = 0; j < series[i].elenco.length; j++)
           {
              if(series[i].elenco[j].indexOf('.') != -1)
                {
                  const nome = series[i].elenco[j]
                  const a = series[i].elenco[j].indexOf('.')
                  const letra =  nome[a-1]
                  hash += letra
                }
          }
      
          if(hash.length < series[i].elenco.length-1)
          {
            hash = '#'
          }
          
        }
        return hash
      }
}

ListaSeries.propTypes = {

    invalidas: PropTypes.array,
    filtrarPorAno: PropTypes.array,
    queroTitulo: PropTypes.array,
    mediaDeEpisodios: PropTypes.array,
    totalSalarios: PropTypes.array,
    queroGenero: PropTypes.array,
    queroTitulo: PropTypes.array,
    creditos: PropTypes.array,  
    abreviados: PropTypes.string,

}