import React from 'react';

const ListaAvaliacoes = props => { 
    let  listaA = props.listaA

    function ordernar(){
        listaA.sort(function(a,b){
            if(a.temporada > b.temporada)
            {
                return 1;
            }
            else if(a.temporada < b.temporada)
            {
                return -1;
            }
            else{
                if(a.ordemEpisodio > b.ordemEpisodio)
                {
                    return 1;
                }
                else if(a.ordemEpisodio < b.ordemEpisodio)
                {
                return -1;
                }
                else{
                    return 0;
                }
            }
        });
    }

    function retirarNaoAvalidos(){
        listaA = listaA.filter( ( serie ) => {
            return Boolean( serie.nota > 0)
        } )
    }


    return (
    <React.Fragment>
        {retirarNaoAvalidos()}
        {ordernar()}
        {listaA.length > 0 ? listaA.map((episodio) => { 
            console.log(`episode`,episodio)
            return (
                <React.Fragment>
                <div className = 'pull-left '>
                    <div className=" article fade-in">
                        <img className = "banner" src={ episodio.thumbUrl } alt={ episodio.nome }></img>
                        <h2>{ episodio.nome }</h2>
                        <h4> Episodio: { episodio.ordemEpisodio }</h4>
                        <h4> Temp: { episodio.temporada }</h4>
                        <h4>Nota: {episodio.nota || 'Sem Nota'}</h4>
                    </div>
                   
                </div>
                
                </React.Fragment> )
        }) : ''}
           
    </React.Fragment>
    )
    
} 
export default ListaAvaliacoes;