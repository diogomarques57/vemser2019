/* eslint-disable no-extend-native */
import React, {Component} from 'react';
import{ Link } from 'react-router-dom';
import {BrowserRouter as Router, Route } from 'react-router-dom';
import series from '../components/series'
import Titles from '../models/Titles'
import Info from '../components/Info'
import ListaSeries from '../components/ListaSeries'
import '../css/App.css'
import '../css/button.css'
import '../css/dropdown.css'
import '../css/general.css'
import '../css/header.css'
import '../css/index.css'
import MensagemFlash from '../components/MensagemFlash';


class JsFlix extends Component {
  constructor( props ){
    super( props )
    this.listaSeries = new ListaSeries();
    this.todas = this.listaSeries.series;
    this.series = this.listaSeries.series;
    let opcao = 1;
    let tela = 0;
    this.texto = '';
    this.state = {
      opcao,
      tela,
      series,
      
    }
  }
  
 renderizarInvalidas()
 {
  const tela = 0;
  this.tela = tela;
  this.setState(
    {tela}
  ) 
    this.series = this.listaSeries.invalidas()
    this.setState({
      series : this.listaSeries.invalidas()
    })
 }
 renderizarPorAno(evt)
 {
 
    this.setState({
      series : this.listaSeries.filtrarPorAno(evt.target.value)
    })
 }
 souUnoAtor(evt){
  const souAtor = this.listaSeries.souUmAtor(evt.target.value)
  this.souAtor = souAtor
  
  this.setState({
    souAtor,
    exibirMensagem:true
  })

   setTimeout(()=>{
    this.setState( {
      exibirMensagem : false
    })
  },5000)
 }
 renderizarSalarios(evt)
 {
   if(evt.target.value <= this.todas.length)
   {
    const salarios = this.todas[evt.target.value].titulo + ": " + ' R$ ' + this.listaSeries.totalSalarios(evt.target.value)
    this.salarios = salarios
    
    this.setState({
      salarios,
      exibirSalario:true
    })
  
     setTimeout(()=>{
      this.setState( {
        exibirSalario : false
      })
    },5000)
    
  }
 }
 renderizarPorGenero(evt)
 {
    this.setState({
      series : this.listaSeries.queroGenero(evt.target.value)
    })
 }
 renderizarPorTitulo(evt)
 {
    this.setState({
      series : this.listaSeries.queroTitulo(evt.target.value)
    })
 }
 renderizarCreditos(evt)
 {
   if(evt.target.value <= this.todas.length)
   {
    const tela = 1;
    this.tela = tela;
    this.setState(
      {tela}
    
    )
    const texto = []
    const texto2 = []
    texto.push(this.todas[evt.target.value].titulo)
    texto2.push(this.listaSeries.creditos(evt.target.value)[0])
    texto2.push(this.listaSeries.creditos(evt.target.value)[1])
    texto.push(texto2[0])
    texto.push(texto2[1])
    this.texto = texto
    this.setState({
      texto
    })
  }
 }




titulos(){
  const tela = 0;
  this.tela = tela;
  this.setState(
    {tela}
  )
  this.series = this.todas;
  this.setState({
    series
  })
  
}

changeBackground()
{
  let nome = this.listaSeries.seriesAleatorias
    nome = nome.replace(/\s/g,'');
    nome = nome.replace('.','');
    nome = nome.toLowerCase();
    nome = './img/'+nome;
    document.body.style.backgroundImage =  nome
    console.log(nome)

}


selecionar(id)
{
  const tela = 0;
  this.tela = tela;
  this.setState(
    {tela}
  )
  const opcao = id.target.value
  this.opcao = opcao;
 
  this.setState(
    {opcao}
  )
  
}

mudarTelaInfo(){
  const tela = 1;
  this.tela = tela;
  this.setState(
    {tela}
  )
}

Buscar(evt){
  
  switch(this.state.opcao)
  {
    case '1': this.renderizarPorAno(evt)
      break;
    case '2': this.renderizarPorGenero(evt)

    break;
    case '3': this.renderizarPorTitulo(evt)

    break;

    case '4': this.souUnoAtor(evt)
    break;

    case '5': this.renderizarSalarios(evt)
    break;

    case '6': this.renderizarCreditos(evt)
    break;

    default: 
      break;
  }
}

  render() {
    const { series,opcao,tela,texto,souAtor,exibirMensagem, exibirSalario, salarios } = this.state
    return (
      
     <div className="App">
        <div className="App-Header">
            <header className="header">
            <nav className="clearfix">
                <div className="logo pull-left"></div>
            
                <ul className="pull-left">
                    <li>
                    <a onClick = { this.titulos.bind(this)} >Todas</a> 
                    </li>
                    <li>
                    <a onClick = { this.renderizarInvalidas.bind(this) } >Invalidas</a> 
                    </li>
                    
                        
                </ul>
                <div className="div-select pull-right">
                  
                  <input type = "text" onBlur= { this.Buscar.bind(this) } Placeholder= "Buscar..." />
                    <select onChange={this.selecionar.bind(this)}>    
                      <option>Selecione uma opção</option>  
                        <option value = '1'>Pesquisar por ano</option>
                        <option value = '2'>Pesquisar genero</option>
                        <option value = '3'>Pesquisar nome</option>
                        <option value = '4'>Buscar ator</option>
                        <option value = '5'>Buscar salario</option>
                        <option value = '6'>Buscar creditos</option>
                        
                    </select>  
                </div>
            </nav>
            <h4> { exibirMensagem ? (souAtor ? 'Ator encontrado' : 'Ator não encontrado'):'' } </h4>
            <h4> { exibirSalario ? salarios :'' } </h4>
        </header>
        </div>
          <div>
               <div className="container">
               {this.state.tela == 0 ?< Titles series = {this.state.series}></Titles> : <Info texto = {this.texto} /> } 
        
               </div>
              
              
            </div>
        </div>    
   )
   }

  };

export default JsFlix;
