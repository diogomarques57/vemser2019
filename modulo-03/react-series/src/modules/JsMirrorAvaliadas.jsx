import React, {Component} from 'react';
import JsMirror from '../modules/JsMirror'
import{ Link } from 'react-router-dom';
import series from '../components/series'
import '../css/App.css'
import '../css/button.css'
import '../css/dropdown.css'
import '../css/general.css'
import '../css/header.css'
import '../css/index.css'
import ListaAvaliacoes from '../components/ListaAvaliacoes';

class JsMirrorAvaliadas extends Component {
  constructor( props ){
    super( props ) 
  }

  render() {
    
    return (
     <div className="App">
        <div className="App-Header fade-in">
          <ListaAvaliacoes listaA={JSON.parse(localStorage.getItem('ListaA'))} />
        </div>    
      </div>
   )
   }
};

export default JsMirrorAvaliadas;
