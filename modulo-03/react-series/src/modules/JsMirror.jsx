import React, {Component} from 'react';

import ListaEpisodios from '../exemplos/ListaEpisodios';
import EpisodioPadrao from '../components/EpisodioPadrao';
import MensagemFlash from '../components/MensagemFlash';
import InputNumero from '../components/InputNumero'
import JsMirrorAvaliadas from './JsMirrorAvaliadas'
import{ Link } from 'react-router-dom';
import series from '../components/series'
import '../css/App.css'
import '../css/button.css'
import '../css/dropdown.css'
import '../css/general.css'
import '../css/header.css'
import '../css/index.css'
import ListaAvaliacoes from '../components/ListaAvaliacoes';
class JsMirror extends Component {
  constructor( props ){
    super( props )
  
    this.listaEpisodios = new ListaEpisodios();

    
    //this.sortear = this.sortear.bind( this )
    this.state={
      episodio : this.listaEpisodios.episodiosAleatorios,
      tempo : 3000,
      cor: 'verde',
      exibirMensagem: false,
      classeMensagem: '',
      listaA : []
    }
  }

  
  componentWillUnmount()
  {
    localStorage.setItem(`ListaA`, JSON.stringify(this.listaEpisodios.todos));
  }

  sortear = () => {
    const episodio = this.listaEpisodios.episodiosAleatorios
    this.setState({
        episodio
      })
  }

  marcarComoAssistido = () => {
    const {episodio} = this.state
    this.listaEpisodios.marcarComoAssistido( episodio )
    this.setState( {
      episodio
     } )
  }

  registrarNota( evt ){
    const { episodio } = this.state
    episodio.avaliar( evt.target.value )
    

    this.setState({
      episodio,
      exibirMensagem : true,
      
    })

    this.setState( {
      episodio,
      exibirMensagem : true,
      classeMensagem : 'fade-in'
     } )
    
  }

  criarCampoNota(){
    return (
      this.state.episodio.assistido &&(
      <div>
                  <input type="radio" value="1" name="campo-radio" id="campo-radio1" />
                  <label>1</label>
                  <input type="radio" value="2" name="campo-radio" id="campo-radio2" />
                  <label>2</label>
                  <input type="radio" value="3" name="campo-radio" id="campo-radio3" />
                  <label>3</label>
                  <input type="radio" value="4" name="campo-radio" id="campo-radio4" />
                  <label>4</label>
                  <input type="radio" value="5" name="campo-radio" id="campo-radio5" />
                  <label>5</label>
                  <button className="button button-green" onClick={ () => this.avaliar() /* .bind( this )*/ }> Avaliar </button>                 
      </div> 
    ))
  }


  gerarCampoDeNota(){
   return( 
      <div>
        {
          this.state.episodio.assistido && (
          <div>
            <span>Qual sua nota para este episódio?</span>
            <input type="number" placeholder="1 a 5" onBlur={ this.registrarNota.bind( this ) }></input>
          </div>
          )
        }
      </div>
    )
  }

  avaliar(){
    const { episodio } = this.state
    const radios = document.getElementsByName('campo-radio');
    let nota = radios.value
    for(let i =0;i<5;i++)
    {
      if(radios[i].checked)
      {
        nota = radios[i].value
        break
      }
    }
    episodio.avaliar( nota )
    
    this.setState( {
      episodio,
      exibirMensagem : true,
      classeMensagem : 'fade-in'
     } )

   
    
  }


  mudarCor(id)
  {
    this.setState({
      cor : id.target.value
    })
  }

  mudarTempo(time){

    this.setState({
      tempo : time.target.value
      
    })
  }

  fade(){
    this.setState({
      classeMensagem : 'fade-out'
    })

  }

  deixarDeExibir(){
    this.setState({
      exibirMensagem: false,
      classeMensagem : ''
    })
  }

  render() {
    const { episodio, exibirMensagem ,cor,tempo,classeMensagem,listaA,tela} = this.state
    return (
     <div className="App">
        <div className="App-Header container article fade-in">
         <EpisodioPadrao episodio={ episodio } sortearNoComp={ this.sortear } marcarNoComp={ this.marcarComoAssistido } />
          {/* { this.criarCampoNota() } */}
          {episodio.assistido ? <InputNumero exibir = {true} ph = "Teste" obrigatorio = {false} mi = " " funcao = {this.registrarNota.bind( this )} />:''}
          <div className="mensagem">
          <MensagemFlash cor={cor} tempo={tempo} exibirMensagem={exibirMensagem} mensagem={'Sua nota foi registrada com sucesso!'} classeMensagem = {classeMensagem} apagar = {this.deixarDeExibir.bind(this)} fade={this.fade.bind( this )}  />
          
          </div> 
        </div>    
      </div>
   )
   }
};

export default JsMirror;
