import React, {Component} from 'react';
import * as axios from 'axios';

export default class Login extends Component {

    constructor(props){
        super(props)
        this.state={
            email : '',
            password : ''
        }
        this.trocaValoresState = this.trocaValoresState.bind(this)
    }

    trocaValoresState(evt){
        const  {name, value} = evt.target;
        this.setState({
            [name] : value,      
        })
    }

    logar(evt){
        evt.preventDefault();
        const { email, password } = this.state
        if(email  && password)
        {
            //executa regra de login
            axios.post('http://localhost:1337/login', {
                email:this.state.email,
                password : this.state.password
            }).then(
                resp=>{
                    localStorage.setItem( 'Authorization' , resp.data.token )
                    this.props.history.push("/")
                }  ) 
        }}
        

    render() {
        return (
        <div>
            <React.Fragment>
                <h1>Logar</h1>
                <input type='email' name = 'email' id='nome' placeholder='Digite o seu email' onChange = { this.trocaValoresState } />
                <br />
                <input type='password' name = 'password' id='senha' placeholder='Digite sua senha' onChange = { this.trocaValoresState } />
                <br />
                <button type='button' onClick={this.logar.bind(this)}> Logar </button>
            </React.Fragment>
        </div>
        
        )   
        
    }
}