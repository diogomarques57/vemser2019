
let teste1 = "1234";
const pessoa = { 
    nome: "Nome",
    idade: 20,
    endereco:{
        logradouro: "rua da esquerda", 
        numero: 123
    }
}

//console.log(teste1);

{
    let teste1 = "Aqui mudou";
    //console.log(teste1);

}

//console.log(teste1);
function somar(valor1,valor2) {
    console.log(valor1 + valor2);
}

Object.freeze(pessoa);
pessoa.nome = "Outro Nome";
//console.log(pessoa);
//somar("Aqui", 3);

function ondeMoro(cidade)
{
    //console.log("Eu moro em " + cidade + " e sou muito feliz");
    console.log(`Eu moro em ${cidade} e sou muito feliz`);

}

//ondeMoro("Canoas");

function fruteira() {
    let text = "Banana" 
                + "\n" +
                "Ameixa" 
                + "\n" +
                "Goiaba" 
                + "\n" +
                "Pessego" 
                + "\n";
    let newText = `
    Banana
    Ameixa
    Goiaba
    Pessego`;

    console.log(text);
    console.log(newText);
}

//fruteira(); 

function quemSou(pessoa)
{
    console.log(`Meu nome é ${pessoa.nome} e tenho ${pessoa.idade} anos`)
}

//quemSou(pessoa);

let funcaoTesteVar = function(a,b,c=0){
    return a + b + c;
}

let add = funcaoTesteVar;
let resultado = add(3,2);
//console.log(resultado);

//const { nome:n, idade:i} = pessoa;
const { endereco: { logradouro,numero }} = pessoa;

//console.log(n,i);
//console.log(logradouro,numero);

const array = [1,2,3,4];
const [n1,,n2,,n3 = 9] = array;
console.log(n1,n2,n3);

function testarPessoa({nome,idade, endereco:{logradouro}}) {
    console.log(nome,idade);
}

testarPessoa(pessoa);

let a1 = 42;
let b1 = 15;

console.log(a1,b1);

[a1,b1] = [b1,a1];
console.log(a1,b1);