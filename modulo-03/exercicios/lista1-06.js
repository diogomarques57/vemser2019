function arredondar(numero,precisao=2){
    const fator = Math.pow(10, precisao);
    return Math.ceil(numero * fator) / fator;
}
function imprimirGBP(numero){
    let qtdCasasMilhares = 3;
    let separadorMilhar = ',';
    let separadorDecimal = '.';

    let stringBuffer = [];
    let ParteDecimal = arredondar(Math.abs(numero)%1);
    let parteInteira = Math.trunc(numero);
    let parteInteiraString = Math.abs(parteInteira).toString();
    let parteInteiraTamanho = parteInteiraString.length;

    let c = 1;
    while (parteInteiraString.length > 0) {
        if(c % qtdCasasMilhares == 0){
            stringBuffer.push(`${separadorMilhar}${parteInteiraString.slice(parteInteiraTamanho-c)}`)
            parteInteiraString = parteInteiraString.slice(0,parteInteiraTamanho - c)
        }
        else if(parteInteiraString.length < qtdCasasMilhares){
            stringBuffer.push(parteInteiraString)
            parteInteiraString = ''

        }
        c++
    }
    stringBuffer.push(parteInteiraString)

    let decimalString = ParteDecimal.toString().replace('0.','').padStart(2,'0')
    return `${parteInteira >= 0 ? '£ ' : '-£ '}${stringBuffer.reverse().join('')}${separadorDecimal}${decimalString}`
}
console.log(imprimirGBP(0)) // “£ 0.00”
console.log(imprimirGBP(3498.99)) // “£ 3,498.99”
console.log(imprimirGBP(-3498.99)) // “-£ 3,498.99”
console.log(imprimirGBP(2313477.0135)) // “£ 2,313,477.02”

