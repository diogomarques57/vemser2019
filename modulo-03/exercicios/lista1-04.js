function adicionar(numero1)
{
    return function(numero2)
    {
        return numero1+numero2;
    }
    return numero1;
}

let add = op1=>op2=>op1+op2;
const resultado = add(5)(3);

console.log(resultado);

const is_divisivel = (divisor,numero) => !(numero%divisor);
const divisor = 2;
console.log(is_divisivel(divisor,20));
console.log(is_divisivel(divisor,11));
console.log(is_divisivel(divisor,12));