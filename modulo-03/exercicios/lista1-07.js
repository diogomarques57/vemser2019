function cardapioIFood( veggie = true, comLactose = true ) {
  const cardapio = [
    'enroladinho de salsicha',
    'cuca de uva'
  ]
 

  if ( comLactose ) {
    cardapio.push( 'pastel de queijo' )
  }

  cardapio.push(
    'pastel de carne',
    'empada de legumes marabijosa'
  )
  
  let arr = []

  if ( veggie ) {
    // TODO: remover alimentos com carne (é obrigatório usar splice!)
    const indiceEnroladinho = cardapio.indexOf( 'enroladinho de salsicha' )
    cardapio.splice( indiceEnroladinho,1)
    const indicePastelCarne = cardapio.indexOf( 'pastel de carne' )
    cardapio.splice( indicePastelCarne,1 )
    arr = cardapio;
  }
  
  let resultadoFinal = []
  let i = 0

  while (i < arr.length) 
  {
    resultadoFinal[i] = arr[i].toUpperCase()
    i++
  }
  return resultadoFinal;
}
console.log(cardapioIFood()) // esperado: [ 'CUCA DE UVA', 'PASTEL DE QUEIJO', 'EMPADA DE LEGUMES MARABIJOSA' ]