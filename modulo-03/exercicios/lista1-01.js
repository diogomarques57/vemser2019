let circulo = {
    raio : 0,
    tipoCalculo : "A"
};


circulo.raio = 3;
circulo.tipoCalculo = "A";

function calcularCiculo({raio,tipoCalculo}){
    if(tipoCalculo == "A"){
        return Math.ceil(Math.PI * Math.pow(raio,2));
    }
    else if(tipoCalculo == "C")
    {
        return  Math.ceil((2*Math.PI) * raio);
    }
    else 
        return 0;
};

let resultado = calcularCiculo(circulo);
console.log(resultado);

