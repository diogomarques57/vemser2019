import React from 'react'

const EpisodioPadrao = props => {
    const { episodio } = props
    return(
        <React.Fragment>
             <div className="row center">
                <div className="col col-50">
                    <img src={ episodio.thumbUrl } alt={ episodio.nome }></img>
                    
                </div>
               
                <div className="col col-50">
                    <h2>{ episodio.nome }</h2>
                    <h4> Duracao: { episodio.duracaoEmMin }</h4>
                    <h4> Temp/Ep: { episodio.temporadaEpisodio }</h4>
                    <h4> Já Assisti? { episodio.assistido ? 'Sim' : 'Não' }, { episodio.qtdVezesAssistido } vez(es) </h4>
                    <h4>Nota: {episodio.nota || 'Sem Nota'}</h4>
                 </div>
                 <div className="container center">
                        <button className="button button-red" onClick={  props.sortearNoComp /* .bind( this )*/ }> Próximo </button>
                        <button className="button button-green" onClick={ props.marcarNoComp /* .bind( this )*/ }> Já Assisti </button>   
                </div>   
            </div>      
        </React.Fragment>
    )
}

export default EpisodioPadrao