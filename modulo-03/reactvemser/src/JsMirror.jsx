import React, {Component} from 'react';
import './App.css';
import './button.css';
import ListaEpisodios from './exemplos/ListaEpisodios';
import EpisodioPadrao from './components/EpisodioPadrao';
import series from './components/series'
import TesteRenderizacao from './components/TesteRenderizacao'
class JsMirror extends Component {
  constructor( props ){
    super( props )
    this.listaEpisodios = new ListaEpisodios();
    //this.sortear = this.sortear.bind( this )
    this.state={
      episodio : this.listaEpisodios.episodiosAleatorios
    }
  }

  sortear = () => {
    const episodio = this.listaEpisodios.episodiosAleatorios
    this.setState({
        episodio
      })
  }

  marcarComoAssistido = () => {
    const {episodio} = this.state
    this.listaEpisodios.marcarComoAssistido( episodio )
    this.setState( {
      episodio
     } )
  }

  registrarNota( evt ){
    const { episodio } = this.state
    episodio.avaliar( evt.target.value )
    this.setState({
      episodio,
      exibirMensagem : true
    })
    setTimeout(()=>{
      this.setState( {
        exibirMensagem : false
      })
    },5000)
    
  }

  criarCampoNota(){
    return (
      this.state.episodio.assistido &&(
      <div>
                  <input type="radio" value="1" name="campo-radio" id="campo-radio1" />
                  <label>1</label>
                  <input type="radio" value="2" name="campo-radio" id="campo-radio2" />
                  <label>2</label>
                  <input type="radio" value="3" name="campo-radio" id="campo-radio3" />
                  <label>3</label>
                  <input type="radio" value="4" name="campo-radio" id="campo-radio4" />
                  <label>4</label>
                  <input type="radio" value="5" name="campo-radio" id="campo-radio5" />
                  <label>5</label>
                  <button className="button button-green" onClick={ () => this.avaliar() /* .bind( this )*/ }> Avaliar </button>                 
      </div> 
    ))
  }


  gerarCampoDeNota(){
   return( 
      <div>
        {
          this.state.episodio.assistido && (
          <div>
            <span>Qual sua nota para este episódio?</span>
            <input type="number" placeholder="1 a 5" onBlur={ this.registrarNota.bind( this ) }></input>
          </div>
          )
        }
      </div>
    )
  }
  
  

  avaliar(){
    const { episodio } = this.state
    const radios = document.getElementsByName('campo-radio');
    let nota = radios.value
    for(let i =0;i<5;i++)
    {
      if(radios[i].checked)
      {
        nota = radios[i].value
        break
      }
    }
    episodio.avaliar( nota )
    
    this.setState( {
      episodio,
      exibirMensagem : true
     } )

     setTimeout(()=>{
      this.setState( {
        exibirMensagem : false
      })
    },5000)
    
  }


  render() {
    const { episodio, exibirMensagem } = this.state
    return (
     <div className="App">
        <div className="App-Header container">
            <EpisodioPadrao episodio={ episodio } sortearNoComp={ this.sortear } marcarNoComp={ this.marcarComoAssistido } />
            { this.criarCampoNota() }
            <h5>{ exibirMensagem ? 'Nota registrada com sucesso!' : ''}</h5> 
        </div>    
      </div>
   )
   }
};

export default JsMirror;
